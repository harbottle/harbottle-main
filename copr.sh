#!/bin/bash
if ./new.sh ${1} ${2} ${3}; then
  mkdir -p ~/.config
  echo -e "[copr-cli]\nlogin = ${copr_login}\nusername = harbottle\ntoken = ${copr_token}\ncopr_url = https://copr.fedorainfracloud.org" > ~/.config/copr
  copr-cli build -r ${2} ${3} rpmbuild/SRPMS/*.src.rpm
fi
