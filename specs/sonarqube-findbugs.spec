%define __jar_repack 0

Name:     sonarqube-findbugs
Version:  4.2.0
Release:  1%{?dist}.harbottle
Summary:  SonarQube SpotBugs plugin
Group:    Applications/System
License:  LGPL-3.0
URL:      https://github.com/spotbugs/sonar-findbugs
Source0:  https://repo1.maven.org/maven2/com/github/spotbugs/sonar-findbugs-plugin/%{version}/sonar-findbugs-plugin-%{version}.jar
Autoprov: no

%description
SpotBugs plugin for SonarQube.

%prep

%install
install -d -m 755 $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins
mv %{SOURCE0} $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins

%pre
rm -f %{_var}/lib/sonarqube/extensions/plugins/sonar-findbugs-plugin-*.jar
getent group sonarqube >/dev/null || groupadd -f -r sonarqube
getent passwd sonarqube >/dev/null || useradd -r -g sonarqube -d /usr/share/sonarqube -s /sbin/nologin -c "SonarQube user" sonarqube
exit 0

%files
%attr(0644,sonarqube,sonarqube) %{_var}/lib/sonarqube/extensions/plugins/sonar-findbugs-plugin-%{version}.jar

%changelog
* Mon Jul 04 2022 - harbottle@room3d3.com - 4.2.0-1
  - Bump version

* Thu May 05 2022 - harbottle@room3d3.com - 4.1.5-1
  - Bump version

* Thu Apr 28 2022 - harbottle@room3d3.com - 4.1.4-1
  - Bump version

* Wed Apr 20 2022 - harbottle@room3d3.com - 4.1.3-1
  - Bump version

* Mon Apr 18 2022 - harbottle@room3d3.com - 4.1.0-1
  - Bump version

* Thu Feb 17 2022 - harbottle@room3d3.com - 4.0.6-1
  - Bump version

* Sun Dec 12 2021 - harbottle@room3d3.com - 4.0.5-1
  - Bump version

* Thu Aug 26 2021 - harbottle@room3d3.com - 4.0.4-1
  - Bump version

* Sat Jan 02 2021 - harbottle@room3d3.com - 4.0.3-1
  - Bump version

* Mon Nov 30 2020 - harbottle@room3d3.com - 4.0.2-1
  - Bump version

* Wed Sep 23 2020 - harbottle@room3d3.com - 4.0.1-1
  - Bump version
  - Modify download source

* Sat Sep 19 2020 - harbottle@room3d3.com - 4.0.0-1
  - Bump version

* Fri Jan 03 2020 - harbottle@room3d3.com - 3.11.1-1
  - Initial package
