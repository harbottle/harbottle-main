%global __os_install_post %(echo '%{__os_install_post}' | sed -e 's!/usr/lib[^[:space:]]*/brp-mangle-shebangs[[:space:]].*$!!g')

Name:             mattermost
Version:          7.0.1
Release:          1%{?dist}.harbottle
Summary:          Mattermost Server - Team Edition: open source Slack-alternative in Golang and React
Group:            Applications/System
License:          MIT Compiled
Url:              https://%{name}.com/
Source0:          https://releases.%{name}.com/%{version}/%{name}-team-%{version}-linux-amd64.tar.gz
Source1:          %{name}.service
Source2:          %{name}.sysconfig
BuildRequires:    systemd-units
Requires(pre):    shadow-utils
Requires(post):   systemd-units
Requires(preun):  systemd-units
Requires(postun): systemd-units
Provides:         %{name}-server = %{version}
Provides:         %{name}-webapp = %{version}

%description
Mattermost Server - Team Edition

Mattermost is an open source, private cloud, Slack-alternative from
https://mattermost.org.

It's written in Golang and React and runs as a single Linux binary with MySQL
or PostgreSQL.

%prep
%setup -q -n %{name}

%install
# dirs
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -d -m 755 $RPM_BUILD_ROOT%{_datadir}/%{name}
install -d -m 755 $RPM_BUILD_ROOT%{_sharedstatedir}/%{name}
install -d -m 755 $RPM_BUILD_ROOT%{_sysconfdir}
install -d -m 755 $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig
install -d -m 755 $RPM_BUILD_ROOT%{_unitdir}
# bin
rm -f bin/platform
mv bin/%{name} $RPM_BUILD_ROOT%{_bindir}
mv bin $RPM_BUILD_ROOT%{_datadir}/%{name}
ln -s %{_bindir}/%{name} $RPM_BUILD_ROOT%{_datadir}/%{name}/bin/%{name}
# config
rm -f config/default.json
mv config $RPM_BUILD_ROOT%{_sysconfdir}/%{name}
ln -s %{_sysconfdir}/%{name} $RPM_BUILD_ROOT%{_datadir}/%{name}/config
# logs
rm -rf logs
# data
ln -s %{_sharedstatedir}/%{name} $RPM_BUILD_ROOT%{_datadir}/%{name}/data
# everything else
shopt -s extglob dotglob
mv !(*.txt|*.md) $RPM_BUILD_ROOT%{_datadir}/%{name}
shopt -u extglob dotglob
# service
install -m 644 %{SOURCE1} $RPM_BUILD_ROOT%{_unitdir}/%{name}.service
# sysconfig
install -m 644 %{SOURCE2} $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/%{name}

%pre
getent group %{name} >/dev/null || groupadd -f -r %{name}
getent passwd %{name} >/dev/null || useradd -r -g %{name} -d %{_datadir}/%{name} -s /sbin/nologin -c "%{name} user" %{name}
exit 0

%post
%systemd_post %{name}.service

%preun
%systemd_preun %{name}.service

%postun
%systemd_postun_with_restart %{name}.service

%files
%license MIT-COMPILED-LICENSE.md NOTICE.txt
%doc README.md
# bin
%attr(0755,root,root) %{_bindir}/%{name}
# config
%config(noreplace) %attr(-,%{name},%{name}) %{_sysconfdir}/%{name}
# data
%attr(-,%{name},%{name}) %{_sharedstatedir}/%{name}
# everything else
%attr(-,%{name},%{name}) %{_datadir}/%{name}
# service
%attr(0644,root,root) %{_unitdir}/%{name}.service
# sysconfig
%config(noreplace) %attr(0644,root,root) %{_sysconfdir}/sysconfig/%{name}

%changelog
* Fri Jun 24 2022 - harbottle@room3d3.com - 7.0.1-1
  - Bump version

* Tue Jun 14 2022 - harbottle@room3d3.com - 7.0.0-1
  - Bump version

* Mon Jun 13 2022 - harbottle@room3d3.com - 6.7.1-1
  - Bump version

* Fri May 13 2022 - harbottle@room3d3.com - 6.7.0-1
  - Bump version

* Thu Apr 28 2022 - harbottle@room3d3.com - 6.6.1-1
  - Bump version

* Wed Apr 13 2022 - harbottle@room3d3.com - 6.6.0-1
  - Bump version

* Tue Mar 15 2022 - harbottle@room3d3.com - 6.5.0-1
  - Bump version

* Thu Mar 10 2022 - harbottle@room3d3.com - 6.4.2-1
  - Bump version

* Fri Feb 25 2022 - harbottle@room3d3.com - 6.4.1-1
  - Bump version

* Wed Feb 16 2022 - harbottle@room3d3.com - 6.4.0-1
  - Bump version

* Thu Feb 03 2022 - harbottle@room3d3.com - 6.3.3-1
  - Bump version

* Fri Jan 28 2022 - harbottle@room3d3.com - 6.3.2-1
  - Bump version

* Fri Jan 21 2022 - harbottle@room3d3.com - 6.3.1-1
  - Bump version

* Sat Jan 15 2022 - harbottle@room3d3.com - 6.3.0-1
  - Bump version

* Sun Dec 12 2021 - harbottle@room3d3.com - 6.1.0-1
  - Bump version

* Wed Oct 27 2021 - harbottle@room3d3.com - 6.0.2-1
  - Bump version

* Mon Oct 18 2021 - harbottle@room3d3.com - 6.0.1-1
  - Bump version

* Tue Oct 12 2021 - harbottle@room3d3.com - 6.0.0-1
  - Bump version

* Wed Sep 15 2021 - harbottle@room3d3.com - 5.39.0-1
  - Bump version

* Thu Aug 26 2021 - harbottle@room3d3.com - 5.38.2-1
  - Bump version

* Wed Aug 18 2021 - harbottle@room3d3.com - 5.38.1-1
  - Bump version

* Mon Aug 16 2021 - harbottle@room3d3.com - 5.38.0-1
  - Bump version

* Wed Aug 04 2021 - harbottle@room3d3.com - 5.37.1-1
  - Bump version

* Thu Jul 15 2021 - harbottle@room3d3.com - 5.37.0-1
  - Bump version

* Mon Jun 21 2021 - harbottle@room3d3.com - 5.36.1-1
  - Bump version

* Tue Jun 15 2021 - harbottle@room3d3.com - 5.36.0-1
  - Bump version

* Thu Jun 03 2021 - harbottle@room3d3.com - 5.35.2-1
  - Bump version

* Tue May 18 2021 - harbottle@room3d3.com - 5.35.1-1
  - Bump version

* Thu May 13 2021 - harbottle@room3d3.com - 5.35.0-1
  - Bump version

* Sat Apr 17 2021 - harbottle@room3d3.com - 5.34.2-1
  - Bump version

* Thu Apr 15 2021 - harbottle@room3d3.com - 5.34.1-1
  - Bump version

* Thu Apr 15 2021 - harbottle@room3d3.com - 5.34.0-1
  - Bump version

* Wed Mar 31 2021 - harbottle@room3d3.com - 5.33.3-1
  - Bump version

* Thu Mar 25 2021 - harbottle@room3d3.com - 5.33.2-1
  - Bump version

* Mon Mar 22 2021 - harbottle@room3d3.com - 5.33.1-1
  - Bump version

* Wed Mar 17 2021 - harbottle@room3d3.com - 5.33.0-1
  - Bump version

* Wed Feb 17 2021 - harbottle@room3d3.com - 5.32.1-1
  - Bump version

* Mon Feb 15 2021 - harbottle@room3d3.com - 5.32.0-1
  - Bump version

* Fri Feb 05 2021 - harbottle@room3d3.com - 5.31.1-1
  - Bump version

* Fri Jan 15 2021 - harbottle@room3d3.com - 5.31.0-1
  - Bump version

* Fri Jan 15 2021 - harbottle@room3d3.com - 5.30.2-1
  - Bump version

* Fri Dec 18 2020 - harbottle@room3d3.com - 5.30.1-1
  - Bump version
  - Fix build

* Thu Dec 03 2020 - harbottle@room3d3.com - 5.29.1-1
  - Bump version

* Fri Nov 20 2020 - harbottle@room3d3.com - 5.29.0-1
  - Bump version

* Mon Oct 19 2020 - harbottle@room3d3.com - 5.28.1-1
  - Bump version

* Fri Oct 16 2020 - harbottle@room3d3.com - 5.28.0-1
  - Bump version

* Mon Sep 14 2020 - harbottle@room3d3.com - 5.27.0-1
  - Bump version

* Fri Sep 04 2020 - harbottle@room3d3.com - 5.26.2-1
  - Bump version

* Wed Aug 26 2020 - harbottle@room3d3.com - 5.26.1-1
  - Bump version

* Wed Aug 12 2020 - harbottle@room3d3.com - 5.26.0-1
  - Bump version

* Fri Jul 31 2020 - harbottle@room3d3.com - 5.25.2-1
  - Bump version

* Fri Jul 24 2020 - harbottle@room3d3.com - 5.25.1-1
  - Bump version

* Fri Jun 26 2020 - harbottle@room3d3.com - 5.24.2-1
  - Bump version

* Fri Jun 19 2020 - harbottle@room3d3.com - 5.24.1-1
  - Bump version

* Wed Jun 17 2020 - harbottle@room3d3.com - 5.24.0-1
  - Bump version

* Tue Jun 02 2020 - harbottle@room3d3.com - 5.23.1-1
  - Bump version

* Thu May 14 2020 - harbottle@room3d3.com - 5.23.0-1
  - Bump version

* Mon May 11 2020 - harbottle@room3d3.com - 5.22.3-1
  - Bump version

* Mon May 04 2020 - harbottle@room3d3.com - 5.22.2-1
  - Bump version

* Thu Apr 23 2020 - harbottle@room3d3.com - 5.22.1-1
  - Bump version

* Thu Apr 16 2020 - harbottle@room3d3.com - 5.22.0-1
  - Bump version

* Fri Mar 13 2020 - harbottle@room3d3.com - 5.21.0-1
  - Bump version

* Fri Mar 13 2020 - harbottle@room3d3.com - 5.20.2-1
  - Bump version

* Sat Feb 29 2020 - harbottle@room3d3.com - 5.20.1-2
  - Rename package
  - Improve systemd service file

* Sat Feb 29 2020 - harbottle@room3d3.com - 5.20.1-1
  - Bump version

* Wed Jan 22 2020 - harbottle@room3d3.com - 5.19.1-1
  - Bump version

* Thu Jan 16 2020 - harbottle@room3d3.com - 5.19.0-1
  - Bump version

* Thu Jan 16 2020 - harbottle@room3d3.com - 5.18.2-1
  - Bump version

* Wed Jan 08 2020 - harbottle@room3d3.com - 5.18.1-1
  - Bump version

* Thu Dec 19 2019 - harbottle@room3d3.com - 5.18.0-1
  - Bump version

* Tue Dec 10 2019 - harbottle@room3d3.com - 5.17.1-3
  - Fix source RPM for el8

* Tue Dec 10 2019 - harbottle@room3d3.com - 5.17.1-2
  - Fix dist tag for el8

* Mon Nov 25 2019 - harbottle@room3d3.com - 5.17.1-1
  - Bump version

* Sat Nov 16 2019 - harbottle@room3d3.com - 5.17.0-1
  - Bump version

* Wed Nov 06 2019 - harbottle@room3d3.com - 5.16.3-1
  - Bump version

* Wed Oct 30 2019 - harbottle@room3d3.com - 5.16.2-1
  - Bump version

* Thu Oct 24 2019 - harbottle@room3d3.com - 5.16.1-1
  - Bump version

* Thu Oct 17 2019 - harbottle@room3d3.com - 5.16.0-1
  - Bump version

* Fri Oct 11 2019 - harbottle@room3d3.com - 5.15.1-1
  - Bump version

* Mon Sep 16 2019 - harbottle@room3d3.com - 5.15.0-1
  - Bump version

* Fri Aug 30 2019 - harbottle@room3d3.com - 5.14.2-1
  - Bump version

* Wed Aug 28 2019 - harbottle@room3d3.com - 5.14.1-1
  - Bump version

* Sat Aug 17 2019 - harbottle@room3d3.com - 5.14.0-1
  - Bump version

* Wed Jul 24 2019 - harbottle@room3d3.com - 5.13.2-1
  - Bump version

* Fri Jul 19 2019 - harbottle@room3d3.com - 5.13.1-1
  - Bump version

* Wed Jul 17 2019 - harbottle@room3d3.com - 5.13.0-1
  - Bump version

* Mon Jul 15 2019 - harbottle@room3d3.com - 5.12.4-1
  - Bump version

* Tue Jul 09 2019 - harbottle@room3d3.com - 5.12.3-1
  - Bump version

* Wed Jul 03 2019 - harbottle@room3d3.com - 5.12.2-1
  - Bump version

* Fri Jun 28 2019 - harbottle@room3d3.com - 5.12.1-1
  - Bump version

* Sat Jun 15 2019 - harbottle@room3d3.com - 5.12.0-1
  - Bump version

* Mon Jun 03 2019 - harbottle@room3d3.com - 5.11.0-1
  - Bump version

* Tue Apr 16 2019 - harbottle@room3d3.com - 5.10.0-1
  - Bump version

* Fri Mar 15 2019 - harbottle@room3d3.com - 5.9.0-1
  - Bump version

* Sat Feb 16 2019 - harbottle@room3d3.com - 5.8.0-1
  - Bump version

* Fri Feb 15 2019 - harbottle@room3d3.com - 5.7.1-1
  - Initial package
