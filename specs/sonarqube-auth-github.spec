%define __jar_repack 0

Name:     sonarqube-auth-github
Version:  1.5.0.870
Release:  3%{?dist}.harbottle
Summary:  SonarQube GitHub authentication plugin
Group:    Applications/System
License:  LGPL-3.0
Source0:  https://binaries.sonarsource.com/Distribution/sonar-auth-github-plugin/sonar-auth-github-plugin-%{version}.jar
Autoprov: no

%description
GitHub authentication plugin for SonarQube.

%prep

%install
install -d -m 755 $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins
mv %{SOURCE0} $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins

%pre
rm -f %{_var}/lib/sonarqube/extensions/plugins/sonar-auth-github-plugin-*.jar
getent group sonarqube >/dev/null || groupadd -f -r sonarqube
getent passwd sonarqube >/dev/null || useradd -r -g sonarqube -d /usr/share/sonarqube -s /sbin/nologin -c "SonarQube user" sonarqube
exit 0

%files
%attr(0644,sonarqube,sonarqube) %{_var}/lib/sonarqube/extensions/plugins/sonar-auth-github-plugin-%{version}.jar

%changelog
* Tue Dec 10 2019 - harbottle@room3d3.com - 1.5.0.870-3
  - Spec file changes for el8

* Sun Jul 21 2019 - harbottle@room3d3.com - 1.5.0.870-2
  - Fix plugin ownership

* Sun Jul 21 2019 - harbottle@room3d3.com - 1.5.0.870-1
  - Initial package
