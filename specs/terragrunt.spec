Name:    terragrunt
Version: 0.38.4
Release: 1%{?dist}.harbottle
Summary: Terragrunt is a thin wrapper for Terraform
Group:   Applications/System
License: MIT
URL:     https://terragrunt.gruntwork.io/
Source0: https://github.com/gruntwork-io/%{name}/archive/v%{version}.tar.gz
Source1: https://github.com/gruntwork-io/%{name}/releases/download/v%{version}/%{name}_linux_amd64

%description
Terragrunt is a thin wrapper for Terraform that provides extra tools for keeping
your Terraform configurations DRY, working with multiple Terraform modules, and
managing remote state.

%prep
%setup -q

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 0755 %{SOURCE1} $RPM_BUILD_ROOT%{_bindir}/%{name}

%files
%license LICENSE.txt
%doc README.md
%{_bindir}/%{name}

%changelog
* Tue Jul 05 2022 - harbottle@room3d3.com - 0.38.4-1
  - Bump version

* Thu Jun 30 2022 - harbottle@room3d3.com - 0.38.3-1
  - Bump version

* Tue Jun 28 2022 - harbottle@room3d3.com - 0.38.2-1
  - Bump version

* Wed Jun 22 2022 - harbottle@room3d3.com - 0.38.1-1
  - Bump version

* Fri Jun 17 2022 - harbottle@room3d3.com - 0.38.0-1
  - Bump version

* Tue Jun 14 2022 - harbottle@room3d3.com - 0.37.4-1
  - Bump version

* Fri Jun 10 2022 - harbottle@room3d3.com - 0.37.3-1
  - Bump version

* Wed Jun 08 2022 - harbottle@room3d3.com - 0.37.2-1
  - Bump version

* Fri May 13 2022 - harbottle@room3d3.com - 0.37.1-1
  - Bump version

* Wed May 11 2022 - harbottle@room3d3.com - 0.37.0-1
  - Bump version

* Tue May 10 2022 - harbottle@room3d3.com - 0.36.11-1
  - Bump version

* Wed May 04 2022 - harbottle@room3d3.com - 0.36.10-1
  - Bump version

* Fri Apr 29 2022 - harbottle@room3d3.com - 0.36.9-1
  - Bump version

* Wed Apr 27 2022 - harbottle@room3d3.com - 0.36.8-1
  - Bump version

* Fri Apr 15 2022 - harbottle@room3d3.com - 0.36.7-1
  - Bump version

* Thu Mar 17 2022 - harbottle@room3d3.com - 0.36.6-1
  - Bump version

* Wed Mar 16 2022 - harbottle@room3d3.com - 0.36.5-1
  - Bump version

* Thu Mar 03 2022 - harbottle@room3d3.com - 0.36.3-1
  - Bump version

* Fri Feb 25 2022 - harbottle@room3d3.com - 0.36.2-1
  - Bump version

* Mon Jan 31 2022 - harbottle@room3d3.com - 0.36.1-1
  - Bump version

* Thu Jan 20 2022 - harbottle@room3d3.com - 0.36.0-1
  - Bump version

* Sat Jan 15 2022 - harbottle@room3d3.com - 0.35.20-1
  - Bump version

* Sun Dec 12 2021 - harbottle@room3d3.com - 0.35.14-1
  - Bump version

* Mon Nov 01 2021 - harbottle@room3d3.com - 0.35.6-1
  - Bump version

* Thu Oct 28 2021 - harbottle@room3d3.com - 0.35.5-1
  - Bump version

* Tue Oct 19 2021 - harbottle@room3d3.com - 0.35.4-1
  - Bump version

* Thu Oct 14 2021 - harbottle@room3d3.com - 0.35.3-1
  - Bump version

* Wed Oct 13 2021 - harbottle@room3d3.com - 0.35.2-1
  - Bump version

* Wed Oct 13 2021 - harbottle@room3d3.com - 0.35.1-1
  - Bump version

* Mon Oct 11 2021 - harbottle@room3d3.com - 0.34.3-1
  - Bump version

* Mon Oct 11 2021 - harbottle@room3d3.com - 0.34.2-1
  - Bump version

* Wed Oct 06 2021 - harbottle@room3d3.com - 0.34.1-1
  - Bump version

* Fri Oct 01 2021 - harbottle@room3d3.com - 0.34.0-1
  - Bump version

* Thu Sep 30 2021 - harbottle@room3d3.com - 0.33.2-1
  - Bump version

* Wed Sep 29 2021 - harbottle@room3d3.com - 0.33.1-1
  - Bump version

* Tue Sep 28 2021 - harbottle@room3d3.com - 0.33.0-1
  - Bump version

* Tue Sep 28 2021 - harbottle@room3d3.com - 0.32.6-1
  - Bump version

* Thu Sep 23 2021 - harbottle@room3d3.com - 0.32.4-1
  - Bump version

* Mon Sep 20 2021 - harbottle@room3d3.com - 0.32.3-1
  - Bump version

* Thu Sep 16 2021 - harbottle@room3d3.com - 0.32.2-1
  - Bump version

* Tue Sep 14 2021 - harbottle@room3d3.com - 0.32.1-1
  - Bump version

* Tue Sep 14 2021 - harbottle@room3d3.com - 0.32.0-1
  - Bump version

* Mon Sep 13 2021 - harbottle@room3d3.com - 0.31.11-1
  - Bump version

* Thu Sep 09 2021 - harbottle@room3d3.com - 0.31.10-1
  - Bump version

* Thu Sep 09 2021 - harbottle@room3d3.com - 0.31.9-1
  - Bump version

* Mon Aug 30 2021 - harbottle@room3d3.com - 0.31.8-1
  - Bump version

* Tue Aug 24 2021 - harbottle@room3d3.com - 0.31.7-1
  - Bump version

* Mon Aug 23 2021 - harbottle@room3d3.com - 0.31.6-1
  - Bump version

* Wed Aug 18 2021 - harbottle@room3d3.com - 0.31.5-1
  - Bump version

* Fri Aug 13 2021 - harbottle@room3d3.com - 0.31.4-1
  - Bump version

* Fri Jul 30 2021 - harbottle@room3d3.com - 0.31.3-1
  - Bump version

* Thu Jul 29 2021 - harbottle@room3d3.com - 0.31.2-1
  - Bump version

* Tue Jul 20 2021 - harbottle@room3d3.com - 0.31.1-1
  - Bump version

* Tue Jun 22 2021 - harbottle@room3d3.com - 0.31.0-1
  - Bump version

* Sat Jun 19 2021 - harbottle@room3d3.com - 0.30.7-1
  - Bump version

* Fri Jun 18 2021 - harbottle@room3d3.com - 0.30.6-1
  - Bump version

* Thu Jun 17 2021 - harbottle@room3d3.com - 0.30.4-1
  - Bump version

* Tue Jun 15 2021 - harbottle@room3d3.com - 0.30.3-1
  - Bump version

* Mon Jun 07 2021 - harbottle@room3d3.com - 0.29.10-1
  - Bump version

* Mon Jun 07 2021 - harbottle@room3d3.com - 0.29.9-1
  - Bump version

* Wed Jun 02 2021 - harbottle@room3d3.com - 0.29.8-1
  - Bump version

* Fri May 28 2021 - harbottle@room3d3.com - 0.29.7-1
  - Bump version

* Tue May 25 2021 - harbottle@room3d3.com - 0.29.6-1
  - Bump version

* Mon May 24 2021 - harbottle@room3d3.com - 0.29.5-1
  - Bump version

* Thu May 20 2021 - harbottle@room3d3.com - 0.29.4-1
  - Bump version

* Sat May 15 2021 - harbottle@room3d3.com - 0.29.3-1
  - Bump version

* Mon May 03 2021 - harbottle@room3d3.com - 0.29.2-1
  - Bump version

* Wed Apr 28 2021 - harbottle@room3d3.com - 0.29.1-1
  - Bump version

* Fri Apr 23 2021 - harbottle@room3d3.com - 0.29.0-1
  - Bump version

* Fri Apr 16 2021 - harbottle@room3d3.com - 0.28.24-1
  - Bump version

* Fri Apr 16 2021 - harbottle@room3d3.com - 0.28.23-1
  - Bump version

* Thu Apr 15 2021 - harbottle@room3d3.com - 0.28.22-1
  - Bump version

* Tue Apr 13 2021 - harbottle@room3d3.com - 0.28.21-1
  - Bump version

* Mon Apr 12 2021 - harbottle@room3d3.com - 0.28.20-1
  - Bump version

* Thu Apr 08 2021 - harbottle@room3d3.com - 0.28.19-1
  - Bump version

* Mon Mar 29 2021 - harbottle@room3d3.com - 0.28.18-1
  - Bump version

* Wed Mar 24 2021 - harbottle@room3d3.com - 0.28.16-1
  - Bump version

* Wed Mar 17 2021 - harbottle@room3d3.com - 0.28.15-1
  - Bump version

* Tue Mar 16 2021 - harbottle@room3d3.com - 0.28.14-1
  - Bump version

* Tue Mar 16 2021 - harbottle@room3d3.com - 0.28.13-1
  - Bump version

* Mon Mar 15 2021 - harbottle@room3d3.com - 0.28.12-1
  - Bump version

* Fri Mar 12 2021 - harbottle@room3d3.com - 0.28.11-1
  - Bump version

* Fri Mar 12 2021 - harbottle@room3d3.com - 0.28.10-1
  - Bump version

* Thu Mar 11 2021 - harbottle@room3d3.com - 0.28.9-1
  - Bump version

* Thu Feb 18 2021 - harbottle@room3d3.com - 0.28.7-1
  - Bump version

* Wed Feb 17 2021 - harbottle@room3d3.com - 0.28.6-1
  - Bump version

* Tue Feb 16 2021 - harbottle@room3d3.com - 0.28.5-1
  - Bump version

* Fri Feb 12 2021 - harbottle@room3d3.com - 0.28.4-1
  - Bump version

* Wed Feb 10 2021 - harbottle@room3d3.com - 0.28.3-1
  - Bump version

* Thu Feb 04 2021 - harbottle@room3d3.com - 0.28.2-1
  - Bump version

* Tue Feb 02 2021 - harbottle@room3d3.com - 0.28.1-1
  - Bump version

* Tue Feb 02 2021 - harbottle@room3d3.com - 0.28.0-1
  - Bump version

* Mon Feb 01 2021 - harbottle@room3d3.com - 0.27.4-1
  - Bump version

* Tue Jan 26 2021 - harbottle@room3d3.com - 0.27.3-1
  - Bump version

* Mon Jan 25 2021 - harbottle@room3d3.com - 0.27.2-1
  - Bump version

* Tue Jan 12 2021 - harbottle@room3d3.com - 0.27.1-1
  - Bump version

* Fri Jan 08 2021 - harbottle@room3d3.com - 0.27.0-1
  - Bump version

* Wed Nov 25 2020 - harbottle@room3d3.com - 0.26.7-1
  - Bump version

* Wed Nov 25 2020 - harbottle@room3d3.com - 0.26.6-1
  - Bump version

* Wed Nov 25 2020 - harbottle@room3d3.com - 0.26.5-1
  - Bump version

* Fri Nov 20 2020 - harbottle@room3d3.com - 0.26.4-1
  - Bump version

* Fri Nov 06 2020 - harbottle@room3d3.com - 0.26.2-1
  - Bump version

* Mon Nov 02 2020 - harbottle@room3d3.com - 0.26.0-1
  - Bump version

* Fri Oct 23 2020 - harbottle@room3d3.com - 0.25.5-1
  - Bump version

* Fri Oct 16 2020 - harbottle@room3d3.com - 0.25.4-1
  - Bump version

* Fri Oct 09 2020 - harbottle@room3d3.com - 0.25.3-1
  - Bump version

* Wed Sep 30 2020 - harbottle@room3d3.com - 0.25.2-1
  - Bump version

* Sun Sep 20 2020 - harbottle@room3d3.com - 0.25.1-1
  - Bump version

* Sat Sep 19 2020 - harbottle@room3d3.com - 0.25.0-1
  - Bump version

* Wed Sep 16 2020 - harbottle@room3d3.com - 0.24.4-1
  - Bump version

* Tue Sep 15 2020 - harbottle@room3d3.com - 0.24.3-1
  - Bump version

* Thu Sep 10 2020 - harbottle@room3d3.com - 0.24.1-1
  - Bump version

* Tue Sep 08 2020 - harbottle@room3d3.com - 0.24.0-1
  - Bump version

* Wed Sep 02 2020 - harbottle@room3d3.com - 0.23.40-1
  - Bump version

* Fri Aug 28 2020 - harbottle@room3d3.com - 0.23.38-1
  - Bump version

* Thu Aug 27 2020 - harbottle@room3d3.com - 0.23.37-1
  - Bump version

* Wed Aug 26 2020 - harbottle@room3d3.com - 0.23.36-1
  - Bump version

* Tue Aug 25 2020 - harbottle@room3d3.com - 0.23.35-1
  - Bump version

* Mon Aug 24 2020 - harbottle@room3d3.com - 0.23.34-1
  - Bump version

* Mon Aug 03 2020 - harbottle@room3d3.com - 0.23.33-1
  - Bump version

* Fri Jul 31 2020 - harbottle@room3d3.com - 0.23.32-1
  - Bump version

* Wed Jul 01 2020 - harbottle@room3d3.com - 0.23.31-1
  - Bump version

* Mon Jun 29 2020 - harbottle@room3d3.com - 0.23.30-1
  - Bump version

* Thu Jun 25 2020 - harbottle@room3d3.com - 0.23.29-1
  - Bump version

* Tue Jun 23 2020 - harbottle@room3d3.com - 0.23.28-1
  - Bump version

* Sun Jun 14 2020 - harbottle@room3d3.com - 0.23.27-1
  - Bump version

* Fri Jun 12 2020 - harbottle@room3d3.com - 0.23.26-1
  - Bump version

* Thu Jun 11 2020 - harbottle@room3d3.com - 0.23.25-1
  - Bump version

* Mon Jun 08 2020 - harbottle@room3d3.com - 0.23.24-1
  - Bump version

* Fri May 29 2020 - harbottle@room3d3.com - 0.23.23-1
  - Bump version

* Thu May 28 2020 - harbottle@room3d3.com - 0.23.22-1
  - Bump version

* Thu May 28 2020 - harbottle@room3d3.com - 0.23.21-1
  - Bump version

* Sat May 23 2020 - harbottle@room3d3.com - 0.23.20-1
  - Bump version

* Fri May 15 2020 - harbottle@room3d3.com - 0.23.18-1
  - Bump version

* Tue May 12 2020 - harbottle@room3d3.com - 0.23.17-1
  - Bump version

* Mon May 11 2020 - harbottle@room3d3.com - 0.23.16-1
  - Bump version

* Fri May 08 2020 - harbottle@room3d3.com - 0.23.15-1
  - Bump version

* Tue May 05 2020 - harbottle@room3d3.com - 0.23.14-1
  - Bump version

* Thu Apr 30 2020 - harbottle@room3d3.com - 0.23.13-1
  - Bump version

* Mon Apr 27 2020 - harbottle@room3d3.com - 0.23.12-1
  - Bump version

* Mon Apr 27 2020 - harbottle@room3d3.com - 0.23.11-1
  - Bump version

* Thu Apr 16 2020 - harbottle@room3d3.com - 0.23.10-1
  - Bump version

* Wed Apr 15 2020 - harbottle@room3d3.com - 0.23.9-1
  - Bump version

* Wed Apr 08 2020 - harbottle@room3d3.com - 0.23.8-1
  - Bump version

* Wed Apr 08 2020 - harbottle@room3d3.com - 0.23.7-1
  - Bump version

* Mon Apr 06 2020 - harbottle@room3d3.com - 0.23.6-1
  - Bump version

* Sat Apr 04 2020 - harbottle@room3d3.com - 0.23.5-1
  - Bump version

* Sun Mar 29 2020 - harbottle@room3d3.com - 0.23.4-1
  - Bump version

* Sat Mar 28 2020 - harbottle@room3d3.com - 0.23.3-1
  - Bump version

* Tue Mar 10 2020 - harbottle@room3d3.com - 0.23.2-1
  - Bump version

* Mon Mar 09 2020 - harbottle@room3d3.com - 0.23.1-1
  - Bump version

* Tue Mar 03 2020 - harbottle@room3d3.com - 0.23.0-1
  - Bump version

* Sat Feb 29 2020 - harbottle@room3d3.com - 0.22.5-1
  - Bump version

* Wed Feb 12 2020 - harbottle@room3d3.com - 0.21.13-1
  - Bump version

* Mon Feb 10 2020 - harbottle@room3d3.com - 0.21.12-1
  - Bump version

* Sat Jan 18 2020 - harbottle@room3d3.com - 0.21.11-1
  - Initial package
