%global git_owner commercialhaskell
%global git_repo stack
%global git_archive %{git_repo}-%{version}-linux-x86_64

Name:     %{git_repo}
Version:  2.7.5
Release:  1%{?dist}.harbottle
Summary:  The Haskell Tool Stack
License:  BSD
Url:      https://github.com/%{git_owner}/%{git_repo}
Source0:  %{url}/releases/download/v%{version}/%{git_archive}.tar.gz
Requires: perl make automake gcc gmp-devel libffi zlib xz tar git

%description
Stack is a cross-platform program for developing Haskell projects.
It is intended for Haskellers both new and experienced.

See haskellstack.org or the doc directory for more information.

%prep
%setup -q -n %{git_archive}

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 755 %{name} $RPM_BUILD_ROOT%{_bindir}/%{name}

%files
%doc CONTRIBUTING.md ChangeLog.md LICENSE README.md doc/*
%{_bindir}/%{name}

%changelog
* Sun Mar 06 2022 - harbottle@room3d3.com - 2.7.5-1
  - Bump version

* Tue Jul 20 2021 - harbottle@room3d3.com - 2.7.3-1
  - Bump version

* Fri May 07 2021 - harbottle@room3d3.com - 2.7.1-1
  - Bump version

* Fri Oct 16 2020 - harbottle@room3d3.com - 2.5.1-1
  - Bump version

* Wed Aug 05 2020 - harbottle@room3d3.com - 2.3.3-1
  - Bump version

* Wed Apr 29 2020 - harbottle@room3d3.com - 2.3.1-1
  - Bump version

* Sat Jul 13 2019 - harbottle@room3d3.com - 2.1.3-1
  - Bump version

* Thu Jun 13 2019 - harbottle@room3d3.com - 2.1.1-1
  - Bump version

* Wed Jan 02 2019 - harbottle@room3d3.com - 1.9.3-1
  - Initial package
