%global gem_name backports

Name:          rubygem-%{gem_name}
Version:       3.21.0
Release:       1%{?dist}.harbottle
Summary:       Backports for earlier ruby versions
Group:         Applications/System
License:       MIT
URL:           https://github.com/marcandre/backports
Source0:       https://rubygems.org/gems/%{gem_name}-%{version}.gem
Requires:      ruby(release)
Requires:      ruby(rubygems)
Requires:      ruby
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: ruby
BuildArch:     noarch

%description
Essential backports that enable many of the nice features of Ruby for earlier
versions.

%package doc
Summary: Documentation for %{name}
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
This package contains documentation for %{name}.

%prep
%setup -q -n %{gem_name}-%{version}

%build
gem build ../%{gem_name}-%{version}.gemspec
%gem_install
%install
mkdir -p %{buildroot}%{gem_dir}
cp -pa .%{gem_dir}/* %{buildroot}%{gem_dir}/

%check

%files
%exclude %{gem_instdir}/.*
%doc %{gem_instdir}/README.md
%license %{gem_instdir}/LICENSE.txt
%doc %{gem_instdir}/CHANGELOG.md
%doc %{gem_instdir}/SECURITY.md
%dir %{gem_instdir}
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%{gem_instdir}/%{gem_name}.gemspec
%{gem_instdir}/Gemfile

%changelog
* Sun May 16 2021 - harbottle@room3d3.com - 3.21.0-1
  - Initial packaging
