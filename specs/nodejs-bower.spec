%global modname bower

Name:           nodejs-bower
Version:        1.8.14
Release:        1%{?dist}.harbottle
Summary:        A package manager for the web
Group:          Applications/System
License:        MIT
Url:            https://%{modname}.io
Source0:        https://github.com/%{modname}/%{modname}/archive/refs/tags/%{version}.tar.gz
BuildRequires:  nodejs >= 0.10.0
Requires:       nodejs >= 0.10.0
Requires:       git

%description
Bower offers a generic, unopinionated solution to the problem of front-end
package management, while exposing the package dependency model via an API that
can be consumed by a more opinionated build stack. There are no system wide
dependencies, no dependencies are shared between different apps, and the
dependency tree is flat.

Bower runs over Git, and is package-agnostic. A packaged component can be made
up of any type of asset, and use any type of transport (e.g., AMD, CommonJS,
etc.).

%prep
%setup -q -n %{modname}-%{version}

%build
npm install -g --prefix=%{buildroot}/usr %{modname}@%{version}

%files
%license LICENSE
%doc *.md
/usr/lib/node_modules/%{modname}
%{_bindir}/%{modname}

%changelog
* Mon Mar 14 2022 - harbottle@room3d3.com - 1.8.14-1
  - Fix build
  - Bump version

* Sun Dec 12 2021 - harbottle@room3d3.com - 1.8.13-1
  - Bump version

* Mon Jan 18 2021 - harbottle@room3d3.com - 1.8.12-1
  - Bump version

* Thu Jan 14 2021 - harbottle@room3d3.com - 1.8.10-1
  - Bump version

* Sat Jan 04 2020 - harbottle@room3d3.com - 1.8.8-2
  - Build for el8
  - Tidy spec file
  - Create seperate bower package

* Fri Feb 01 2019 - harbottle@room3d3.com - 1.8.8-1
  - Bump version
  - Standardize spec for automation

* Fri Jan 18 2019 - harbottle@room3d3.com - 1.8.7.20190117git4f68fc7-1
  - Bump version

* Mon Aug 13 2018 grainger@gmail.com - 1.8.4.20180328git1c15dea-1
- Bump version
* Mon Apr 16 2018 grainger@gmail.com
- v1.8.4
* Fri Sep 29 2017 grainger@gmail.com
- v1.8.2
* Mon Jun 12 2017 grainger@gmail.com
- Initial packaging
