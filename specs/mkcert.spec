Name:          mkcert
Version:       1.4.4
Release:       1%{?dist}.harbottle
Summary:       A simple zero-config tool to make locally trusted development certificates with any names you would like
Group:         Applications/System
License:       BSD 3
Url:           https://github.com/FiloSottile/%{name}
Source0:       %{url}/archive/v%{version}.tar.gz
Source1:       %{url}/releases/download/v%{version}/%{name}-v%{version}-linux-amd64
Requires:      nss-tools

%description
mkcert is a simple tool for making locally-trusted development certificates. It
requires no configuration.

Using certificates from real certificate authorities (CAs) for development can
be dangerous or impossible (for hosts like localhost or 127.0.0.1), but
self-signed certificates cause trust errors. Managing your own CA is the best
solution, but usually involves arcane commands, specialized knowledge and manual steps.

mkcert automatically creates and installs a local CA in the system root store,
and generates locally-trusted certificates. mkcert does not automatically
configure servers to use the certificates, though, that's up to you.

%prep
%setup -q

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 755 %{SOURCE1} $RPM_BUILD_ROOT%{_bindir}/%{name}

%files
%license LICENSE
%doc {AUTHORS,README.md}
%{_bindir}/%{name}

%changelog
* Wed Apr 27 2022 - harbottle@room3d3.com - 1.4.4-1
  - Fix build
  - Bump version

* Wed Nov 25 2020 - harbottle@room3d3.com - 1.4.3-1
  - Bump version

* Mon Oct 26 2020 - harbottle@room3d3.com - 1.4.2-1
  - Bump version

* Sun Nov 10 2019 - harbottle@room3d3.com - 1.4.1-1
  - Bump version

* Fri Aug 16 2019 - harbottle@room3d3.com - 1.4.0-1
  - Bump version

* Sun Feb 03 2019 - harbottle@room3d3.com - 1.3.0-1
  - Bump version

* Wed Jan 09 2019 - harbottle@room3d3.com - 1.2.0-1
  - Initial package
