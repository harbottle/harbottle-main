Name:           microsoft-release
Version:        1
Release:        1.el7
Summary:        Microsoft repository configuration
Group:          System Environment/Base
License:        MIT
URL:            https://packages.microsoft.com/rhel/7/prod/
Source0:        RPM-GPG-KEY-microsoft
Source1:        https://gitlab.com/snippets/1801438/raw
Source2:        microsoft.repo

BuildArch:     noarch
Requires:      redhat-release >=  7

%description
This package contains the Microsoft repository
GPG key as well as configuration for yum.

%prep
%setup -q  -c -T
install -pm 644 %{SOURCE0} .
install -pm 644 %{SOURCE1} ./MIT

%build

%install
rm -rf $RPM_BUILD_ROOT
install -Dpm 644 %{SOURCE0} $RPM_BUILD_ROOT%{_sysconfdir}/pki/rpm-gpg/RPM-GPG-KEY-microsoft
install -dm 755 $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d
install -pm 644 %{SOURCE2} $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc MIT
%config(noreplace) /etc/yum.repos.d/*
/etc/pki/rpm-gpg/*

%changelog
* Sat Jul 14 2018 <grainger@gmail.com> -  1-1.el7
- Initial packaging
