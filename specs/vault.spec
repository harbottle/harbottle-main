%global confdir %{_sysconfdir}/%{name}.d
%global datadir %{_var}/lib/%{name}/

Name:             vault
Version:          1.11.0
Release:          1%{?dist}.harbottle
Summary:          Vault is a tool for securely accessing secrets
Group:            Applications/System
License:          MPL-2.0
URL:              https://www.vaultproject.io
Source0:          https://github.com/hashicorp/%{name}/archive/v%{version}.tar.gz
Source1:          https://releases.hashicorp.com/%{name}/%{version}/%{name}_%{version}_linux_amd64.zip
Source2:          %{name}.hcl
Source3:          %{name}.service
Requires(pre):    shadow-utils
Requires(post):   systemd-units
Requires(preun):  systemd-units
Requires(postun): systemd-units
%if 0%{?rhel} == 8
BuildRequires:    systemd-rpm-macros
%endif

%description
Vault is a tool for securely accessing secrets. A secret is anything that you
want to tightly control access to, such as API keys, passwords, certificates,
and more. Vault provides a unified interface to any secret, while providing
tight access control and recording a detailed audit log.

A modern system requires access to a multitude of secrets: database credentials,
API keys for external services, credentials for service-oriented architecture
communication, etc. Understanding who is accessing what secrets is already very
difficult and platform-specific. Adding on key rolling, secure storage, and
detailed audit logs is almost impossible without a custom solution. This is
where Vault steps in.

%prep
%setup -q
rm -rf vault
%setup -q -T -D -a 1

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -d -m 755 $RPM_BUILD_ROOT%{confdir}
install -d -m 755 $RPM_BUILD_ROOT%{datadir}
install -d -m 755 $RPM_BUILD_ROOT%{_unitdir}

install -m 0755 %{name} $RPM_BUILD_ROOT%{_bindir}
install -m 0640 %{SOURCE2} $RPM_BUILD_ROOT%{confdir}
install -m 0644 %{SOURCE3} $RPM_BUILD_ROOT%{_unitdir}

%pre
getent group %{name} >/dev/null || groupadd -f -r %{name}
getent passwd %{name} >/dev/null || useradd -r -g %{name} -d %{confdir} -s /sbin/nologin -c "%{name} service user" %{name}
exit 0

%post
%systemd_post %{name}.service

%preun
%systemd_preun %{name}.service

%postun
%systemd_postun_with_restart %{name}.service

%files
%license LICENSE
%doc *.md
%caps(cap_ipc_lock=+ep) %{_bindir}/%{name}
%attr(-,%{name},%{name}) %config(noreplace) %{confdir}
%attr(-,%{name},%{name}) %{datadir}
%{_unitdir}/%{name}.service

%changelog
* Mon Jun 20 2022 - harbottle@room3d3.com - 1.11.0-1
  - Bump version

* Fri Jun 10 2022 - harbottle@room3d3.com - 1.10.4-1
  - Bump version

* Wed May 11 2022 - harbottle@room3d3.com - 1.10.3-1
  - Bump version

* Fri Apr 29 2022 - harbottle@room3d3.com - 1.10.2-1
  - Bump version

* Fri Apr 22 2022 - harbottle@room3d3.com - 1.10.1-1
  - Bump version

* Wed Mar 23 2022 - harbottle@room3d3.com - 1.10.0-1
  - Bump version

* Fri Mar 04 2022 - harbottle@room3d3.com - 1.9.4-1
  - Bump version

* Thu Jan 27 2022 - harbottle@room3d3.com - 1.9.3-1
  - Bump version

* Sat Jan 15 2022 - harbottle@room3d3.com - 1.9.2-1
  - Bump version

* Sun Dec 12 2021 - harbottle@room3d3.com - 1.9.1-1
  - Bump version

* Sun Dec 12 2021 - harbottle@room3d3.com - 1.9.0-1
  - Fix build
  - Bump version

* Wed Oct 06 2021 - harbottle@room3d3.com - 1.8.4-1
  - Fix
  - Bump version

* Wed Sep 29 2021 - harbottle@room3d3.com - 1.8.3-1
  - Bump version

* Thu Aug 26 2021 - harbottle@room3d3.com - 1.8.2-1
  - Bump version

* Fri Aug 06 2021 - harbottle@room3d3.com - 1.8.1-1
  - Bump version

* Wed Jul 28 2021 - harbottle@room3d3.com - 1.8.0-1
  - Bump version

* Wed Jun 16 2021 - harbottle@room3d3.com - 1.7.3-1
  - Bump version

* Fri May 21 2021 - harbottle@room3d3.com - 1.7.2-1
  - Bump version

* Wed Apr 21 2021 - harbottle@room3d3.com - 1.7.1-1
  - Bump version

* Wed Mar 24 2021 - harbottle@room3d3.com - 1.7.0-1
  - Bump version

* Thu Feb 25 2021 - harbottle@room3d3.com - 1.6.3-1
  - Bump version

* Fri Jan 29 2021 - harbottle@room3d3.com - 1.6.2-1
  - Bump version

* Fri Dec 18 2020 - harbottle@room3d3.com - 1.6.1-1
  - Bump version

* Wed Nov 11 2020 - harbottle@room3d3.com - 1.6.0-1
  - Bump version

* Wed Oct 21 2020 - harbottle@room3d3.com - 1.5.5-1
  - Bump version

* Fri Sep 25 2020 - harbottle@room3d3.com - 1.5.4-1
  - Bump version

* Fri Aug 28 2020 - harbottle@room3d3.com - 1.5.3-1
  - Bump version

* Mon Aug 24 2020 - harbottle@room3d3.com - 1.5.2-1
  - Bump version

* Fri Jul 24 2020 - harbottle@room3d3.com - 1.5.0-1
  - Bump version

* Thu Jul 02 2020 - harbottle@room3d3.com - 1.4.3-1
  - Bump version

* Thu May 21 2020 - harbottle@room3d3.com - 1.4.2-1
  - Bump version

* Thu Apr 30 2020 - harbottle@room3d3.com - 1.4.1-1
  - Bump version

* Tue Apr 07 2020 - harbottle@room3d3.com - 1.4.0-1
  - Bump version

* Thu Mar 19 2020 - harbottle@room3d3.com - 1.3.4-1
  - Bump version

* Thu Mar 05 2020 - harbottle@room3d3.com - 1.3.3-1
  - Bump version

* Wed Jan 22 2020 - harbottle@room3d3.com - 1.3.2-1
  - Bump version

* Thu Dec 19 2019 - harbottle@room3d3.com - 1.3.1-1
  - Bump version

* Fri Dec 13 2019 - harbottle@room3d3.com - 1.3.0-2
  - Tidy-up spec file
  - Build for el8

* Thu Nov 14 2019 - harbottle@room3d3.com - 1.3.0-1
  - Bump version

* Thu Nov 07 2019 - harbottle@room3d3.com - 1.2.4-1
  - Bump version

* Fri Sep 13 2019 - harbottle@room3d3.com - 1.2.3-1
  - Bump version

* Thu Aug 15 2019 - harbottle@room3d3.com - 1.2.2-1
  - Bump version

* Tue Aug 06 2019 - harbottle@room3d3.com - 1.2.1-1
  - Bump version

* Tue Jul 30 2019 - harbottle@room3d3.com - 1.2.0-1
  - Bump version

* Thu Jul 25 2019 - harbottle@room3d3.com - 1.1.4-1
  - Bump version

* Wed Jun 05 2019 - harbottle@room3d3.com - 1.1.3-1
  - Bump version

* Thu Apr 18 2019 - harbottle@room3d3.com - 1.1.2-1
  - Bump version

* Thu Apr 11 2019 - harbottle@room3d3.com - 1.1.1-1
  - Bump version

* Mon Mar 18 2019 - harbottle@room3d3.com - 1.1.0-1
  - Bump version

* Tue Feb 12 2019 - harbottle@room3d3.com - 1.0.3-1
  - Bump version

* Sat Feb 02 2019 - harbottle@room3d3.com - 1.0.2-1
  - Initial package
