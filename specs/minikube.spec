%define debug_package %{nil}

Name:    minikube
Version: 1.26.0
Release: 1%{?dist}.harbottle
Summary: Minikube is a tool that makes it easy to run Kubernetes locally
Group:   Applications/System
License: Apache 2.0
Url:     https://github.com/kubernetes/%{name}
Source0: %{url}/archive/v%{version}.tar.gz
Source1: %{url}/releases/download/v%{version}/%{name}-linux-amd64

%prep
%setup -q -n %{name}-%{version}

%description
Minikube is a tool that makes it easy to run Kubernetes locally. Minikube runs a
single-node Kubernetes cluster inside a VM on your laptop for users looking to
try out Kubernetes or develop with it day-to-day.

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 755 %{SOURCE1} $RPM_BUILD_ROOT%{_bindir}/%{name}

%files
%license LICENSE
%doc {*.md,OWNERS,SECURITY_CONTACTS}
%{_bindir}/%{name}

%changelog
* Thu Jun 23 2022 - harbottle@room3d3.com - 1.26.0-1
  - Bump version

* Fri Feb 25 2022 - harbottle@room3d3.com - 1.25.2-1
  - Bump version

* Fri Jan 21 2022 - harbottle@room3d3.com - 1.25.1-1
  - Bump version

* Thu Jan 20 2022 - harbottle@room3d3.com - 1.25.0-1
  - Bump version

* Sun Dec 12 2021 - harbottle@room3d3.com - 1.24.0-1
  - Bump version

* Wed Sep 22 2021 - harbottle@room3d3.com - 1.23.2-1
  - Bump version

* Fri Sep 17 2021 - harbottle@room3d3.com - 1.23.1-1
  - Bump version

* Fri Sep 03 2021 - harbottle@room3d3.com - 1.23.0-1
  - Bump version

* Wed Jul 07 2021 - harbottle@room3d3.com - 1.22.0-1
  - Bump version

* Tue Jun 15 2021 - harbottle@room3d3.com - 1.21.0-1
  - Bump version

* Thu May 06 2021 - harbottle@room3d3.com - 1.20.0-1
  - Bump version

* Sat Apr 10 2021 - harbottle@room3d3.com - 1.19.0-1
  - Bump version

* Thu Mar 04 2021 - harbottle@room3d3.com - 1.18.1-1
  - Bump version

* Tue Mar 02 2021 - harbottle@room3d3.com - 1.18.0-1
  - Bump version

* Thu Jan 28 2021 - harbottle@room3d3.com - 1.17.1-1
  - Bump version

* Fri Jan 22 2021 - harbottle@room3d3.com - 1.17.0-1
  - Bump version

* Fri Dec 18 2020 - harbottle@room3d3.com - 1.16.0-1
  - Bump version

* Fri Nov 20 2020 - harbottle@room3d3.com - 1.15.1-1
  - Bump version

* Tue Oct 27 2020 - harbottle@room3d3.com - 1.14.2-1
  - Bump version

* Sat Oct 24 2020 - harbottle@room3d3.com - 1.14.1-1
  - Bump version

* Mon Oct 12 2020 - harbottle@room3d3.com - 1.14.0-1
  - Bump version

* Mon Sep 21 2020 - harbottle@room3d3.com - 1.13.1-1
  - Bump version

* Thu Sep 03 2020 - harbottle@room3d3.com - 1.13.0-1
  - Bump version

* Thu Aug 13 2020 - harbottle@room3d3.com - 1.12.3-1
  - Bump version

* Wed Aug 05 2020 - harbottle@room3d3.com - 1.12.2-1
  - Bump version

* Fri Jul 24 2020 - harbottle@room3d3.com - 1.12.1-1
  - Bump version

* Thu Jul 09 2020 - harbottle@room3d3.com - 1.12.0-1
  - Bump version

* Fri May 29 2020 - harbottle@room3d3.com - 1.11.0-1
  - Bump version

* Wed May 13 2020 - harbottle@room3d3.com - 1.10.1-1
  - Bump version

* Tue May 12 2020 - harbottle@room3d3.com - 1.10.0-1
  - Bump version
  - Fix build

* Sat Apr 04 2020 - harbottle@room3d3.com - 1.9.2-1
  - Bump version

* Fri Apr 03 2020 - harbottle@room3d3.com - 1.9.1-1
  - Bump version

* Thu Mar 26 2020 - harbottle@room3d3.com - 1.9.0-1
  - Bump version

* Fri Mar 13 2020 - harbottle@room3d3.com - 1.8.2-1
  - Bump version

* Sat Mar 07 2020 - harbottle@room3d3.com - 1.8.1-1
  - Bump version

* Sat Mar 07 2020 - harbottle@room3d3.com - 1.8.0-1
  - Bump version

* Sat Feb 29 2020 - harbottle@room3d3.com - 1.7.3-1
  - Bump version
  - Fix build

* Sat Feb 08 2020 - harbottle@room3d3.com - 1.7.2-1
  - Bump version

* Thu Feb 06 2020 - harbottle@room3d3.com - 1.7.1-1
  - Bump version

* Wed Feb 05 2020 - harbottle@room3d3.com - 1.7.0-1
  - Bump version

* Fri Dec 20 2019 - harbottle@room3d3.com - 1.6.2-1
  - Bump version

* Thu Dec 19 2019 - harbottle@room3d3.com - 1.6.1-1
  - Bump version

* Thu Oct 31 2019 - harbottle@room3d3.com - 1.5.2-1
  - Bump version

* Wed Oct 30 2019 - harbottle@room3d3.com - 1.5.1-1
  - Bump version

* Sat Oct 26 2019 - harbottle@room3d3.com - 1.5.0-1
  - Bump version

* Thu Sep 19 2019 - harbottle@room3d3.com - 1.4.0-1
  - Bump version
  - Remove MAINTAINERS file

* Tue Aug 13 2019 - harbottle@room3d3.com - 1.3.1-1
  - Bump version

* Tue Aug 06 2019 - harbottle@room3d3.com - 1.3.0-1
  - Bump version

* Mon Jun 24 2019 - harbottle@room3d3.com - 1.2.0-1
  - Bump version

* Fri Jun 07 2019 - harbottle@room3d3.com - 1.1.1-1
  - Bump version

* Mon Jun 03 2019 - harbottle@room3d3.com - 1.1.0-1
  - Bump version

* Wed Mar 27 2019 - harbottle@room3d3.com - 1.0.0-1
  - Bump version

* Thu Mar 07 2019 - harbottle@room3d3.com - 0.35.0-1
  - Bump version

* Sat Feb 16 2019 - harbottle@room3d3.com - 0.34.1-1
  - Bump version

* Fri Feb 15 2019 - harbottle@room3d3.com - 0.34.0-1
  - Bump version

* Wed Feb 06 2019 - harbottle@room3d3.com - 0.33.1-1
  - Initial package
