%global gem_name flores

Name:          rubygem-%{gem_name}
Version:       0.0.7
Release:       1%{?dist}.harbottle
Summary:       Add fuzzing, randomization, and stress to your tests
Group:         Applications/System
License:       AGPL-3.0
URL:           https://github.com/jordansissel/ruby-flores
Source0:       https://rubygems.org/gems/%{gem_name}-%{version}.gem
Requires:      ruby(release)
Requires:      ruby(rubygems)
Requires:      ruby
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: ruby
BuildArch:     noarch

%description
This library is an exploration to build the tools to let you write tests that
find bugs. In memory of Carlo Flores.

%package doc
Summary: Documentation for %{name}
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
This package contains documentation for %{name}.

%prep
%setup -q -n %{gem_name}-%{version}

%build
gem build ../%{gem_name}-%{version}.gemspec
%gem_install
%install
mkdir -p %{buildroot}%{gem_dir}
cp -pa .%{gem_dir}/* %{buildroot}%{gem_dir}/

%check

%files
%exclude %{gem_instdir}/.*
%doc %{gem_instdir}/README.md
%license %{gem_instdir}/LICENSE.txt
%dir %{gem_instdir}
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%{gem_instdir}/%{gem_name}.gemspec
%{gem_instdir}/Gemfile
%{gem_instdir}/Gemfile.lock
%{gem_instdir}/examples
%{gem_instdir}/Makefile
%{gem_instdir}/spec

%changelog
* Sun May 16 2021 - harbottle@room3d3.com - 0.0.7-1
  - Initial packaging
