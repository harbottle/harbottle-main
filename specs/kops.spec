Name:    kops
Version: 1.24.0
Release: 1%{?dist}.harbottle
Summary: Kubernetes Operations
Group:   Applications/System
License: Apache-2.0
Url:     https://github.com/kubernetes/%{name}
Source0: %{url}/archive/v%{version}.tar.gz
Source1: %{url}/releases/download/v%{version}/%{name}-linux-amd64

%description
kops - Kubernetes Operations

kops helps you create, destroy, upgrade and maintain production-grade, highly
available, Kubernetes clusters from the command line. AWS (Amazon Web Services)
is currently officially supported, with GCE in beta support , and VMware vSphere
in alpha, and other platforms planned.

%prep
%setup -q

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 755 %{SOURCE1} $RPM_BUILD_ROOT%{_bindir}/%{name}

%files
%license LICENSE
%doc *.md
%{_bindir}/%{name}

%changelog
* Fri Jul 08 2022 - harbottle@room3d3.com - 1.24.0-1
  - Bump version

* Wed May 11 2022 - harbottle@room3d3.com - 1.23.2-1
  - Bump version

* Wed Apr 20 2022 - harbottle@room3d3.com - 1.23.1-1
  - Bump version

* Sun Mar 06 2022 - harbottle@room3d3.com - 1.23.0-1
  - Bump version

* Mon Feb 21 2022 - harbottle@room3d3.com - 1.22.4-1
  - Bump version

* Sat Jan 15 2022 - harbottle@room3d3.com - 1.22.3-1
  - Bump version

* Sun Dec 12 2021 - harbottle@room3d3.com - 1.22.2-1
  - Bump version

* Fri Oct 22 2021 - harbottle@room3d3.com - 1.22.1-1
  - Bump version

* Thu Oct 14 2021 - harbottle@room3d3.com - 1.22.0-1
  - Bump version

* Sat Oct 09 2021 - harbottle@room3d3.com - 1.21.2-1
  - Bump version

* Tue Aug 24 2021 - harbottle@room3d3.com - 1.21.1-1
  - Bump version

* Fri Jul 02 2021 - harbottle@room3d3.com - 1.21.0-1
  - Package binary
  - Bump version

* Fri Jun 18 2021 - harbottle@room3d3.com - 1.20.2-1
  - Bump version

* Thu May 13 2021 - harbottle@room3d3.com - 1.20.1-1
  - Bump version

* Sat Apr 10 2021 - harbottle@room3d3.com - 1.20.0-1
  - Bump version

* Fri Feb 19 2021 - harbottle@room3d3.com - 1.19.1-1
  - Bump version

* Fri Jan 29 2021 - harbottle@room3d3.com - 1.19.0-1
  - Bump version

* Thu Jan 28 2021 - harbottle@room3d3.com - 1.18.3-1
  - Bump version

* Sat Oct 24 2020 - harbottle@room3d3.com - 1.18.2-1
  - Bump version

* Thu Sep 10 2020 - harbottle@room3d3.com - 1.18.1-1
  - Bump version

* Mon Aug 03 2020 - harbottle@room3d3.com - 1.18.0-1
  - Bump version
  - Fix download location

* Mon Jul 06 2020 - harbottle@room3d3.com - 1.17.1-1
  - Bump version

* Sun May 31 2020 - harbottle@room3d3.com - 1.17.0-1
  - Bump version

* Sun May 31 2020 - harbottle@room3d3.com - 1.16.3-1
  - Bump version

* Wed May 06 2020 - harbottle@room3d3.com - 1.16.2-1
  - Bump version

* Thu Apr 23 2020 - harbottle@room3d3.com - 1.16.1-1
  - Bump version

* Sat Feb 29 2020 - harbottle@room3d3.com - 1.16.0-1
  - Bump version

* Fri Feb 07 2020 - harbottle@room3d3.com - 1.15.2-1
  - Bump version

* Thu Jan 30 2020 - harbottle@room3d3.com - 1.15.1-1
  - Bump version

* Wed Dec 11 2019 - harbottle@room3d3.com - 1.15.0-1
  - Fix build
  - Tidy spec file
  - Bump version

* Fri Nov 08 2019 - harbottle@room3d3.com - 1.14.1-1
  - Bump version

* Tue Oct 01 2019 - harbottle@room3d3.com - 1.14.0-1
  - Bump version

* Wed Sep 25 2019 - harbottle@room3d3.com - 1.13.2-1
  - Bump version

* Wed Sep 25 2019 - harbottle@room3d3.com - 1.13.1-1
  - Bump version

* Fri Aug 02 2019 - harbottle@room3d3.com - 1.12.3-1
  - Bump version

* Fri Jun 21 2019 - harbottle@room3d3.com - 1.12.2-1
  - Bump version

* Mon Jun 03 2019 - harbottle@room3d3.com - 1.12.1-1
  - Bump version

* Fri Mar 01 2019 - harbottle@room3d3.com - 1.11.1-1
  - Bump version

* Thu Jan 10 2019 - harbottle@room3d3.com - 1.11.0-1
  - Initial package
