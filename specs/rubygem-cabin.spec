%global gem_name cabin

Name:          rubygem-%{gem_name}
Version:       0.8.1
Release:       1%{?dist}.harbottle
Summary:       Improved logging
Group:         Applications/System
License:       Apache-2.0
URL:           https://github.com/jordansissel/ruby-cabin
Source0:       https://rubygems.org/gems/%{gem_name}-%{version}.gem
Requires:      ruby(release)
Requires:      ruby(rubygems)
Requires:      ruby
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: ruby
BuildRequires: rubygem(rake)
BuildArch:     noarch

%description
This is an experiment to try and make logging more flexible and more consumable.
Plain text logs are bullshit, let's emit structured and contextual logs.
Metrics, too!

%package doc
Summary: Documentation for %{name}
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
This package contains documentation for %{name}.

%prep
%setup -q -n %{gem_name}-%{version}

%build
gem build ../%{gem_name}-%{version}.gemspec
%gem_install
%install
mkdir -p %{buildroot}%{gem_dir}
cp -pa .%{gem_dir}/* %{buildroot}%{gem_dir}/

mkdir -p %{buildroot}%{_bindir}
cp -a .%{_bindir}/* %{buildroot}%{_bindir}/

find %{buildroot}%{gem_instdir}/bin -type f | xargs chmod a+x
find %{buildroot}%{_bindir} -type f | xargs chmod a+x

%check

%files
%exclude %{gem_instdir}/.*
%license %{gem_instdir}/LICENSE
%doc %{gem_instdir}/CHANGELIST
%dir %{gem_instdir}
%{gem_libdir}
%{gem_instdir}/bin
%{_bindir}/rubygems-cabin-test
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%{gem_instdir}/examples/
%{gem_instdir}/test/

%changelog
* Sat May 15 2021 - harbottle@room3d3.com - 0.8.1-1
  - Initial packaging
