%global gem_name thor

Name:          rubygem-%{gem_name}
Version:       1.2.1
Release:       1%{?dist}.harbottle
Summary:       Build powerful command-line interfaces
Group:         Applications/System
License:       MIT
URL:           http://whatisthor.com/
Source0:       https://rubygems.org/gems/%{gem_name}-%{version}.gem
Requires:      ruby(release)
Requires:      ruby(rubygems)
Requires:      ruby
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: ruby
BuildArch:     noarch

%description
Thor is a toolkit for building powerful command-line interfaces.

%package doc
Summary: Documentation for %{name}
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
This package contains documentation for %{name}.

%prep
%setup -q -n %{gem_name}-%{version}

%build
gem build ../%{gem_name}-%{version}.gemspec
%gem_install
%install
mkdir -p %{buildroot}%{gem_dir}
cp -pa .%{gem_dir}/* %{buildroot}%{gem_dir}/

mkdir -p %{buildroot}%{_bindir}
cp -a .%{_bindir}/* %{buildroot}%{_bindir}/

find %{buildroot}%{gem_instdir}/bin -type f | xargs chmod a+x

%check

%files
%exclude %{gem_instdir}/.*
%doc %{gem_instdir}/{CHANGELOG.md,CONTRIBUTING.md,README.md}
%license %{gem_instdir}/LICENSE.md
%dir %{gem_instdir}
%{gem_libdir}
%{gem_instdir}/bin
%{_bindir}/%{gem_name}
%exclude %{gem_cache}
%{gem_spec}
 
%files doc
%doc %{gem_docdir}
%{gem_instdir}/%{gem_name}.gemspec

%changelog
* Sat Jan 15 2022 - harbottle@room3d3.com - 1.2.1-1
  - Bump version

* Wed Jan 20 2021 - harbottle@room3d3.com - 1.1.0-1
  - Bump version

* Sat Dec 21 2019 - harbottle@room3d3.com - 1.0.1-2
  - Improve dependencies

* Fri Dec 20 2019 - harbottle@room3d3.com - 1.0.1-1
  - Initial packaging
