%global owner kubernetes
%global repo kubernetes
%global host github.com
%global archive v%{version}.tar.gz
%global dir %{repo}-%{version}
%global namespace k8s.io/%{repo}
%global cmd kubeadm
%global tree 1.18
%global go_version 1.15.3

Name:          %{cmd}%{tree}
Version:       1.18.20
Release:       1%{?dist}.harbottle
Summary:       Kubernetes tool for standing up clusters
Group:         Applications/System
License:       Apache-2.0
Url:           https://%{host}/%{owner}/%{repo}
Source0:       %{url}/archive/%{archive}
Source1:       https://dl.google.com/go/go%{go_version}.linux-amd64.tar.gz
BuildRequires: which make rsync

%description
Kubernetes tool for standing up clusters. This package provides an
up-to-date version of the command. It can be run using command: %{name}

%prep
%setup -q -n %{dir}
%setup -q -T -D -a 1 -n %{dir}

%build
%define debug_package %{nil}
export GOPATH=$PWD
export GOROOT=$PWD/go
export PATH=$GOPATH/bin:$GOROOT/bin:$PATH
mkdir -p src/%{namespace}/
shopt -s extglob dotglob
mv !(src|go) src/%{namespace}/
shopt -u extglob dotglob
pushd src/%{namespace}/
make %{cmd}
popd

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 755 src/%{namespace}/_output/bin/%{cmd} $RPM_BUILD_ROOT%{_bindir}/%{name}

%files
%license src/%{namespace}/LICENSE
%doc src/%{namespace}/{*.md,OWNERS,SECURITY_CONTACTS}
%{_bindir}/%{name}

%changelog
* Wed Jun 16 2021 - harbottle@room3d3.com - 1.18.20-1
  - Bump version

* Wed May 12 2021 - harbottle@room3d3.com - 1.18.19-1
  - Bump version

* Thu Apr 15 2021 - harbottle@room3d3.com - 1.18.18-1
  - Bump version

* Thu Mar 18 2021 - harbottle@room3d3.com - 1.18.17-1
  - Bump version

* Wed Feb 17 2021 - harbottle@room3d3.com - 1.18.16-1
  - Bump version

* Wed Jan 13 2021 - harbottle@room3d3.com - 1.18.15-1
  - Bump version

* Fri Dec 18 2020 - harbottle@room3d3.com - 1.18.14-1
  - Bump version

* Wed Dec 09 2020 - harbottle@room3d3.com - 1.18.13-1
  - Bump version

* Fri Nov 20 2020 - harbottle@room3d3.com - 1.18.12-1
  - Bump version

* Wed Nov 11 2020 - harbottle@room3d3.com - 1.18.11-1
  - Bump version

* Fri Oct 16 2020 - harbottle@room3d3.com - 1.18.10-1
  - Initial package
