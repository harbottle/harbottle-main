Name:             envconsul
Version:          0.12.1
Release:          1%{?dist}.harbottle
Summary:          Launch a subprocess with environment variables populated from HashiCorp Consul and Vault
Group:            Applications/System
License:          MPLv2.0
URL:              https://github.com/hashicorp/envconsul
Source0:          https://releases.hashicorp.com/%{name}/%{version}/%{name}_%{version}_linux_amd64.zip
Source1:          https://raw.githubusercontent.com/hashicorp/%{name}/v%{version}/CHANGELOG.md
Source2:          https://raw.githubusercontent.com/hashicorp/%{name}/v%{version}/LICENSE
Source3:          https://raw.githubusercontent.com/hashicorp/%{name}/v%{version}/README.md
Requires(post):   systemd-units
Requires(preun):  systemd-units
Requires(postun): systemd-units

%description
Envconsul provides a convenient way to launch a subprocess with environment
variables populated from HashiCorp Consul and Vault. The tool is inspired by
envdir and envchain, but works on many major operating systems with no runtime
requirements.

%prep
%setup -q -c

%install
mv %{SOURCE1} .
mv %{SOURCE2} .
mv %{SOURCE3} .

install -d -m 755 $RPM_BUILD_ROOT%{_bindir}

install -m 0755 %{name} $RPM_BUILD_ROOT%{_bindir}

%files
%license LICENSE
%doc CHANGELOG.md README.md
%{_bindir}/%{name}

%changelog
* Sun Dec 12 2021 - harbottle@room3d3.com - 0.12.1-1
  - Bump version

* Thu Oct 07 2021 - harbottle@room3d3.com - 0.12.0-1
  - Bump version

* Tue Dec 01 2020 - harbottle@room3d3.com - 0.11.0-1
  - Bump version

* Mon Aug 10 2020 - harbottle@room3d3.com - 0.10.0-1
  - Bump version

* Tue Apr 28 2020 - harbottle@room3d3.com - 0.9.3-1
  - Bump version

* Wed Jan 08 2020 - harbottle@room3d3.com - 0.9.2-1
  - Bump version

* Fri Nov 08 2019 - harbottle@room3d3.com - 0.9.1-1
  - Bump version

* Thu Aug 15 2019 - harbottle@room3d3.com - 0.9.0-1
  - Bump version


* Thu Jun 13 2019 - harbottle@room3d3.com - 0.8.0-1
  - Initial package
