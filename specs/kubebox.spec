%global git_owner astefanutti
%global git_repo kubebox
%global git_archive_file v%{version}.tar.gz
%global git_archive_dir %{git_repo}-%{version}

Name:          %{git_repo}
Version:       0.10.0
Release:       1%{?dist}.harbottle
Summary:       Kubebox is a Terminal and Web console for Kubernetes
Group:         Applications/System
License:       MIT
Url:           https://github.com/%{git_owner}/%{git_repo}
Source0:       %{url}/archive/%{git_archive_file}
BuildRequires: devtoolset-7 rh-nodejs10 make python glibc-static

%description
Kubebox is a terminal console for Kubernetes cluster which allows you to manage
and monitor your cluster-live status with nice, old-school interface. Kubebox
shows your pod resource usage, cluster monitoring, and container logs, etc.
Additionally, you can easily navigate to the desired namespace and execute into
the desired container for fast troubleshooting/recovery.

%prep
%setup -q -n %{git_archive_dir}

%build
%define debug_package %{nil}
scl enable devtoolset-7 - <<SCL1
scl enable rh-nodejs10 - <<SCL2
npm install --ignore-scripts spawn-sync
npm install
npm run executable-linux-x64
SCL2
SCL1

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 755 %{name} $RPM_BUILD_ROOT%{_bindir}

%files
%license LICENSE
%doc README.adoc
%{_bindir}/%{name}

%changelog
* Sat Apr 09 2022 - harbottle@room3d3.com - 0.10.0-1
  - Bump version

* Sat Nov 21 2020 - harbottle@room3d3.com - 0.9.0-1
  - Bump version

* Tue Apr 14 2020 - harbottle@room3d3.com - 0.8.0-1
  - Bump version

* Fri Nov 29 2019 - harbottle@room3d3.com - 0.7.0-1
  - Bump version

* Fri Sep 27 2019 - harbottle@room3d3.com - 0.6.1-1
  - Bump version

* Tue Aug 13 2019 - harbottle@room3d3.com - 0.6.0-1
  - Bump version
  - Fix build

* Sun Jun 09 2019 - harbottle@room3d3.com - 0.5.0-1
  - Bump version

* Sun Jan 27 2019 - harbottle@room3d3.com - 0.4.0-1
  - Bump version

* Tue Jan 08 2019 - harbottle@room3d3.com - 0.3.2-1
  - Build from source

* Tue Jan 08 2019 - vlinx@yahoo.com - 0.3.2-1
  - Initial spec
