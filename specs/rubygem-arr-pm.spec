%global gem_name arr-pm

Name:          rubygem-%{gem_name}
Version:       0.0.10
Release:       1%{?dist}.harbottle
Summary:       Read and write rpm packages
Group:         Applications/System
License:       Apache-2.0
URL:           https://github.com/jordansissel/ruby-arr-pm
Source0:       https://rubygems.org/gems/%{gem_name}-%{version}.gem
Requires:      ruby(release)
Requires:      ruby(rubygems)
Requires:      ruby
Requires:      rubygem(cabin)
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: ruby
BuildRequires: rubygem(flores)
BuildArch:     noarch

%description
This library allows to you to read and write rpm packages. Written in pure ruby
because librpm is not available on all systems.

%package doc
Summary: Documentation for %{name}
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
This package contains documentation for %{name}.

%prep
%setup -q -n %{gem_name}-%{version}

%build
gem build ../%{gem_name}-%{version}.gemspec
%gem_install
%install
mkdir -p %{buildroot}%{gem_dir}
cp -pa .%{gem_dir}/* %{buildroot}%{gem_dir}/

%check

%files
%exclude %{gem_instdir}/.*
%doc %{gem_instdir}/README.md
%license %{gem_instdir}/LICENSE
%dir %{gem_instdir}
%{gem_libdir}
%{gem_instdir}/cpio.rb
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%{gem_instdir}/%{gem_name}.gemspec
%{gem_instdir}/Gemfile
%{gem_instdir}/Makefile

%changelog
* Sun May 16 2021 - harbottle@room3d3.com - 0.0.10-1
  - Initial packaging
