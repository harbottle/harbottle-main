%global owner kubernetes
%global repo kubernetes
%global host github.com
%global archive v%{version}.tar.gz
%global dir %{repo}-%{version}
%global namespace k8s.io/%{repo}
%global cmd kubeadm
%global tree 1.13

Name:          %{cmd}%{tree}
Version:       1.13.12
Release:       1.el7.harbottle
Summary:       Kubernetes tool for standing up clusters
Group:         Applications/System
License:       Apache 2.0
Url:           https://%{host}/%{owner}/%{repo}
Source0:       %{url}/archive/%{archive}
BuildRequires: golang which make

%description
Kubernetes tool for standing up clusters. This package provides an
up-to-date version of the command. It can be run using command: %{name}

%prep
%setup -q -n %{dir}

%build
%define debug_package %{nil}
export GOPATH=$PWD
mkdir -p src/%{namespace}/
shopt -s extglob dotglob
mv !(src) src/%{namespace}/
shopt -u extglob dotglob
pushd src/%{namespace}/
make %{cmd}
popd

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 755 src/%{namespace}/_output/bin/%{cmd} $RPM_BUILD_ROOT%{_bindir}/%{name}

%files
%license src/%{namespace}/LICENSE
%doc src/%{namespace}/{*.md,OWNERS,SECURITY_CONTACTS}
%{_bindir}/%{name}

%changelog
* Tue Oct 15 2019 - harbottle@room3d3.com - 1.13.12-1
  - Bump version

* Wed Sep 18 2019 - harbottle@room3d3.com - 1.13.11-1
  - Bump version

* Mon Aug 19 2019 - harbottle@room3d3.com - 1.13.10-1
  - Bump version

* Mon Aug 05 2019 - harbottle@room3d3.com - 1.13.9-1
  - Bump version

* Mon Jul 08 2019 - harbottle@room3d3.com - 1.13.8-1
  - Bump version

* Thu Jun 06 2019 - harbottle@room3d3.com - 1.13.7-1
  - Bump version

* Mon Jun 03 2019 - harbottle@room3d3.com - 1.13.6-1
  - Bump version

* Mon Mar 25 2019 - harbottle@room3d3.com - 1.13.5-1
  - Bump version

* Thu Feb 28 2019 - harbottle@room3d3.com - 1.13.4-1
  - Bump version

* Wed Feb 06 2019 - harbottle@room3d3.com - 1.13.3-1
  - Initial package
