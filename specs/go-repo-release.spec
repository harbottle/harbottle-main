Name:           go-repo-release
Version:        2
Release:        2.el7
Summary:        Go-Repo repository configuration
Group:          System Environment/Base
License:        BSD 3
URL:            https://golang.org/
Source0:        RPM-GPG-KEY-GO-REPO
Source1:        go-repo.repo

BuildArch:     noarch
Requires:      redhat-release >=  7

%description
This package contains the Go-Repo repository GPG key as well as configuration
for yum.

%prep
%setup -q  -c -T
install -pm 644 %{SOURCE0} .
install -pm 644 %{SOURCE1} .

%build

%install
rm -rf $RPM_BUILD_ROOT
install -Dpm 644 %{SOURCE0} $RPM_BUILD_ROOT%{_sysconfdir}/pki/rpm-gpg/RPM-GPG-KEY-GO-REPO
install -dm 755 $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d
install -pm 644 %{SOURCE1} $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%config(noreplace) /etc/yum.repos.d/*
/etc/pki/rpm-gpg/*

%changelog
* Wed Aug 23 2017 <grainger@gmail.com> -  2-2.el7
- Fix release number

* Mon Jun 26 2017 grainger@gmail.com
- Initial packaging
