%global namespace sigs.k8s.io/%{name}

Name:          kind
Version:       0.14.0
Release:       1%{?dist}.harbottle
Summary:       Kubernetes IN Docker - local clusters for testing Kubernetes
Group:         Applications/System
License:       Apache-2.0
URL:           https://github.com/kubernetes-sigs/%{name}
Source0:       %{url}/archive/v%{version}.tar.gz
BuildRequires: golang make which zip

%description
kind is a tool for running local Kubernetes clusters using Docker container
"nodes". kind is primarily designed for testing Kubernetes 1.11+, initially
targeting the conformance tests.

%prep
%setup -q -n %{name}-%{version}

%build
%define debug_package %{nil}
export GOPROXY=https://proxy.golang.org
export GOPATH=$PWD
export PATH=${PATH}:${GOPATH}/bin
export GO111MODULE="on"
mkdir -p src/%{namespace}
shopt -s extglob dotglob
mv !(src) src/%{namespace}
shopt -u extglob dotglob
pushd src/%{namespace}
go build -o=%{name}
popd

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 0755 src/%{namespace}/%{name} $RPM_BUILD_ROOT%{_bindir}

%clean
rm -rf %{buildroot}

%files
%license src/%{namespace}/LICENSE
%doc src/%{namespace}/*.md
%{_bindir}/%{name}

%changelog
* Thu May 19 2022 - harbottle@room3d3.com - 0.14.0-1
  - Bump version

* Tue May 10 2022 - harbottle@room3d3.com - 0.13.0-1
  - Bump version

* Mon Mar 07 2022 - harbottle@room3d3.com - 0.12.0-1
  - Bump version

* Fri May 28 2021 - harbottle@room3d3.com - 0.11.1-1
  - Bump version

* Tue May 18 2021 - harbottle@room3d3.com - 0.11.0-1
  - Bump version

* Sat Jan 23 2021 - harbottle@room3d3.com - 0.10.0-1
  - Bump version

* Tue Sep 15 2020 - harbottle@room3d3.com - 0.9.0-1
  - Bump version

* Sat May 02 2020 - harbottle@room3d3.com - 0.8.1-1
  - Bump version

* Fri May 01 2020 - harbottle@room3d3.com - 0.8.0-1
  - Bump version

* Tue Jan 14 2020 - harbottle@room3d3.com - 0.7.0-1
  - Bump version

* Mon Dec 09 2019 - harbottle@room3d3.com - 0.6.1-2
  - Build from source again

* Fri Dec 06 2019 - harbottle@room3d3.com - 0.6.1-1
  - Bump version
  - Use binary as build is broken

* Mon Nov 18 2019 - harbottle@room3d3.com - 0.6.0-1
  - Bump version

* Wed Aug 21 2019 - harbottle@room3d3.com - 0.5.1-1
  - Bump version

* Tue Aug 20 2019 - harbottle@room3d3.com - 0.5.0-1
  - Bump version

* Mon Aug 05 2019 - harbottle@room3d3.com - 0.4.0-1
  - Initial version
