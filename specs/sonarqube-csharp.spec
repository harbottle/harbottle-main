%define __jar_repack 0

Name:     sonarqube-csharp
Version:  8.41.0.50478
Release:  1%{?dist}.harbottle
Summary:  SonarQube C Sharp plugin
Group:    Applications/System
License:  LGPL-3.0
Source0:  https://binaries.sonarsource.com/Distribution/sonar-csharp-plugin/sonar-csharp-plugin-%{version}.jar
Autoprov: no

%description
C Sharp plugin for SonarQube.

%prep

%install
install -d -m 755 $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins
mv %{SOURCE0} $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins

%pre
rm -f %{_var}/lib/sonarqube/extensions/plugins/sonar-csharp-plugin-*.jar
getent group sonarqube >/dev/null || groupadd -f -r sonarqube
getent passwd sonarqube >/dev/null || useradd -r -g sonarqube -d /usr/share/sonarqube -s /sbin/nologin -c "SonarQube user" sonarqube
exit 0

%files
%attr(0644,sonarqube,sonarqube) %{_var}/lib/sonarqube/extensions/plugins/sonar-csharp-plugin-%{version}.jar

%changelog
* Mon Jul 04 2022 - harbottle@room3d3.com - 8.41.0.50478-1
  - Bump version

* Tue May 31 2022 - harbottle@room3d3.com - 8.40.0.48530-1
  - Bump version

* Thu May 12 2022 - harbottle@room3d3.com - 8.39.0.47922-1
  - Bump version

* Mon Apr 25 2022 - harbottle@room3d3.com - 8.38.0.46746-1
  - Bump version

* Wed Mar 30 2022 - harbottle@room3d3.com - 8.37.0.45539-1
  - Bump version

* Fri Mar 04 2022 - harbottle@room3d3.com - 8.36.1.44192-1
  - Bump version

* Tue Feb 22 2022 - harbottle@room3d3.com - 8.36.0.43782-1
  - Bump version

* Mon Jan 31 2022 - harbottle@room3d3.com - 8.35.0.42613-1
  - Bump version

* Mon Jan 17 2022 - harbottle@room3d3.com - 8.34.0.42011-1
  - Bump version

* Sun Dec 12 2021 - harbottle@room3d3.com - 8.33.0.40503-1
  - Bump version

* Wed Oct 06 2021 - harbottle@room3d3.com - 8.30.0.37606-1
  - Bump version

* Tue Sep 14 2021 - harbottle@room3d3.com - 8.29.0.36737-1
  - Bump version

* Fri Sep 03 2021 - harbottle@room3d3.com - 8.28.0.36372-1
  - Bump version

* Fri Aug 06 2021 - harbottle@room3d3.com - 8.27.0.35380-1
  - Bump version

* Mon Jul 12 2021 - harbottle@room3d3.com - 8.26.0.34506-1
  - Bump version

* Mon Jun 21 2021 - harbottle@room3d3.com - 8.25.0.33663-1
  - Bump version

* Mon Jun 07 2021 - harbottle@room3d3.com - 8.24.0.32949-1
  - Bump version

* Fri May 21 2021 - harbottle@room3d3.com - 8.23.0.32424-1
  - Bump version

* Thu Apr 29 2021 - harbottle@room3d3.com - 8.22.0.31243-1
  - Bump version

* Mon Apr 19 2021 - harbottle@room3d3.com - 8.21.0.30542-1
  - Bump version

* Wed Mar 17 2021 - harbottle@room3d3.com - 8.20.0.28934-1
  - Bump version

* Mon Mar 01 2021 - harbottle@room3d3.com - 8.19.0.28253-1
  - Bump version

* Mon Feb 08 2021 - harbottle@room3d3.com - 8.18.0.27296-1
  - Bump version

* Tue Jan 19 2021 - harbottle@room3d3.com - 8.17.0.26580-1
  - Bump version

* Mon Dec 21 2020 - harbottle@room3d3.com - 8.16.0.25740-1
  - Bump version

* Mon Nov 23 2020 - harbottle@room3d3.com - 8.15.0.24505-1
  - Bump version

* Mon Oct 12 2020 - harbottle@room3d3.com - 8.14.0.22654-1
  - Bump version

* Mon Sep 21 2020 - harbottle@room3d3.com - 8.13.1.21947-1
  - Bump version

* Mon Sep 14 2020 - harbottle@room3d3.com - 8.13.0.21683-1
  - Bump version

* Mon Aug 24 2020 - harbottle@room3d3.com - 8.12.0.21095-1
  - Bump version

* Tue Aug 11 2020 - harbottle@room3d3.com - 8.11.0.20529-1
  - Bump version

* Fri Jul 24 2020 - harbottle@room3d3.com - 8.10.0.19839-1
  - Bump version

* Fri Jun 26 2020 - harbottle@room3d3.com - 8.9.0.19135-1
  - Bump version

* Fri Jun 05 2020 - harbottle@room3d3.com - 8.8.0.18411-1
  - Bump version

* Mon May 11 2020 - harbottle@room3d3.com - 8.7.0.17535-1
  - Bump version

* Tue Apr 28 2020 - harbottle@room3d3.com - 8.6.1.17183-1
  - Bump version

* Thu Apr 02 2020 - harbottle@room3d3.com - 8.6.0.16497-1
  - Bump version

* Mon Mar 16 2020 - harbottle@room3d3.com - 8.5.0.15942-1
  - Bump version

* Sat Feb 29 2020 - harbottle@room3d3.com - 8.4.0.15306-1
  - Bump version

* Fri Jan 31 2020 - harbottle@room3d3.com - 8.3.0.14607-1
  - Bump version

* Thu Jan 09 2020 - harbottle@room3d3.com - 8.2.0.14119-1
  - Bump version

* Thu Dec 19 2019 - harbottle@room3d3.com - 8.1.0.13383-1
  - Bump version

* Tue Dec 10 2019 - harbottle@room3d3.com - 8.0.0.9566-2
  - Spec file changes for el8

* Tue Oct 22 2019 - harbottle@room3d3.com - 8.0.0.9566-1
  - Bump version

* Tue Oct 01 2019 - harbottle@room3d3.com - 7.17.0.9346-1
  - Bump version

* Mon Aug 12 2019 - harbottle@room3d3.com - 7.16.0.8981-1
  - Bump version

* Sun Jul 21 2019 - harbottle@room3d3.com - 7.15.0.8572-2
  - Fix plugin ownership

* Sun Jul 21 2019 - harbottle@room3d3.com - 7.15.0.8572-1
  - Initial package
