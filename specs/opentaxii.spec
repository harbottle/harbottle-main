%global __os_install_post %(echo '%{__os_install_post}' | sed -e 's!/usr/lib[^[:space:]]*/brp-python-bytecompile[[:space:]].*$!!g')
%global installdir /usr/share/python
%global homedir /usr/lib/opentaxii

Name:            opentaxii
Version:         0.9.1
Release:         1%{?dist}.harbottle
Summary:         A robust Python implementation of TAXII Services
Group:           Applications/System
License:         BSD
URL:             http://www.opentaxii.org
Source0:         opentaxii.sysconfig
Source1:         opentaxii.yml
Source2:         opentaxii.service
%if 0%{?rhel} == 7
BuildRequires:   python36-virtualenv
%endif
%if 0%{?rhel} == 8
BuildRequires:   python3-virtualenv
%endif
BuildRequires:   systemd-units
AutoReqProv:     no
Requires:        python36
Requires(post):  sqlite
Requires(post):  systemd
Requires(preun): systemd

%description
A robust Python implementation of TAXII Services.

%prep
%build
mkdir -p .%{installdir}
virtualenv-3.6 .%{installdir}/%{name}
export LANG=en_US.UTF-8
export LANGUAGE=en_US:en
export LC_ALL=en_US.UTF-8
.%{installdir}/%{name}/bin/pip3.6 install "opentaxii==%{version}"
.%{installdir}/%{name}/bin/pip3.6 install "psycopg2-binary==2.7.6.1"
.%{installdir}/%{name}/bin/pip3.6 install "gunicorn==19.9.0"
.%{installdir}/%{name}/bin/pip3.6 install "virtualenv-tools3==2.0.2"

pushd .%{installdir}/%{name}
  bin/virtualenv-tools --update-path %{installdir}/%{name}
popd
find . -iname *.pyc -exec rm {} \;
find . -iname *.pyo -exec rm {} \;

%install
install -d -m 755 $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig
install -d -m 755 $RPM_BUILD_ROOT%{_sysconfdir}/%{name}
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -d -m 755 $RPM_BUILD_ROOT%{homedir}
install -d -m 755 $RPM_BUILD_ROOT%{_unitdir}
install -d -m 755 $RPM_BUILD_ROOT%{installdir}

# Python virtualenv
mv .%{installdir}/* $RPM_BUILD_ROOT%{installdir}/

# Conf
install -m 644 %{SOURCE0} $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/%{name}
install -m 644 %{SOURCE1} $RPM_BUILD_ROOT%{_sysconfdir}/%{name}/%{name}.yml

# Service
install -m 644 %{SOURCE2} $RPM_BUILD_ROOT%{_unitdir}/%{name}.service

# Binary links
for file in $RPM_BUILD_ROOT%{installdir}/opentaxii/bin/opentaxii*; do
  ln -s %{installdir}/opentaxii/bin/$(basename $file) $RPM_BUILD_ROOT%{_bindir}/$(basename $file)
done
ln -s %{installdir}/opentaxii/bin/gunicorn $RPM_BUILD_ROOT%{_bindir}/opentaxii-gunicorn

%pre
getent group %{name} >/dev/null || groupadd -f -r %{name}
getent passwd %{name} >/dev/null || useradd -r -g %{name} -d %{homedir} -s /sbin/nologin -c "OpenTAXII user" %{name}
exit 0

%post
if [ -e %{homedir}/auth.db ] && [ ! -e %{homedir}/authdb_migration_0.1.9_0.1.10 ]
then
  /usr/bin/sqlite3 %{homedir}/auth.db <<EOS
ALTER TABLE accounts ADD COLUMN is_admin;
ALTER TABLE accounts ADD COLUMN _permissions;
EOS
touch %{homedir}/authdb_migration_0.1.9_0.1.10
echo OpenTAXII 0.1.9 to 0.1.10 auth.db SQLite database migrations complete
fi
systemctl daemon-reload
%systemd_post %{name}.service

%preun
%systemd_preun %{name}.service

%postun
%systemd_postun_with_restart %{name}.service

%files
%config(noreplace) %{_sysconfdir}/sysconfig
%dir %config(noreplace) %attr(0550,%{name},%{name}) %{_sysconfdir}/%{name}
%config(noreplace) %attr(0440,%{name},%{name}) %{_sysconfdir}/%{name}/%{name}.yml
%{_bindir}/%{name}*
%attr(0770,%{name},%{name}) %{homedir}
%{_unitdir}/%{name}.service
%{installdir}/%{name}

%changelog
* Mon Jul 11 2022 - harbottle@room3d3.com - 0.9.1-1
  - Bump version

* Mon Jun 13 2022 - harbottle@room3d3.com - 0.9.0-1
  - Bump version

* Sun Jun 05 2022 - harbottle@room3d3.com - 0.8.0-1
  - Bump version

* Fri May 27 2022 - harbottle@room3d3.com - 0.7.0-1
  - Bump version

* Wed May 25 2022 - harbottle@room3d3.com - 0.6.0-1
  - Bump version

* Wed May 25 2022 - harbottle@room3d3.com - 0.5.0-1
  - Bump version

* Fri May 20 2022 - harbottle@room3d3.com - 0.4.0-1
  - Bump version

* Wed Apr 13 2022 - harbottle@room3d3.com - 0.3.0a4-1
  - Bump version

* Fri Jan 21 2022 - harbottle@room3d3.com - 0.3.0a3-1
  - Bump version

* Sat Jan 15 2022 - harbottle@room3d3.com - 0.3.0a2-1
  - Bump version

* Sun Dec 12 2021 - harbottle@room3d3.com - 0.2.2-1
  - Bump version

* Thu Nov 04 2021 - harbottle@room3d3.com - 0.2.1-1
  - Bump version

* Fri Oct 16 2020 - harbottle@room3d3.com - 0.2.0-2
  - Refactor spec to allow build for el8

* Thu Oct 08 2020 - harbottle@room3d3.com - 0.2.0-1
  - Bump version

* Sat Jun 08 2019 - harbottle@room3d3.com - 0.1.12-2
  - Use python 3.6 instead of python 3.4 following EPEL

* Wed Mar 06 2019 - harbottle@room3d3.com - 0.1.12-1
  - Bump version

* Mon Feb 18 2019 - harbottle@room3d3.com - 0.1.11-1
  - Bump version

* Thu Jan 31 2019 - harbottle@room3d3.com - 0.1.10-1
  - New version
  - Use Python 3.4 instead
  - Handle database migrations automatically

* Mon Jan 22 2018 - grainger@gmail.com - 0.1.9-4
  - Fixed bundled psycopg2 (again)
* Mon Jan 22 2018 - grainger@gmail.com - 0.1.9-3
  - Fixed bundled psycopg2
* Fri Jan 19 2018 - grainger@gmail.com - 0.1.9-2
  - Initial packaging replacing version packaged using FPM
