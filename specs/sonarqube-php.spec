%define __jar_repack 0

Name:     sonarqube-php
Version:  3.23.1.8766
Release:  1%{?dist}.harbottle
Summary:  SonarQube PHP plugin
Group:    Applications/System
License:  LGPL-3.0
Source0:  https://binaries.sonarsource.com/Distribution/sonar-php-plugin/sonar-php-plugin-%{version}.jar
Autoprov: no

%description
PHP plugin for SonarQube.

%prep

%install
install -d -m 755 $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins
mv %{SOURCE0} $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins

%pre
rm -f %{_var}/lib/sonarqube/extensions/plugins/sonar-php-plugin-*.jar
getent group sonarqube >/dev/null || groupadd -f -r sonarqube
getent passwd sonarqube >/dev/null || useradd -r -g sonarqube -d /usr/share/sonarqube -s /sbin/nologin -c "SonarQube user" sonarqube
exit 0

%files
%attr(0644,sonarqube,sonarqube) %{_var}/lib/sonarqube/extensions/plugins/sonar-php-plugin-%{version}.jar

%changelog
* Mon Mar 28 2022 - harbottle@room3d3.com - 3.23.1.8766-1
  - Bump version

* Wed Feb 09 2022 - harbottle@room3d3.com - 3.23.0.8726-1
  - Bump version

* Mon Jan 24 2022 - harbottle@room3d3.com - 3.22.1.8626-1
  - Bump version

* Fri Jan 21 2022 - harbottle@room3d3.com - 3.22.1.8616-1
  - Bump version

* Sun Dec 12 2021 - harbottle@room3d3.com - 3.22.0.8482-1
  - Bump version

* Tue Oct 26 2021 - harbottle@room3d3.com - 3.21.1.8250-1
  - Bump version

* Mon Oct 11 2021 - harbottle@room3d3.com - 3.21.0.8193-1
  - Bump version

* Mon Sep 13 2021 - harbottle@room3d3.com - 3.20.0.8080-1
  - Bump version

* Mon Aug 16 2021 - harbottle@room3d3.com - 3.19.0.7847-1
  - Bump version

* Tue Jul 27 2021 - harbottle@room3d3.com - 3.18.0.7718-1
  - Bump version

* Thu Apr 29 2021 - harbottle@room3d3.com - 3.17.0.7439-1
  - Bump version

* Wed Mar 24 2021 - harbottle@room3d3.com - 3.16.0.7320-1
  - Bump version

* Fri Jan 29 2021 - harbottle@room3d3.com - 3.15.0.7197-1
  - Bump version

* Fri Jan 08 2021 - harbottle@room3d3.com - 3.14.0.6990-1
  - Bump version

* Mon Nov 30 2020 - harbottle@room3d3.com - 3.13.0.6849-1
  - Bump version

* Thu Oct 29 2020 - harbottle@room3d3.com - 3.12.0.6710-1
  - Bump version

* Tue Oct 20 2020 - harbottle@room3d3.com - 3.11.0.6645-1
  - Bump version

* Mon Oct 05 2020 - harbottle@room3d3.com - 3.10.0.6474-1
  - Bump version

* Tue Aug 25 2020 - harbottle@room3d3.com - 3.9.0.6331-1
  - Bump version

* Fri Aug 14 2020 - harbottle@room3d3.com - 3.8.1.6222-1
  - Bump version

* Fri Aug 07 2020 - harbottle@room3d3.com - 3.8.0.6152-1
  - Bump version

* Fri Jul 24 2020 - harbottle@room3d3.com - 3.7.0.5943-1
  - Bump version

* Fri Jul 03 2020 - harbottle@room3d3.com - 3.6.0.5808-1
  - Bump version

* Thu May 28 2020 - harbottle@room3d3.com - 3.5.0.5655-1
  - Bump version

* Mon May 11 2020 - harbottle@room3d3.com - 3.4.0.5461-1
  - Bump version

* Tue Dec 10 2019 - harbottle@room3d3.com - 3.3.0.5166-2
  - Spec file changes for el8

* Mon Dec 09 2019 - harbottle@room3d3.com - 3.3.0.5166-1
  - Bump version

* Sun Jul 21 2019 - harbottle@room3d3.com - 3.2.0.4868-2
  - Fix plugin ownership

* Sun Jul 21 2019 - harbottle@room3d3.com - 3.2.0.4868-1
  - Initial package
