%global __os_install_post %(echo '%{__os_install_post}' | sed -e 's!/usr/lib[^[:space:]]*/brp-python-bytecompile[[:space:]].*$!!g')
%global installdir /opt
%global homedir /opt/%{name}
%global cffiversion 1.1.0
%global gunicornversion 19.9.0
%global whitenoiseversion 4.1.4

Name:            graphite
Version:         1.1.10
Release:         1%{?dist}.harbottle
Summary:         An enterprise-scale monitoring tool
License:         Apache
URL:             https://github.com/graphite-project
Source0:         graphite-web.conf
Source1:         graphite.logrotate
Source2:         graphite-carbon-aggregator.service
Source3:         graphite-carbon-aggregator-cache.service
Source4:         graphite-carbon-cache.service
Source5:         graphite-carbon-relay.service
Source6:         graphite-web.service
BuildRequires:   gcc
BuildRequires:   make
BuildRequires:   openssl-devel
BuildRequires:   python-pip
BuildRequires:   python-virtualenv
BuildRequires:   python-virtualenv-tools
BuildRequires:   systemd-units
BuildRequires:   python-devel
BuildRequires:   cairo-devel
BuildRequires:   libffi-devel
AutoReqProv:     no
Requires:        python cairo
Requires(post):  systemd
Requires(preun): systemd

%description
Graphite is an enterprise-scale monitoring tool that runs well on cheap
hardware. Graphite consists of 3 software components:

  carbon:
    A Twisted daemon that listens for time-series data
  whisper:
    A simple database library for storing time-series data
  graphite webapp:
    A Django webapp that renders graphs on-demand using Cairo

%prep
%build

# Build virtualenv
mkdir -p .%{homedir}
virtualenv .%{homedir}
.%{homedir}/bin/pip install --upgrade pip
.%{homedir}/bin/pip install "cffi>=%{cffiversion}"
.%{homedir}/bin/pip install "whisper==%{version}"
.%{homedir}/bin/pip install "carbon==%{version}"
.%{homedir}/bin/pip install "graphite-web==%{version}"
.%{homedir}/bin/pip install "carbonate==%{version}"
.%{homedir}/bin/pip install "gunicorn==%{gunicornversion}"
.%{homedir}/bin/pip install "whitenoise==%{whitenoiseversion}"

pushd .%{homedir}
  virtualenv-tools --update-path %{homedir}

  # Workaround for path bug: https://github.com/graphite-project/carbon/issues/514
  mv lib/python2.7/site-packages/opt/graphite/lib/carbon lib/python2.7/site-packages/
  mv lib/python2.7/site-packages/opt/graphite/lib/twisted/plugins/* lib/python2.7/site-packages/twisted/plugins/
  mv lib/python2.7/site-packages/opt/graphite/webapp/graphite webapp

  # Rename example conf files
  for file in conf/*.example; do
    mv "$file" conf/"$(basename "$file" .example)"
  done
  
  # Move wsgi application
  mv conf/graphite.wsgi webapp/wsgi.py

  # Remove complied files
  find . -iname *.pyc -exec rm {} \;
  find . -iname *.pyo -exec rm {} \;
popd

%install

# Dirs
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -d -m 755 $RPM_BUILD_ROOT%{homedir}
install -d -m 755 $RPM_BUILD_ROOT%{homedir}/storage
install -d -m 755 $RPM_BUILD_ROOT%{homedir}/storage/log/
install -d -m 755 $RPM_BUILD_ROOT%{_localstatedir}/log/graphite/{web,carbon}
install -d -m 755 $RPM_BUILD_ROOT%{_sysconfdir}
install -d -m 755 $RPM_BUILD_ROOT%{_sysconfdir}/logrotate.d
install -d -m 755 $RPM_BUILD_ROOT%{_unitdir}

# Install conf
mv .%{homedir}/conf $RPM_BUILD_ROOT%{_sysconfdir}/%{name}
install -m 644 %{SOURCE0} $RPM_BUILD_ROOT%{_sysconfdir}/%{name}/graphite-web.conf
ln -s %{_sysconfdir}/%{name} $RPM_BUILD_ROOT%{homedir}/conf
mv .%{homedir}/webapp/graphite/local_settings.py.example $RPM_BUILD_ROOT%{_sysconfdir}/%{name}/local_settings.py
install -m 644 %{SOURCE1} $RPM_BUILD_ROOT%{_sysconfdir}/logrotate.d/%{name}

# Install python virtualenv
mv .%{homedir}/* $RPM_BUILD_ROOT%{homedir}/

# Link graphite-web setting
ln -s %{_sysconfdir}/%{name}/local_settings.py $RPM_BUILD_ROOT%{homedir}/webapp/graphite/local_settings.py

# Install logs
ln -s %{_localstatedir}/log/graphite/web $RPM_BUILD_ROOT%{homedir}/storage/log/webapp

# Install services
install -m 644 %{SOURCE2} $RPM_BUILD_ROOT%{_unitdir}/
install -m 644 %{SOURCE3} $RPM_BUILD_ROOT%{_unitdir}/
install -m 644 %{SOURCE4} $RPM_BUILD_ROOT%{_unitdir}/
install -m 644 %{SOURCE5} $RPM_BUILD_ROOT%{_unitdir}/
install -m 644 %{SOURCE6} $RPM_BUILD_ROOT%{_unitdir}/

# Link executables
for file in $RPM_BUILD_ROOT%{homedir}/bin/{carbon*,whisper*.py,whisper-aggregate,update-storage-times*,validate-storage-schemas*,find-corrupt-whisper-files*,run-graphite-devel-server*,rrd2whisper*}; do
  file_nopath=${file##*/}
  file_noext=${file_nopath%.*}
  ln -s %{homedir}/bin/$file_nopath $RPM_BUILD_ROOT%{_bindir}/graphite-$file_noext
done

%pre
getent group %{name} >/dev/null || groupadd -f -r %{name}
getent passwd %{name} >/dev/null || useradd -r -g %{name} -d %{homedir} -s /sbin/nologin -c "graphite user" %{name}
exit 0

%post
systemctl daemon-reload
%systemd_post graphite-carbon-aggregator.service
%systemd_post graphite-carbon-aggregator-cache.service
%systemd_post graphite-carbon-cache.service
%systemd_post graphite-carbon-relay.service
%systemd_post graphite-web.service
echo Initalizing the graphite-web database...
su - graphite -s /bin/bash -c "%{homedir}/bin/django-admin.py migrate --settings=graphite.settings --pythonpath=%{homedir}/webapp"
echo Building the graphite index...
su - graphite -s /bin/bash -c "%{homedir}/bin/build-index.sh"

%preun
%systemd_preun graphite-carbon-aggregator.service
%systemd_preun graphite-carbon-aggregator-cache.service
%systemd_preun graphite-carbon-cache.service
%systemd_preun graphite-carbon-relay.service
%systemd_preun graphite-web.service

%postun
%systemd_postun_with_restart graphite-carbon-aggregator.service
%systemd_postun_with_restart graphite-carbon-aggregator-cache.service
%systemd_postun_with_restart graphite-carbon-cache.service
%systemd_postun_with_restart graphite-carbon-relay.service
%systemd_postun_with_restart graphite-web.service

%files
%attr(-,%{name},%{name}) %homedir
%config(noreplace) %{_sysconfdir}/%{name}
%config(noreplace) %{_sysconfdir}/logrotate.d/%{name}
%attr(-,%{name},%{name}) %{_localstatedir}/log/graphite
%{_unitdir}/graphite-carbon-aggregator.service
%{_unitdir}/graphite-carbon-aggregator-cache.service
%{_unitdir}/graphite-carbon-cache.service
%{_unitdir}/graphite-carbon-relay.service
%{_unitdir}/graphite-web.service
%{_bindir}/graphite-*

%changelog
* Wed May 25 2022 - harbottle@room3d3.com - 1.1.10-1
  - Fix build
  - Bump version

* Mon Apr 19 2021 - harbottle@room3d3.com - 1.1.8-1
  - Bump version

* Mon Mar 16 2020 - harbottle@room3d3.com - 1.1.7-1
  - Bump version

* Thu Oct 24 2019 - harbottle@room3d3.com - 1.1.6-1
  - Bump version

* Mon Dec 31 2018 - harbottle@room3d3.com - 1.1.5-1
  - Bump version
* Mon Oct 15 2018 - grainger@gmail.com - 1.1.4-1
  - Initial packaging
* Thu Jun 14 2018 - grainger@gmail.com - 1.1.3-1
  - Initial packaging
