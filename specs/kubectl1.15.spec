%global owner kubernetes
%global repo kubernetes
%global host github.com
%global archive v%{version}.tar.gz
%global dir %{repo}-%{version}
%global namespace k8s.io/%{repo}
%global cmd kubectl
%global tree 1.15
%global go_version 1.12.5

Name:          %{cmd}%{tree}
Version:       1.15.12
Release:       1%{?dist}.harbottle
Summary:       %{cmd} controls the Kubernetes cluster manager.
Group:         Applications/System
License:       Apache 2.0
Url:           https://%{host}/%{owner}/%{repo}
Source0:       %{url}/archive/%{archive}
Source1:       https://dl.google.com/go/go%{go_version}.linux-amd64.tar.gz
BuildRequires: which make rsync

%description
%{cmd} controls the Kubernetes cluster manager. This package provides an
up-to-date version of the command. It can be run using command: %{name}

%prep
%setup -q -n %{dir}
%setup -q -T -D -a 1 -n %{dir}

%build
%define debug_package %{nil}
export GOPATH=$PWD
export GOROOT=$PWD/go
export PATH=$GOPATH/bin:$GOROOT/bin:$PATH
mkdir -p src/%{namespace}/
shopt -s extglob dotglob
mv !(src|go) src/%{namespace}/
shopt -u extglob dotglob
pushd src/%{namespace}/
make %{cmd}
popd

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 755 src/%{namespace}/_output/bin/%{cmd} $RPM_BUILD_ROOT%{_bindir}/%{name}

%files
%license src/%{namespace}/LICENSE
%doc src/%{namespace}/{*.md,OWNERS,SECURITY_CONTACTS}
%{_bindir}/%{name}

%changelog
* Wed May 06 2020 - harbottle@room3d3.com - 1.15.12-1
  - Bump version

* Fri Mar 13 2020 - harbottle@room3d3.com - 1.15.11-1
  - Bump version

* Tue Feb 11 2020 - harbottle@room3d3.com - 1.15.10-1
  - Bump version

* Tue Jan 21 2020 - harbottle@room3d3.com - 1.15.9-1
  - Bump version

* Thu Jan 16 2020 - harbottle@room3d3.com - 1.15.8-1
  - Bump version

* Thu Dec 19 2019 - harbottle@room3d3.com - 1.15.7-1
  - Bump version

* Wed Nov 13 2019 - harbottle@room3d3.com - 1.15.6-1
  - Bump version

* Tue Oct 15 2019 - harbottle@room3d3.com - 1.15.5-1
  - Bump version

* Wed Sep 18 2019 - harbottle@room3d3.com - 1.15.4-1
  - Bump version

* Mon Aug 19 2019 - harbottle@room3d3.com - 1.15.3-1
  - Bump version

* Mon Aug 05 2019 - harbottle@room3d3.com - 1.15.2-1
  - Bump version

* Thu Jul 18 2019 - harbottle@room3d3.com - 1.15.1-1
  - Bump version

* Tue Jul 09 2019 - harbottle@room3d3.com - 1.15.0-1
  - Initial package
