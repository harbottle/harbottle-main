%define __jar_repack 0

Name:     sonarqube-yaml
Version:  1.7.0
Release:  1%{?dist}.harbottle
Summary:  SonarQube YAML plugin
Group:    Applications/System
License:  Apache-2.0
URL:      https://github.com/sbaudoin/sonar-yaml
Source0:  %{url}/releases/download/v%{version}/sonar-yaml-plugin-%{version}.jar
Autoprov: no

%description
YAML plugin for SonarQube.

%install
install -d -m 755 $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins
mv %{SOURCE0} $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins/sonar-yaml-plugin-%{version}.jar

%pre
rm -f %{_var}/lib/sonarqube/extensions/plugins/sonar-yaml-plugin-*.jar
getent group sonarqube >/dev/null || groupadd -f -r sonarqube
getent passwd sonarqube >/dev/null || useradd -r -g sonarqube -d /usr/share/sonarqube -s /sbin/nologin -c "SonarQube user" sonarqube
exit 0

%files
%attr(0644,sonarqube,sonarqube) %{_var}/lib/sonarqube/extensions/plugins/sonar-yaml-plugin-%{version}.jar

%changelog
* Sun Dec 12 2021 - harbottle@room3d3.com - 1.7.0-1
  - Bump version

* Sun Jul 25 2021 - harbottle@room3d3.com - 1.6.0-1
  - Bump version

* Sun Nov 08 2020 - harbottle@room3d3.com - 1.5.2-1
  - Bump version

* Mon Feb 10 2020 - harbottle@room3d3.com - 1.5.1-1
  - Bump version

* Thu Dec 19 2019 - harbottle@room3d3.com - 1.5.0-1
  - Bump version

* Tue Dec 10 2019 - harbottle@room3d3.com - 1.4.3-2
  - Spec file changes for el8

* Fri Dec 06 2019 - harbottle@room3d3.com - 1.4.3-1
  - Initial packaging
