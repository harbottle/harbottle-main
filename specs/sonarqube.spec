%define __jar_repack 0
%global homedir /usr/share/sonarqube

Name:             sonarqube
Version:          7.9.6
Release:          1%{?dist}.harbottle
Summary:          SonarQube is an open source code quality platform
Group:            Applications/System
License:          LGPL-3.0
URL:              https://www.sonarqube.org
Source0:          https://binaries.sonarsource.com/Distribution/sonarqube/sonarqube-%{version}.zip
Source1:          sonarqube.service
Source2:          sonarqube.cron
Source3:          sonarqube-es.sysctl
BuildRequires:    systemd-units dos2unix
Autoprov:         no
Provides:         config(sonarqube) sonarqube SonarQube
Requires:         java-11
Requires:         sonarqube-auth-github
Requires:         sonarqube-auth-saml
Requires:         sonarqube-csharp
Requires:         sonarqube-css
Requires:         sonarqube-flex
Requires:         sonarqube-go
Requires:         sonarqube-html
Requires:         sonarqube-jacoco
Requires:         sonarqube-java
Requires:         sonarqube-javascript
Requires:         sonarqube-kotlin
Requires:         sonarqube-ldap
Requires:         sonarqube-php
Requires:         sonarqube-python
Requires:         sonarqube-ruby
Requires:         sonarqube-scala
Requires:         sonarqube-scm-git
Requires:         sonarqube-scm-svn
Requires:         sonarqube-typescript
Requires:         sonarqube-vbnet
Requires:         sonarqube-xml
Requires(pre):    shadow-utils
Requires(post):   systemd-units
Requires(preun):  systemd-units
Requires(postun): systemd-units

%description
SonarQube (formerly Sonar) is an open source platform for continuous inspection
of code quality to perform automatic reviews with static analysis of code to
detect bugs, code smells and security vulnerabilities on 20+ programming
languages including Java (including Android), C#, PHP, JavaScript, C/C++,
COBOL, PL/SQL, PL/I, ABAP, VB.NET, VB6, Python, RPG, Flex, Objective-C, Swift,
Web and XML. SonarQube offers reports on duplicated code, coding standards, unit
tests, code coverage, code complexity, comments, bugs, and security
vulnerabilities. SonarQube can record metrics history and provides evolution
graphs. SonarQube's greatest asset is that it provides fully automated analysis
and integration with Maven, Ant, Gradle, MSBuild and continuous integration
tools (Atlassian Bamboo, Jenkins, Hudson, etc.). SonarQube also integrates with
Eclipse, Visual Studio and IntelliJ IDEA development environments through the
SonarLint plugins and integrates with external tools like LDAP, Active
Directory, GitHub, etc. SonarQube is expandable with the use of plugins.

%prep
%setup -qn %{name}-%{version}

%install
install -d -m 755 $RPM_BUILD_ROOT%{homedir}
install -d -m 755 $RPM_BUILD_ROOT%{homedir}/bin
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -d -m 755 $RPM_BUILD_ROOT%{_sysconfdir}
install -d -m 755 $RPM_BUILD_ROOT%{_sysconfdir}/%{name}
install -d -m 755 $RPM_BUILD_ROOT%{_sysconfdir}/cron.d
install -d -m 755 $RPM_BUILD_ROOT%{_sysctldir}
install -d -m 755 $RPM_BUILD_ROOT%{_unitdir}
install -d -m 755 $RPM_BUILD_ROOT%{_var}/log
install -d -m 755 $RPM_BUILD_ROOT%{_var}/log/%{name}
install -d -m 755 $RPM_BUILD_ROOT%{_var}/cache
install -d -m 755 $RPM_BUILD_ROOT%{_var}/cache/%{name}
install -d -m 755 $RPM_BUILD_ROOT%{_var}/lib
install -d -m 755 $RPM_BUILD_ROOT%{_var}/lib/%{name}
install -d -m 755 $RPM_BUILD_ROOT%{_var}/lib/%{name}/data

# Elasticsearch additions in 6.7 and above
install -d -m 755 $RPM_BUILD_ROOT%{homedir}/elasticsearch
install -d -m 755 $RPM_BUILD_ROOT%{_libexecdir}
install -d -m 755 $RPM_BUILD_ROOT%{_libexecdir}/%{name}

mv bin/linux-x86-64 $RPM_BUILD_ROOT%{homedir}/bin/linux-x86-64
dos2unix conf/*
mv conf/* $RPM_BUILD_ROOT%{_sysconfdir}/%{name}
rm -f extensions/plugins/*-plugin*.jar
mv extensions $RPM_BUILD_ROOT%{_var}/lib/%{name}
mv lib $RPM_BUILD_ROOT%{homedir}
mv web $RPM_BUILD_ROOT%{homedir}
mv COPYING $RPM_BUILD_ROOT%{homedir}

# Elasticsearch additions in 6.7 and above
rm -f elasticsearch/bin/*.bat
rm -f elasticsearch/bin/*.exe
mv elasticsearch/bin $RPM_BUILD_ROOT%{_libexecdir}/%{name}/elasticsearch
mv elasticsearch/config $RPM_BUILD_ROOT%{_sysconfdir}/%{name}/elasticsearch
mv elasticsearch/* $RPM_BUILD_ROOT%{homedir}/elasticsearch/

ln -s %{_sysconfdir}/%{name} $RPM_BUILD_ROOT%{homedir}/conf
ln -s %{_var}/lib/%{name}/data $RPM_BUILD_ROOT%{homedir}/data
ln -s %{_var}/lib/%{name}/extensions $RPM_BUILD_ROOT%{homedir}/extensions
ln -s %{_var}/log/%{name} $RPM_BUILD_ROOT%{homedir}/logs
ln -s %{_var}/cache/%{name} $RPM_BUILD_ROOT%{homedir}/temp
ln -s %{homedir}/bin/linux-x86-64/sonar.sh $RPM_BUILD_ROOT%{_bindir}/%{name}

# Elasticsearch additions in 6.7 and above
ln -s %{_libexecdir}/%{name}/elasticsearch $RPM_BUILD_ROOT%{homedir}/elasticsearch/bin
ln -s %{_sysconfdir}/%{name}/elasticsearch $RPM_BUILD_ROOT%{homedir}/elasticsearch/config

install -m 644 %{SOURCE1} $RPM_BUILD_ROOT%{_unitdir}/%{name}.service
install -m 644 %{SOURCE2} $RPM_BUILD_ROOT%{_sysconfdir}/cron.d/%{name}
install -m 644 %{SOURCE3} $RPM_BUILD_ROOT%{_sysctldir}/100-sonarqube-es.conf

%pre
getent group %{name} >/dev/null || groupadd -f -r %{name}
getent passwd %{name} >/dev/null || useradd -r -g %{name} -d %{homedir} -s /sbin/nologin -c "SonarQube user" %{name}
alternatives --set java $(readlink -f /etc/alternatives/jre_11)/bin/java
exit 0

%post
%sysctl_apply 100-sonarqube-es.conf
%systemd_post %{name}.service

%preun
%systemd_preun %{name}.service

%postun
%systemd_postun_with_restart %{name}.service

%files
%{_bindir}/%{name}
%{_libexecdir}/%{name}
%config(noreplace) %{_sysconfdir}/%{name}
%config(noreplace) %{_sysconfdir}/cron.d/%{name}
%config(noreplace) %{_sysctldir}/100-sonarqube-es.conf
%{_unitdir}/%{name}.service
%dir %{homedir}
%dir %{homedir}/bin
%dir %attr(-,%{name},%{name}) %{homedir}/bin/linux-x86-64
%{homedir}/bin/linux-x86-64/*
%{homedir}/conf
%{homedir}/data
%{homedir}/elasticsearch
%{homedir}/extensions
%{homedir}/lib
%{homedir}/logs
%{homedir}/temp
%{homedir}/web
%doc %{homedir}/COPYING
%attr(-,%{name},%{name}) %{_var}/cache/%{name}
%attr(-,%{name},%{name}) %{_var}/lib/%{name}
%attr(-,%{name},%{name}) %{_var}/log/%{name}

%changelog
* Tue Mar 02 2021 - harbottle@room3d3.com - 7.9.6-1
  - Bump version

* Wed Nov 11 2020 - harbottle@room3d3.com - 7.9.5-1
  - Bump version

* Tue Jul 28 2020 - harbottle@room3d3.com - 7.9.4-1
  - Bump version

* Tue Mar 24 2020 - harbottle@room3d3.com - 7.9.3-1
  - Bump version

* Tue Dec 10 2019 - harbottle@room3d3.com - 7.9.2-2
  - Spec file changes for el8

* Mon Dec 09 2019 - harbottle@room3d3.com - 7.9.2-1
  - Bump version

* Mon Aug 19 2019 - harbottle@room3d3.com - 7.9.1-3
  - Increase process limit in systemd service

* Sun Jul 21 2019 - harbottle@room3d3.com - 7.9.1-2
  - Improve plugin packaging

* Sat Jul 20 2019 - harbottle@room3d3.com - 7.9.1-1
  - Major version update
  - New Java version
  - New ElasticSearch requirements
  - Fix log rotation
  - Better plugin packaging

* Wed Apr 17 2019 - harbottle@room3d3.com - 6.7.7-1
  - Bump version

* Wed Jan 02 2019 - harbottle@room3d3.com - 6.7.6-1
  - Bump version

* Mon Oct 15 2018 grainger@gmail.com - 6.7.5-1
  - Bump version

* Mon Jun 11 2018 grainger@gmail.com - 6.7.4-1
  - Bump version

* Mon Apr 30 2018 grainger@gmail.com - 6.7.3-1
  - Bump version
  
* Sat Mar 03 2018 grainger@gmail.com - 6.7.2-1
  - Bump version

* Thu Dec 21 2017 grainger@gmail.com - 6.7.1-1
  - Bump version

* Fri Nov 17 2017 grainger@gmail.com - 6.7-3
  - Fix elasticsearch config link

* Fri Nov 17 2017 grainger@gmail.com - 6.7-2
  - Bump iteration due to build complications

* Tue Nov 14 2017 grainger@gmail.com - 6.7-1
  - Bump version
  - Added bundled elasticsearch components

* Thu Oct 12 2017 grainger@gmail.com - 5.6.7-4
  - Remove Windows line endings from conf files

* Tue Oct 10 2017 grainger@gmail.com - 5.6.7-3
  - Fix provides

* Tue Oct 10 2017 grainger@gmail.com - 5.6.7-2
  - Fix systemd

* Mon Oct 09 2017 grainger@gmail.com - 5.6.7-1
  - Initial packaging

