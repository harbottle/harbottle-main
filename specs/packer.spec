Name:          packer
Version:       1.8.2
Release:       1%{?dist}.harbottle
Summary:       Packer is a tool for creating identical machine images for multiple platforms from a single source configuration.
Group:         Applications/System
License:       MPL-2.0
URL:           https://packer.io/
Source0:       https://github.com/hashicorp/%{name}/archive/v%{version}.tar.gz
Source1:       https://releases.hashicorp.com/%{name}/%{version}/%{name}_%{version}_linux_amd64.zip

%description
Packer is a tool for creating identical machine images for multiple platforms
from a single source configuration.
Out of the box Packer comes with support to build images for Amazon EC2,
DigitalOcean, Google Compute Engine, QEMU, VirtualBox, VMware, and more.
Support for more platforms is on the way, and anyone can add new platforms via
plugins.

%prep
%setup -q -n %{name}-%{version}
rm -rf packer
%setup -q -T -D -a 1 -n %{name}-%{version}

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 0755 %{name} $RPM_BUILD_ROOT%{_bindir}
ln -s %{_bindir}/%{name} $RPM_BUILD_ROOT%{_bindir}/%{name}.io

%files
%license LICENSE
%doc {CHANGELOG.md,README.md}
%{_bindir}/%{name}
%{_bindir}/%{name}.io

%changelog
* Tue Jun 21 2022 - harbottle@room3d3.com - 1.8.2-1
  - Bump version

* Fri May 27 2022 - harbottle@room3d3.com - 1.8.1-1
  - Bump version

* Fri Mar 04 2022 - harbottle@room3d3.com - 1.8.0-1
  - Bump version

* Thu Feb 03 2022 - harbottle@room3d3.com - 1.7.10-1
  - Bump version

* Wed Jan 19 2022 - harbottle@room3d3.com - 1.7.9-1
  - Bump version

* Wed Oct 27 2021 - harbottle@room3d3.com - 1.7.8-1
  - Bump version

* Tue Oct 19 2021 - harbottle@room3d3.com - 1.7.7-1
  - Bump version

* Tue Sep 28 2021 - harbottle@room3d3.com - 1.7.6-1
  - Bump version

* Wed Sep 15 2021 - harbottle@room3d3.com - 1.7.5-1
  - Bump version

* Tue Jul 20 2021 - harbottle@room3d3.com - 1.7.4-1
  - Bump version

* Tue Jun 15 2021 - harbottle@room3d3.com - 1.7.3-1
  - Bump version

* Tue Apr 06 2021 - harbottle@room3d3.com - 1.7.2-1
  - Bump version

* Thu Apr 01 2021 - harbottle@room3d3.com - 1.7.1-1
  - Bump version
  - Use binary for build

* Wed Feb 17 2021 - harbottle@room3d3.com - 1.7.0-1
  - Bump version

* Fri Dec 18 2020 - harbottle@room3d3.com - 1.6.6-1
  - Bump version

* Fri Oct 30 2020 - harbottle@room3d3.com - 1.6.5-1
  - Bump version

* Wed Sep 30 2020 - harbottle@room3d3.com - 1.6.4-1
  - Bump version

* Fri Sep 25 2020 - harbottle@room3d3.com - 1.6.3-1
  - Bump version

* Thu Aug 27 2020 - harbottle@room3d3.com - 1.6.2-1
  - Bump version

* Thu Jul 30 2020 - harbottle@room3d3.com - 1.6.1-1
  - Bump version

* Tue Jun 09 2020 - harbottle@room3d3.com - 1.6.0-1
  - Bump version

* Fri May 01 2020 - harbottle@room3d3.com - 1.5.6-1
  - Bump version

* Wed Mar 25 2020 - harbottle@room3d3.com - 1.5.5-1
  - Bump version

* Sat Feb 29 2020 - harbottle@room3d3.com - 1.5.4-1
  - Bump version
  - Fix build

* Thu Feb 13 2020 - harbottle@room3d3.com - 1.5.2-1
  - Bump version

* Fri Dec 20 2019 - harbottle@room3d3.com - 1.5.1-1
  - Bump version

* Fri Dec 20 2019 - harbottle@room3d3.com - 1.5.0-2
  - Build for el8

* Thu Dec 19 2019 - harbottle@room3d3.com - 1.5.0-1
  - Fix build
  - Tidy spec file
  - Bump version

* Mon Nov 04 2019 - harbottle@room3d3.com - 1.4.5-1
  - Bump version

* Tue Oct 01 2019 - harbottle@room3d3.com - 1.4.4-1
  - Bump version

* Thu Aug 15 2019 - harbottle@room3d3.com - 1.4.3-1
  - Bump version

* Wed Jun 26 2019 - harbottle@room3d3.com - 1.4.2-1
  - Bump version

* Mon Jun 03 2019 - harbottle@room3d3.com - 1.4.1-1
  - Bump version

* Fri Apr 12 2019 - harbottle@room3d3.com - 1.4.0-1
  - Bump version

* Thu Mar 07 2019 - harbottle@room3d3.com - 1.3.5-2
  - Fix BuildRequires

* Mon Mar 04 2019 - harbottle@room3d3.com - 1.3.5-1
  - Bump version

* Sat Feb 02 2019 - harbottle@room3d3.com - 1.3.4-2
  - Build from source

* Wed Jan 30 2019 - harbottle@room3d3.com - 1.3.4-1
  - Bump version

* Mon Jan 07 2019 - harbottle@room3d3.com - 1.3.3-1
  - Add docs, tidy-up, add packer.io symlink
   (https://github.com/hashicorp/packer/issues/1117)

* Mon Jan 07 2019 - vlinx@yahoo.com - 1.3.3-1
  - Initial spec, based on
    https://github.com/phrawzty/packer-rpm/blob/master/packer.spec
