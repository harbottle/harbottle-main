Name:           posh-git-bash
Version:        1.4.0
Release:        1%{?dist}.harbottle
Summary:        Bash version of the posh-git command prompt

License:        GPL-3.0-only
URL:            https://github.com/lyze/posh-git-sh                
Source0:        %{url}/archive/refs/tags/%{version}.tar.gz
Source1:        posh-git.sh

Requires:       bash
Requires:       bash-completion
Requires:       git

%description
This script allows you to see the status of the current git repository in your
prompt. It replicates the prompt status from the Windows PowerShell module
dahlbyk/posh-git (https://github.com/dahlbyk/posh-git).

%prep
%setup -qn posh-git-sh-%{version}

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -d -m 755 $RPM_BUILD_ROOT%{_sysconfdir}
install -d -m 755 $RPM_BUILD_ROOT%{_sysconfdir}/profile.d
install -m 755 git-prompt.sh $RPM_BUILD_ROOT%{_bindir}/posh-git
install -m 644 %{SOURCE1} $RPM_BUILD_ROOT%{_sysconfdir}/profile.d/posh-git.sh

%files
%doc README.md
%{_bindir}/posh-git
%{_sysconfdir}/profile.d/posh-git.sh

%changelog
* Tue Jun 15 2021 - harbottle@room3d3.com - 1.4.0-1
  - Bump version
  - Simplify spec
  - Update license

* Tue Jun 23 2020 - harbottle@room3d3.com - 1.3.0-1
  - Bump version

* Wed Apr 03 2019 - harbottle@room3d3.com - 1.2.0-1
  - Bump version

* Sat Jan 26 2019 - harbottle@room3d3.com - 1.1.1-3
  - Add harbottle to release

* Wed Aug 23 2017 <grainger@gmail.com>
- Initial packaging
