%define __jar_repack 0

Name:     sonarqube-java
Version:  7.13.0.29990
Release:  1%{?dist}.harbottle
Summary:  SonarQube Java plugin
Group:    Applications/System
License:  LGPL-3.0
Source0:  https://binaries.sonarsource.com/Distribution/sonar-java-plugin/sonar-java-plugin-%{version}.jar
Autoprov: no
Requires: sonarqube

%description
Java plugin for SonarQube.

%prep

%install
install -d -m 755 $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins
mv %{SOURCE0} $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins

%pre
rm -f %{_var}/lib/sonarqube/extensions/plugins/sonar-java-plugin-*.jar
getent group sonarqube >/dev/null || groupadd -f -r sonarqube
getent passwd sonarqube >/dev/null || useradd -r -g sonarqube -d /usr/share/sonarqube -s /sbin/nologin -c "SonarQube user" sonarqube
exit 0

%files
%attr(0644,sonarqube,sonarqube) %{_var}/lib/sonarqube/extensions/plugins/sonar-java-plugin-%{version}.jar

%changelog
* Mon Jul 04 2022 - harbottle@room3d3.com - 7.13.0.29990-1
  - Bump version

* Thu Jun 16 2022 - harbottle@room3d3.com - 7.12.1.29810-1
  - Bump version

* Mon May 16 2022 - harbottle@room3d3.com - 7.12.0.29739-1
  - Bump version

* Wed Mar 30 2022 - harbottle@room3d3.com - 7.11.0.29148-1
  - Bump version

* Fri Mar 25 2022 - harbottle@room3d3.com - 7.10.0.29108-1
  - Bump version

* Mon Mar 07 2022 - harbottle@room3d3.com - 7.9.0.28969-1
  - Bump version

* Tue Feb 08 2022 - harbottle@room3d3.com - 7.8.1.28740-1
  - Bump version

* Fri Jan 28 2022 - harbottle@room3d3.com - 7.8.0.28662-1
  - Bump version

* Tue Jan 18 2022 - harbottle@room3d3.com - 7.7.0.28547-1
  - Bump version

* Tue Jan 18 2022 - harbottle@room3d3.com - 7.7.0.28540-1
  - Bump version

* Sun Dec 12 2021 - harbottle@room3d3.com - 7.6.0.28201-1
  - Bump version

* Tue Oct 19 2021 - harbottle@room3d3.com - 7.4.0.27839-1
  - Bump version

* Mon Sep 06 2021 - harbottle@room3d3.com - 7.3.0.27589-1
  - Bump version

* Tue Jul 20 2021 - harbottle@room3d3.com - 7.2.0.26923-1
  - Bump version

* Fri Jun 25 2021 - harbottle@room3d3.com - 7.1.0.26670-1
  - Bump version

* Tue Jun 08 2021 - harbottle@room3d3.com - 7.0.0.26422-1
  - Bump version

* Thu Apr 29 2021 - harbottle@room3d3.com - 6.15.1.26025-1
  - Bump version

* Thu Apr 15 2021 - harbottle@room3d3.com - 6.15.0.25849-1
  - Bump version

* Fri Mar 19 2021 - harbottle@room3d3.com - 6.14.0.25463-1
  - Bump version

* Mon Feb 22 2021 - harbottle@room3d3.com - 6.13.0.25138-1
  - Bump version

* Mon Feb 01 2021 - harbottle@room3d3.com - 6.12.0.24852-1
  - Bump version

* Wed Jan 13 2021 - harbottle@room3d3.com - 6.11.0.24617-1
  - Bump version

* Tue Jan 12 2021 - harbottle@room3d3.com - 6.11.0.24599-1
  - Bump version

* Mon Dec 07 2020 - harbottle@room3d3.com - 6.10.0.24201-1
  - Bump version

* Mon Oct 05 2020 - harbottle@room3d3.com - 6.9.0.23563-1
  - Bump version

* Wed Sep 23 2020 - harbottle@room3d3.com - 6.8.0.23379-1
  - Bump version

* Mon Aug 31 2020 - harbottle@room3d3.com - 6.7.0.23054-1
  - Bump version

* Fri Jul 24 2020 - harbottle@room3d3.com - 6.6.0.22815-1
  - Bump version

* Thu Jul 02 2020 - harbottle@room3d3.com - 6.5.1.22586-1
  - Bump version

* Fri Jun 19 2020 - harbottle@room3d3.com - 6.5.0.22421-1
  - Bump version

* Thu May 14 2020 - harbottle@room3d3.com - 6.4.0.21967-1
  - Bump version

* Wed Apr 08 2020 - harbottle@room3d3.com - 6.3.0.21585-1
  - Bump version

* Mon Mar 09 2020 - harbottle@room3d3.com - 6.2.0.21135-1
  - Bump version

* Fri Feb 14 2020 - harbottle@room3d3.com - 6.1.0.20866-1
  - Bump version

* Fri Jan 31 2020 - harbottle@room3d3.com - 6.0.2.20657-1
  - Bump version

* Mon Jan 13 2020 - harbottle@room3d3.com - 6.0.1.20589-1
  - Bump version

* Fri Dec 20 2019 - harbottle@room3d3.com - 6.0.0.20538-1
  - Bump version

* Tue Dec 10 2019 - harbottle@room3d3.com - 5.14.0.18788-2
  - Spec file changes for el8

* Mon Aug 05 2019 - harbottle@room3d3.com - 5.14.0.18788-1
  - Bump version

* Sun Jul 21 2019 - harbottle@room3d3.com - 5.13.1.18282-3
  - Fix plugin ownership

* Sun Jul 21 2019 - harbottle@room3d3.com - 5.13.1.18282-2
  - Standardize SonarQube plugins

* Thu Jun 27 2019 - harbottle@room3d3.com - 5.13.1.18282-1
  - Bump version

* Mon Jun 03 2019 - harbottle@room3d3.com - 5.13.0.18197-1
  - Bump version

* Tue Apr 16 2019 - harbottle@room3d3.com - 5.12.1.17771-1
  - Bump version

* Mon Apr 08 2019 - harbottle@room3d3.com - 5.12.0.17701-1
  - Bump version

* Tue Feb 19 2019 - harbottle@room3d3.com - 5.11.0.17289-1
  - Bump version

* Wed Jan 30 2019 - harbottle@room3d3.com - 5.10.2.17019-1
  - Bump version

* Fri Jan 18 2019 - harbottle@room3d3.com - 5.10.1.16922-1
  - Bump version

* Wed Jan 02 2019 - harbottle@room3d3.com - 5.9.2.16552-1
  - Bump version

* Mon Oct 15 2018 grainger@gmail.com - 5.8.0.15699-1
  - Bump version

* Mon Aug 06 2018 grainger@gmail.com - 5.6.0.15032-1
  - Bump version

* Mon Jul 02 2018 grainger@gmail.com - 5.5.0.14655-1
  - Bump version

* Mon Jun 11 2018 grainger@gmail.com - 5.4.0.14284-1
  - Bump version

* Mon Apr 30 2018 grainger@gmail.com - 5.3.0.13828-1
  - Bump version

* Fri Apr 06 2018 grainger@gmail.com - 5.2.0.13398-1
  - Bump version

* Sat Mar 03 2018 grainger@gmail.com - 5.1.1.13214-1
  - Bump version

* Thu Dec 21 2017 grainger@gmail.com - 5.0.0.12796-1
  - Bump version

* Fri Nov 17 2017 grainger@gmail.com - 4.15.0.12310-2
  - Bump iteration due to build complications

* Tue Nov 14 2017 grainger@gmail.com - 4.15.0.12310-1
  - Bump version
* Mon Oct 23 2017 grainger@gmail.com - 4.14.0.11784-1
  - Initial packaging
