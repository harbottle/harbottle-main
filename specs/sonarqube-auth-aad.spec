%define __jar_repack 0

Name:     sonarqube-auth-aad
Version:  1.2.0
Release:  1%{?dist}.harbottle
Summary:  SonarQube Azure Active Directory authentication plugin
Group:    Applications/System
License:  MIT
URL:      https://github.com/hkamel/sonar-auth-aad
Source0:  %{url}/releases/download/%{version}/sonar-auth-aad-plugin-%{version}.jar
Autoprov: no

%description
Azure Active Directory authentication plugin for SonarQube.

%install
install -d -m 755 $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins
mv %{SOURCE0} $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins/sonar-auth-aad-plugin-%{version}.jar

%pre
rm -f %{_var}/lib/sonarqube/extensions/plugins/sonar-auth-aad-plugin-*.jar
getent group sonarqube >/dev/null || groupadd -f -r sonarqube
getent passwd sonarqube >/dev/null || useradd -r -g sonarqube -d /usr/share/sonarqube -s /sbin/nologin -c "SonarQube user" sonarqube
exit 0

%files
%attr(0644,sonarqube,sonarqube) %{_var}/lib/sonarqube/extensions/plugins/sonar-auth-aad-plugin-%{version}.jar

%changelog
* Thu Feb 06 2020 - harbottle@room3d3.com - 1.2.0-1
  - Bump version

* Tue Dec 10 2019 - harbottle@room3d3.com - 1.1-2
  - Spec file changes for el8

* Fri Dec 06 2019 - harbottle@room3d3.com - 1.1-1
  - Fix version

* Fri Dec 06 2019 - harbottle@room3d3.com - 1.1.0-1
  - Initial packaging
