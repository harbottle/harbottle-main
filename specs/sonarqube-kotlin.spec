%define __jar_repack 0

Name:     sonarqube-kotlin
Version:  2.9.0.1147
Release:  1%{?dist}.harbottle
Summary:  SonarQube Kotlin plugin
Group:    Applications/System
License:  LGPL-3.0
Source0:  https://binaries.sonarsource.com/Distribution/sonar-kotlin-plugin/sonar-kotlin-plugin-%{version}.jar
Autoprov: no

%description
Kotlin plugin for SonarQube.

%prep

%install
install -d -m 755 $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins
mv %{SOURCE0} $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins

%pre
rm -f %{_var}/lib/sonarqube/extensions/plugins/sonar-kotlin-plugin-*.jar
getent group sonarqube >/dev/null || groupadd -f -r sonarqube
getent passwd sonarqube >/dev/null || useradd -r -g sonarqube -d /usr/share/sonarqube -s /sbin/nologin -c "SonarQube user" sonarqube
exit 0

%files
%attr(0644,sonarqube,sonarqube) %{_var}/lib/sonarqube/extensions/plugins/sonar-kotlin-plugin-%{version}.jar

%changelog
* Tue Jan 18 2022 - harbottle@room3d3.com - 2.9.0.1147-1
  - Bump version

* Mon Jan 17 2022 - harbottle@room3d3.com - 2.9.0.1141-1
  - Bump version

* Sun Dec 12 2021 - harbottle@room3d3.com - 2.8.0.1093-1
  - Bump version

* Fri Oct 22 2021 - harbottle@room3d3.com - 2.5.0.754-1
  - Bump version

* Thu Oct 14 2021 - harbottle@room3d3.com - 2.4.0.703-1
  - Bump version

* Fri Oct 01 2021 - harbottle@room3d3.com - 2.3.0.609-1
  - Bump version

* Wed Sep 08 2021 - harbottle@room3d3.com - 2.2.0.499-1
  - Bump version

* Mon Aug 02 2021 - harbottle@room3d3.com - 2.1.0.344-1
  - Bump version

* Thu Jul 01 2021 - harbottle@room3d3.com - 2.0.1.110-1
  - Bump version

* Thu Jun 24 2021 - harbottle@room3d3.com - 2.0.0.29-1
  - Bump version

* Thu Apr 29 2021 - harbottle@room3d3.com - 1.8.3.2219-1
  - Bump version

* Thu Apr 29 2021 - harbottle@room3d3.com - 1.8.3.2215-1
  - Bump version

* Wed Jan 13 2021 - harbottle@room3d3.com - 1.8.2.1946-1
  - Bump version

* Fri Nov 06 2020 - harbottle@room3d3.com - 1.8.1.1804-1
  - Bump version

* Tue Oct 27 2020 - harbottle@room3d3.com - 1.8.0.1775-1
  - Bump version

* Tue Dec 10 2019 - harbottle@room3d3.com - 1.7.0.883-2
  - Spec file changes for el8

* Fri Aug 16 2019 - harbottle@room3d3.com - 1.7.0.883-1
  - Bump version

* Tue Jul 30 2019 - harbottle@room3d3.com - 1.6.0.719-1
  - Bump version

* Sun Jul 21 2019 - harbottle@room3d3.com - 1.5.0.315-2
  - Fix plugin ownership

* Sun Jul 21 2019 - harbottle@room3d3.com - 1.5.0.315-1
  - Initial package
