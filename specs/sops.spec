Name:    sops
Version: 3.7.3
Release: 1%{?dist}.harbottle
Summary: sops is an editor of encrypted files
Group:   Applications/System
License: MPL-2.0
Url:     https://github.com/mozilla/%{name}
Source0: %{url}/archive/v%{version}.tar.gz
Source1: %{url}/releases/download/v%{version}/%{name}-v%{version}.linux.amd64

%description
sops is an editor of encrypted files that supports YAML, JSON and BINARY formats
and encrypts with AWS KMS, GCP KMS, Azure Key Vault and PGP.

%prep
%setup -q -n %{name}-%{version}

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 755 %{SOURCE1} $RPM_BUILD_ROOT%{_bindir}/%{name}

%files
%license LICENSE
%doc {CHANGELOG.rst,README.rst}
%{_bindir}/%{name}

%changelog
* Tue May 10 2022 - harbottle@room3d3.com - 3.7.3-1
  - Use binary
  - Bump version

* Wed Mar 09 2022 - harbottle@room3d3.com - 3.7.2-1
  - Bump version

* Thu Apr 08 2021 - harbottle@room3d3.com - 3.7.1-1
  - Bump version

* Wed Mar 24 2021 - harbottle@room3d3.com - 3.7.0-1
  - Bump version

* Mon Sep 14 2020 - harbottle@room3d3.com - 3.6.1-1
  - Bump version

* Fri Jul 24 2020 - harbottle@room3d3.com - 3.6.0-1
  - Bump version

* Tue Dec 10 2019 - harbottle@room3d3.com - 3.5.0-1
  - Fix source URL
  - Tidy spec file
  - Bump version

* Thu Sep 12 2019 - harbottle@room3d3.com - 3.4.0-1
  - Bump version

* Tue Jun 11 2019 - harbottle@room3d3.com - 3.3.1-1
  - Bump version

* Thu Apr 18 2019 - harbottle@room3d3.com - 3.3.0-1
  - Bump version

* Tue Jan 08 2019 - harbottle@room3d3.com - 3.2.0-1
  - Initial package
