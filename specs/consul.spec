%global confdir %{_sysconfdir}/%{name}.d
%global datadir %{_var}/lib/%{name}/

Name:             consul
Version:          1.12.2
Release:          1%{?dist}.harbottle
Summary:          Consul is a tool for service discovery and configuration
Group:            Applications/System
License:          MPL-2.0
URL:              https://consul.io/
Source0:          https://github.com/hashicorp/%{name}/archive/v%{version}.tar.gz
Source1:          https://releases.hashicorp.com/%{name}/%{version}/%{name}_%{version}_linux_amd64.zip
Source2:          %{name}.hcl
Source3:          %{name}.service
Requires(pre):    shadow-utils
Requires(post):   systemd-units
Requires(preun):  systemd-units
Requires(postun): systemd-units
%if 0%{?rhel} == 8
BuildRequires:    systemd-rpm-macros
%endif

%description
Consul is a tool for service discovery and configuration. Consul is distributed,
highly available, and extremely scalable.

Consul provides several key features:

- Service Discovery - Consul makes it simple for services to register themselves
  and to discover other services via a DNS or HTTP interface. External services
  such as SaaS providers can be registered as well.

- Health Checking - Health Checking enables Consul to quickly alert operators
  about any issues in a cluster. The integration with service discovery prevents
  routing traffic to unhealthy hosts and enables service level circuit breakers.

- Key/Value Storage - A flexible key/value store enables storing dynamic
  configuration, feature flagging, coordination, leader election and more. The
  simple HTTP API makes it easy to use anywhere.

- Multi-Datacenter - Consul is built to be datacenter aware, and can support any
  number of regions without complex configuration.

- Service Segmentation - Consul Connect enables secure service-to-service
  communication with automatic TLS encryption and identity-based authorization.

%prep
%setup -q -n %{name}-%{version}
%setup -q -T -D -a 1 -n %{name}-%{version}

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -d -m 755 $RPM_BUILD_ROOT%{confdir}
install -d -m 755 $RPM_BUILD_ROOT%{datadir}
install -d -m 755 $RPM_BUILD_ROOT%{_unitdir}

install -m 0755 %{name} $RPM_BUILD_ROOT%{_bindir}
install -m 0640 %{SOURCE2} $RPM_BUILD_ROOT%{confdir}
install -m 0644 %{SOURCE3} $RPM_BUILD_ROOT%{_unitdir}

%pre
getent group %{name} >/dev/null || groupadd -f -r %{name}
getent passwd %{name} >/dev/null || useradd -r -g %{name} -d %{confdir} -s /sbin/nologin -c "%{name} service user" %{name}
exit 0

%post
%systemd_post %{name}.service

%preun
%systemd_preun %{name}.service

%postun
%systemd_postun_with_restart %{name}.service

%files
%license {LICENSE,NOTICE.md}
%doc {CHANGELOG.md,INTERNALS.md,README.md}
%{_bindir}/%{name}
%attr(-,%{name},%{name}) %config(noreplace) %{confdir}
%attr(-,%{name},%{name}) %{datadir}
%{_unitdir}/%{name}.service

%changelog
* Fri Jun 03 2022 - harbottle@room3d3.com - 1.12.2-1
  - Bump version

* Thu May 26 2022 - harbottle@room3d3.com - 1.12.1-1
  - Bump version

* Wed Apr 20 2022 - harbottle@room3d3.com - 1.12.0-1
  - Bump version

* Thu Apr 14 2022 - harbottle@room3d3.com - 1.11.5-1
  - Bump version

* Mon Feb 28 2022 - harbottle@room3d3.com - 1.11.4-1
  - Bump version

* Sat Feb 12 2022 - harbottle@room3d3.com - 1.11.3-1
  - Bump version

* Sat Jan 15 2022 - harbottle@room3d3.com - 1.11.2-1
  - Bump version

* Sun Dec 12 2021 - harbottle@room3d3.com - 1.9.0-1
  - Fix build
  - Bump version

* Mon Sep 27 2021 - harbottle@room3d3.com - 1.10.3-1
  - Bump version

* Fri Aug 27 2021 - harbottle@room3d3.com - 1.10.2-1
  - Bump version

* Thu Jul 15 2021 - harbottle@room3d3.com - 1.10.1-1
  - Bump version

* Tue Jun 22 2021 - harbottle@room3d3.com - 1.10.0-1
  - Bump version

* Tue Jun 22 2021 - harbottle@room3d3.com - 1.9.7-1
  - Bump version

* Fri Jun 04 2021 - harbottle@room3d3.com - 1.9.6-1
  - Bump version

* Thu Apr 15 2021 - harbottle@room3d3.com - 1.9.5-1
  - Bump version

* Fri Mar 05 2021 - harbottle@room3d3.com - 1.9.4-1
  - Bump version

* Mon Feb 01 2021 - harbottle@room3d3.com - 1.9.3-1
  - Bump version

* Thu Jan 21 2021 - harbottle@room3d3.com - 1.9.2-1
  - Bump version

* Fri Dec 11 2020 - harbottle@room3d3.com - 1.9.1-1
  - Bump version

* Tue Nov 24 2020 - harbottle@room3d3.com - 1.9.0-1
  - Bump version

* Fri Nov 20 2020 - harbottle@room3d3.com - 1.8.6-1
  - Bump version

* Fri Oct 23 2020 - harbottle@room3d3.com - 1.8.5-1
  - Bump version

* Fri Sep 11 2020 - harbottle@room3d3.com - 1.8.4-1
  - Bump version

* Wed Aug 12 2020 - harbottle@room3d3.com - 1.8.3-1
  - Bump version

* Fri Aug 07 2020 - harbottle@room3d3.com - 1.8.2-1
  - Bump version

* Fri Jul 31 2020 - harbottle@room3d3.com - 1.8.1-2
  - Fix build

* Fri Jul 31 2020 - harbottle@room3d3.com - 1.8.1-1
  - Bump version

* Thu Jun 18 2020 - harbottle@room3d3.com - 1.8.0-1
  - Bump version

* Wed Jun 10 2020 - harbottle@room3d3.com - 1.7.4-1
  - Bump version

* Wed May 06 2020 - harbottle@room3d3.com - 1.7.3-1
  - Bump version

* Mon Mar 16 2020 - harbottle@room3d3.com - 1.7.2-1
  - Bump version

* Sat Feb 29 2020 - harbottle@room3d3.com - 1.7.1-1
  - Bump version
  - Fix build

* Tue Feb 11 2020 - harbottle@room3d3.com - 1.7.0-1
  - Bump version

* Thu Jan 30 2020 - harbottle@room3d3.com - 1.6.3-1
  - Bump version

* Fri Dec 20 2019 - harbottle@room3d3.com - 1.6.2-2
  - Build for el8
  - Tidy spec file

* Thu Nov 14 2019 - harbottle@room3d3.com - 1.6.2-1
  - Bump version

* Thu Sep 12 2019 - harbottle@room3d3.com - 1.6.1-1
  - Bump version

* Mon Aug 26 2019 - harbottle@room3d3.com - 1.6.0-1
  - Bump version

* Fri Jul 26 2019 - harbottle@room3d3.com - 1.5.3-1
  - Bump version

* Thu Jun 27 2019 - harbottle@room3d3.com - 1.5.2-1
  - Bump version

* Mon Jun 03 2019 - harbottle@room3d3.com - 1.5.1-1
  - Bump version

* Thu Mar 21 2019 - harbottle@room3d3.com - 1.4.4-1
  - Bump version

* Tue Mar 05 2019 - harbottle@room3d3.com - 1.4.3-1
  - Bump version

* Sat Feb 02 2019 - harbottle@room3d3.com - 1.4.2-2
  - Mark config files with noreplace

* Sat Feb 02 2019 - harbottle@room3d3.com - 1.4.2-1
  - Initial package
