Name:     bower
Version:  1.8.14
Release:  1%{?dist}.harbottle
Summary:  A package manager for the web
Group:    Applications/System
License:  MIT
Url:      https://%{modname}.io
Requires: nodejs-bower = %{version}

%description
Bower offers a generic, unopinionated solution to the problem of front-end
package management, while exposing the package dependency model via an API that
can be consumed by a more opinionated build stack. There are no system wide
dependencies, no dependencies are shared between different apps, and the
dependency tree is flat.

Bower runs over Git, and is package-agnostic. A packaged component can be made
up of any type of asset, and use any type of transport (e.g., AMD, CommonJS,
etc.).

%prep

%build

%files

%changelog
* Mon Mar 14 2022 - harbottle@room3d3.com - 1.8.14-1
  - Fix build
  - Bump version

* Sun Dec 12 2021 - harbottle@room3d3.com - 1.8.13-1
  - Bump version

* Mon Jan 18 2021 - harbottle@room3d3.com - 1.8.12-1
  - Bump version

* Thu Jan 14 2021 - harbottle@room3d3.com - 1.8.10-1
  - Bump version

* Sat Jan 04 2020 - harbottle@room3d3.com - 1.8.8-2
  - Initial package
