Name:           pulp-release
Version:        2
Release:        3.el7
Summary:        Pulp repository configuration
Group:          System Environment/Base
License:        GPLv2
URL:            http://pulpproject.org/
Source0:        RPM-GPG-KEY-pulp-2
Source1:        https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
Source2:        pulp.repo

BuildArch:     noarch
Requires:      redhat-release >=  7

%description
This package contains the Pulp repository
GPG key as well as configuration for yum.

%prep
%setup -q  -c -T
install -pm 644 %{SOURCE0} .
install -pm 644 %{SOURCE1} ./GPL

%build

%install
rm -rf $RPM_BUILD_ROOT
install -Dpm 644 %{SOURCE0} $RPM_BUILD_ROOT%{_sysconfdir}/pki/rpm-gpg/RPM-GPG-KEY-pulp-2
install -dm 755 $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d
install -pm 644 %{SOURCE2} $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc GPL
%config(noreplace) /etc/yum.repos.d/*
/etc/pki/rpm-gpg/*

%changelog
* Sat Jul 14 2018 <grainger@gmail.com> -  2-3.el7
- Fix GPG key filename

* Wed Aug 23 2017 <grainger@gmail.com> -  2-2.el7
- Fix release number

* Fri Jun 16 2017 grainger@gmail.com
- Initial packaging
