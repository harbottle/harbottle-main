Name:    mmctl
Version: 7.0.1
Release: 1%{?dist}.harbottle
Summary: A remote CLI tool for Mattermost
Group:   Applications/System
License: Apache-2.0
Url:     https://github.com/mattermost/%{name}
Source0: %{url}/archive/v%{version}.tar.gz
Source1: %{url}/releases/download/v%{version}/linux_amd64.tar

%description
A remote CLI tool for Mattermost: the Open Source, self-hosted
Slack-alternative.

%prep
%setup -q
%setup -q -T -D -a 1

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 755 %{name} $RPM_BUILD_ROOT%{_bindir}

%files
%license LICENSE.txt
%doc *.md docs/*
%{_bindir}/%{name}

%changelog
* Fri Jun 24 2022 - harbottle@room3d3.com - 7.0.1-1
  - Bump version

* Tue Jun 14 2022 - harbottle@room3d3.com - 7.0.0-1
  - Bump version

* Tue Jun 14 2022 - harbottle@room3d3.com - 6.7.1-1
  - Bump version

* Tue May 17 2022 - harbottle@room3d3.com - 6.7.0-1
  - Bump version

* Thu Apr 28 2022 - harbottle@room3d3.com - 6.6.1-1
  - Bump version

* Thu Apr 14 2022 - harbottle@room3d3.com - 6.6.0-1
  - Bump version

* Wed Mar 30 2022 - harbottle@room3d3.com - 6.5.0-1
  - Bump version

* Fri Mar 11 2022 - harbottle@room3d3.com - 6.4.2-1
  - Bump version

* Tue Feb 08 2022 - harbottle@room3d3.com - 6.3.3-1
  - Bump version

* Sat Jan 15 2022 - harbottle@room3d3.com - 6.3.0-1
  - Bump version

* Sat Jan 15 2022 - harbottle@room3d3.com - 6.2.1-1
  - Bump version

* Mon Nov 01 2021 - harbottle@room3d3.com - 6.1.0-1
  - Bump version

* Tue Oct 12 2021 - harbottle@room3d3.com - 6.0.0-1
  - Bump version

* Thu Aug 26 2021 - harbottle@room3d3.com - 5.39.1-1
  - Bump version

* Tue Aug 24 2021 - harbottle@room3d3.com - 5.38.1-1
  - Bump version

* Sun Aug 22 2021 - harbottle@room3d3.com - 5.38.0-1
  - Bump version

* Thu Jun 03 2021 - harbottle@room3d3.com - 5.36.0-1
  - Bump version

* Thu Apr 22 2021 - harbottle@room3d3.com - 5.35.0-1
  - Bump version

* Thu Mar 25 2021 - harbottle@room3d3.com - 5.34.0-1
  - Bump version

* Mon Mar 22 2021 - harbottle@room3d3.com - 5.33.1-1
  - Bump version

* Thu Jan 28 2021 - harbottle@room3d3.com - 5.32.0-1
  - Bump version

* Tue Dec 22 2020 - harbottle@room3d3.com - 5.31.0-1
  - Bump version

* Thu Dec 03 2020 - harbottle@room3d3.com - 5.30.0-1
  - Bump version

* Mon Oct 19 2020 - harbottle@room3d3.com - 5.29.0-1
  - Update version
  - Package from official binary
  - Include license

* Sun Mar 15 2020 - harbottle@room3d3.com - 5.21.0-1
  - Initial package
