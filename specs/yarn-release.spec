Name:           yarn-release
Version:        1
Release:        1.el7
Summary:        Yarn repository configuration
Group:          System Environment/Base
License:        BSD 3
URL:            https://yarnpkg.com
Source0:        RPM-GPG-KEY-YARN
Source1:        yarn.repo

BuildArch:     noarch
Requires:      redhat-release >=  7

%description
This package contains the Yarn repository GPG key as well as configuration
for yum.

%prep
%setup -q  -c -T
install -pm 644 %{SOURCE0} .
install -pm 644 %{SOURCE1} .

%build

%install
rm -rf $RPM_BUILD_ROOT
install -Dpm 644 %{SOURCE0} $RPM_BUILD_ROOT%{_sysconfdir}/pki/rpm-gpg/RPM-GPG-KEY-YARN
install -dm 755 $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d
install -pm 644 %{SOURCE1} $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%config(noreplace) /etc/yum.repos.d/*
/etc/pki/rpm-gpg/*

%changelog
* Wed Aug 23 2017 <grainger@gmail.com> -  1-1.el7
- Initial packaging
