%define __jar_repack 0

Name:     sonarqube-dependency-check
Version:  3.0.1
Release:  1%{?dist}.harbottle
Summary:  SonarQube Dependency-Check plugin
Group:    Applications/System
License:  LGPL-3.0
URL:      https://github.com/dependency-check/dependency-check-sonar-plugin
Source0:  %{url}/releases/download/%{version}/sonar-dependency-check-plugin-%{version}.jar
Autoprov: no

%description
Dependency-Check plugin for SonarQube.

%prep

%install
install -d -m 755 $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins
mv %{SOURCE0} $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins

%pre
rm -f %{_var}/lib/sonarqube/extensions/plugins/sonar-dependency-check-plugin-*.jar
getent group sonarqube >/dev/null || groupadd -f -r sonarqube
getent passwd sonarqube >/dev/null || useradd -r -g sonarqube -d /usr/share/sonarqube -s /sbin/nologin -c "SonarQube user" sonarqube
exit 0

%files
%attr(0644,sonarqube,sonarqube) %{_var}/lib/sonarqube/extensions/plugins/sonar-dependency-check-plugin-%{version}.jar

%changelog
* Fri Feb 25 2022 - harbottle@room3d3.com - 3.0.1-1
  - Bump version

* Thu Feb 10 2022 - harbottle@room3d3.com - 3.0.0-1
  - Bump version

* Mon May 31 2021 - harbottle@room3d3.com - 2.0.8-1
  - Bump version

* Tue Dec 22 2020 - harbottle@room3d3.com - 2.0.7-1
  - Bump version

* Tue Sep 15 2020 - harbottle@room3d3.com - 2.0.6-1
  - Bump version

* Wed Jun 10 2020 - harbottle@room3d3.com - 2.0.5-1
  - Bump version

* Thu Mar 26 2020 - harbottle@room3d3.com - 2.0.4-1
  - Bump version

* Sat Mar 07 2020 - harbottle@room3d3.com - 2.0.3-1
  - Bump version

* Tue Jan 28 2020 - harbottle@room3d3.com - 2.0.2-1
  - Bump version

* Thu Jan 16 2020 - harbottle@room3d3.com - 2.0.1-1
  - Bump version

* Wed Jan 15 2020 - harbottle@room3d3.com - 2.0.0-1
  - Bump version

* Fri Jan 03 2020 - harbottle@room3d3.com - 1.2.6-2
  - Tidy spec file

* Fri Jan 03 2020 - harbottle@room3d3.com - 1.2.6-1
  - Initial package
