Name:          helm
Version:       3.9.0
Release:       1%{?dist}.harbottle
Summary:       The package manager for Kubernetes
Group:         Applications/System
License:       Apache-2.0
Url:           https://github.com/%{name}/%{name}
Source0:       https://get.helm.sh/%{name}-v%{version}-linux-amd64.tar.gz

%description
Helm helps you manage Kubernetes applications — Helm Charts helps you define,
install, and upgrade even the most complex Kubernetes application.

Charts are easy to create, version, share, and publish — so start using Helm and
stop the copy-and-paste.

The latest version of Helm is maintained by the CNCF - in collaboration with
Microsoft, Google, Bitnami and the Helm contributor community.

%prep
%setup -q -n linux-amd64

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 755 %{name} $RPM_BUILD_ROOT%{_bindir}

%files
%license LICENSE
%doc README.md
%{_bindir}/helm

%changelog
* Wed May 18 2022 - harbottle@room3d3.com - 3.9.0-1
  - Bump version

* Wed Apr 13 2022 - harbottle@room3d3.com - 3.8.2-1
  - Bump version

* Wed Mar 09 2022 - harbottle@room3d3.com - 3.8.1-1
  - Bump version

* Mon Jan 24 2022 - harbottle@room3d3.com - 3.8.0-1
  - Bump version

* Sun Dec 12 2021 - harbottle@room3d3.com - 3.7.2-1
  - Bump version

* Wed Oct 13 2021 - harbottle@room3d3.com - 3.7.1-1
  - Bump version

* Thu Sep 16 2021 - harbottle@room3d3.com - 3.7.0-1
  - Bump version

* Wed Jul 14 2021 - harbottle@room3d3.com - 3.6.3-1
  - Bump version

* Tue Jun 29 2021 - harbottle@room3d3.com - 3.6.2-1
  - Bump version

* Wed Jun 16 2021 - harbottle@room3d3.com - 3.6.1-1
  - Bump version

* Thu May 27 2021 - harbottle@room3d3.com - 3.6.0-1
  - Bump version

* Wed Apr 14 2021 - harbottle@room3d3.com - 3.5.4-1
  - Bump version

* Thu Mar 11 2021 - harbottle@room3d3.com - 3.5.3-1
  - Bump version

* Thu Feb 04 2021 - harbottle@room3d3.com - 3.5.2-1
  - Bump version

* Thu Jan 28 2021 - harbottle@room3d3.com - 3.5.1-1
  - Bump version

* Wed Jan 13 2021 - harbottle@room3d3.com - 3.5.0-1
  - Bump version

* Wed Dec 09 2020 - harbottle@room3d3.com - 3.4.2-1
  - Bump version

* Wed Nov 11 2020 - harbottle@room3d3.com - 3.4.1-1
  - Bump version

* Mon Oct 26 2020 - harbottle@room3d3.com - 3.4.0-1
  - Bump version

* Wed Sep 23 2020 - harbottle@room3d3.com - 3.3.4-1
  - Bump version

* Sat Sep 19 2020 - harbottle@room3d3.com - 3.3.3-1
  - Bump version

* Thu Sep 17 2020 - harbottle@room3d3.com - 3.3.2-1
  - Bump version

* Wed Sep 02 2020 - harbottle@room3d3.com - 3.3.1-1
  - Bump version

* Tue Aug 11 2020 - harbottle@room3d3.com - 3.3.0-1
  - Bump version

* Mon Jun 15 2020 - harbottle@room3d3.com - 3.2.4-1
  - Bump version

* Mon Jun 08 2020 - harbottle@room3d3.com - 3.2.3-1
  - Bump version

* Thu Jun 04 2020 - harbottle@room3d3.com - 3.2.2-1
  - Bump version

* Thu May 07 2020 - harbottle@room3d3.com - 3.2.1-1
  - Bump version

* Wed Apr 22 2020 - harbottle@room3d3.com - 3.2.0-1
  - Bump version

* Thu Mar 12 2020 - harbottle@room3d3.com - 3.1.2-1
  - Bump version

* Sat Feb 29 2020 - harbottle@room3d3.com - 3.1.1-1
  - Bump version

* Thu Feb 13 2020 - harbottle@room3d3.com - 3.1.0-1
  - Bump version

* Wed Jan 29 2020 - harbottle@room3d3.com - 3.0.3-1
  - Bump version

* Thu Dec 19 2019 - harbottle@room3d3.com - 3.0.2-1
  - Bump version

* Mon Dec 09 2019 - harbottle@room3d3.com - 3.0.1-2
  - Build from source again

* Fri Dec 06 2019 - harbottle@room3d3.com - 3.0.1-1
  - Download binary, as build is now broken
  - Bump version

* Wed Nov 13 2019 - harbottle@room3d3.com - 3.0.0-1
  - Bump version

* Tue Nov 12 2019 - harbottle@room3d3.com - 2.16.1-1
  - Bump version

* Wed Nov 06 2019 - harbottle@room3d3.com - 2.16.0-1
  - Bump version

* Tue Oct 29 2019 - harbottle@room3d3.com - 2.15.2-1
  - Bump version

* Wed Oct 23 2019 - harbottle@room3d3.com - 2.15.1-1
  - Bump version

* Fri Oct 18 2019 - harbottle@room3d3.com - 2.15.0-1
  - Bump version

* Tue Jul 30 2019 - harbottle@room3d3.com - 2.14.3-1
  - Bump version

* Thu Jul 11 2019 - harbottle@room3d3.com - 2.14.2-1
  - Bump version

* Wed Jun 05 2019 - harbottle@room3d3.com - 2.14.1-1
  - Bump version

* Mon Jun 03 2019 - harbottle@room3d3.com - 2.14.0-1
  - Bump version

* Thu Mar 21 2019 - harbottle@room3d3.com - 2.13.1-1
  - Bump version

* Wed Feb 27 2019 - harbottle@room3d3.com - 2.13.0-1
  - Bump version

* Sun Jan 27 2019 - harbottle@room3d3.com - 2.12.3-1
  - Bump version

* Fri Jan 18 2019 - harbottle@room3d3.com - 2.12.2-1
  - Bump version

* Mon Jan 07 2019 - harbottle@room3d3.com - 2.12.1-1
  - Initial package

