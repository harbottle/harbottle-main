%define __jar_repack 0

Name:     sonarqube-typescript
Version:  2.1.0.4359
Release:  2%{?dist}.harbottle
Summary:  SonarQube TypeScript plugin
Group:    Applications/System
License:  LGPL-3.0
Source0:  https://binaries.sonarsource.com/Distribution/sonar-typescript-plugin/sonar-typescript-plugin-%{version}.jar
Autoprov: no

%description
TypeScript plugin for SonarQube.

%prep

%install
install -d -m 755 $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins
mv %{SOURCE0} $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins

%pre
rm -f %{_var}/lib/sonarqube/extensions/plugins/sonar-typescript-plugin-*.jar
getent group sonarqube >/dev/null || groupadd -f -r sonarqube
getent passwd sonarqube >/dev/null || useradd -r -g sonarqube -d /usr/share/sonarqube -s /sbin/nologin -c "SonarQube user" sonarqube
exit 0

%files
%attr(0644,sonarqube,sonarqube) %{_var}/lib/sonarqube/extensions/plugins/sonar-typescript-plugin-%{version}.jar

%changelog
* Tue Dec 10 2019 - harbottle@room3d3.com - 2.1.0.4359-2
  - Spec file changes for el8

* Thu Nov 14 2019 - harbottle@room3d3.com - 2.1.0.4359-1
  - Bump version

* Wed Oct 02 2019 - harbottle@room3d3.com - 2.0.0.4283-1
  - Bump version

* Sun Jul 21 2019 - harbottle@room3d3.com - 1.9.0.3766-2
  - Fix plugin ownership

* Sun Jul 21 2019 - harbottle@room3d3.com - 1.9.0.3766-1
  - Initial package
