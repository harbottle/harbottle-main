Name:    golangci-lint
Version: 1.46.2
Release: 1%{?dist}.harbottle
Summary: Linters Runner for Go
Group:   Applications/System
License: Apache-2.0
Url:     https://github.com/golangci/%{name}
Source0: %{url}/releases/download/v%{version}/%{name}-%{version}-linux-amd64.tar.gz

%description
GolangCI-Lint is a linters aggregator. It's fast: on average 5 times faster than
gometalinter. It's easy to integrate and use, has nice output and has a minimum
number of false positives. It supports go modules.

GolangCI-Lint has integrations with VS Code, GNU Emacs, Sublime Text.

%prep
%setup -qn %{name}-%{version}-linux-amd64

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 755 %{name} $RPM_BUILD_ROOT%{_bindir}

%files
%license LICENSE
%doc *.md
%{_bindir}/%{name}

%changelog
* Tue May 17 2022 - harbottle@room3d3.com - 1.46.2-1
  - Bump version

* Thu May 12 2022 - harbottle@room3d3.com - 1.46.1-1
  - Bump version

* Mon May 09 2022 - harbottle@room3d3.com - 1.46.0-1
  - Bump version

* Thu Mar 24 2022 - harbottle@room3d3.com - 1.45.2-1
  - Bump version

* Thu Mar 24 2022 - harbottle@room3d3.com - 1.45.1-1
  - Bump version

* Fri Mar 18 2022 - harbottle@room3d3.com - 1.45.0-1
  - Bump version

* Thu Feb 17 2022 - harbottle@room3d3.com - 1.44.2-1
  - Bump version

* Thu Feb 17 2022 - harbottle@room3d3.com - 1.44.1-1
  - Bump version

* Tue Jan 25 2022 - harbottle@room3d3.com - 1.44.0-1
  - Bump version

* Wed Nov 03 2021 - harbottle@room3d3.com - 1.43.0-1
  - Bump version

* Mon Sep 06 2021 - harbottle@room3d3.com - 1.42.1-1
  - Bump version

* Tue Aug 17 2021 - harbottle@room3d3.com - 1.42.0-1
  - Bump version

* Sat Jun 19 2021 - harbottle@room3d3.com - 1.41.1-1
  - Bump version

* Wed Jun 16 2021 - harbottle@room3d3.com - 1.41.0-1
  - Bump version

* Fri May 14 2021 - harbottle@room3d3.com - 1.40.1-1
  - Bump version

* Mon May 10 2021 - harbottle@room3d3.com - 1.40.0-1
  - Bump version

* Fri Mar 26 2021 - harbottle@room3d3.com - 1.39.0-1
  - Bump version

* Wed Mar 03 2021 - harbottle@room3d3.com - 1.38.0-1
  - Bump version

* Sat Feb 20 2021 - harbottle@room3d3.com - 1.37.1-1
  - Bump version

* Wed Feb 17 2021 - harbottle@room3d3.com - 1.37.0-1
  - Bump version

* Tue Jan 26 2021 - harbottle@room3d3.com - 1.36.0-1
  - Bump version

* Mon Jan 11 2021 - harbottle@room3d3.com - 1.35.2-1
  - Bump version

* Mon Jan 11 2021 - harbottle@room3d3.com - 1.35.1-1
  - Bump version

* Fri Jan 08 2021 - harbottle@room3d3.com - 1.35.0-1
  - Bump version

* Tue Dec 29 2020 - harbottle@room3d3.com - 1.34.1-1
  - Bump version

* Mon Dec 28 2020 - harbottle@room3d3.com - 1.34.0-1
  - Bump version

* Mon Dec 28 2020 - harbottle@room3d3.com - 1.33.1-1
  - Bump version

* Mon Nov 23 2020 - harbottle@room3d3.com - 1.33.0-1
  - Bump version

* Tue Nov 03 2020 - harbottle@room3d3.com - 1.32.2-1
  - Bump version

* Sat Oct 31 2020 - harbottle@room3d3.com - 1.32.1-1
  - Bump version

* Sun Oct 25 2020 - harbottle@room3d3.com - 1.32.0-1
  - Bump version

* Mon Sep 07 2020 - harbottle@room3d3.com - 1.31.0-1
  - Bump version

* Mon Aug 03 2020 - harbottle@room3d3.com - 1.30.0-1
  - Bump version

* Fri Jul 24 2020 - harbottle@room3d3.com - 1.29.0-1
  - Bump version

* Thu Jul 09 2020 - harbottle@room3d3.com - 1.28.2-1
  - Bump version

* Mon Jul 06 2020 - harbottle@room3d3.com - 1.28.1-1
  - Bump version

* Sat Jul 04 2020 - harbottle@room3d3.com - 1.28.0-1
  - Bump version

* Wed May 13 2020 - harbottle@room3d3.com - 1.27.0-1
  - Bump version

* Fri May 01 2020 - harbottle@room3d3.com - 1.26.0-1
  - Bump version

* Mon Apr 27 2020 - harbottle@room3d3.com - 1.25.1-1
  - Bump version

* Wed Apr 22 2020 - harbottle@room3d3.com - 1.25.0-1
  - Bump version

* Tue Mar 17 2020 - harbottle@room3d3.com - 1.24.0-1
  - Initial package
