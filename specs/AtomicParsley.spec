Name:          AtomicParsley
Version:       20210715.151551.e7ad03a
Release:       1%{?dist}.harbottle
Summary:       Program to read and set MPEG-4 tags compatible with iTunes
Group:         Applications/System
License:       GPL-2.0
URL:           https://github.com/wez/atomicparsley
Source0:       https://github.com/wez/atomicparsley/archive/%{version}.tar.gz
BuildRequires: cmake gcc gcc-c++ make perl-generators zlib-devel

%description
AtomicParsley is a lightweight command line program for reading, parsing and
setting metadata into MPEG-4 files, in particular, iTunes-style metadata.

%prep
%setup -qn atomicparsley-%{version}

%build
%define debug_package %{nil}
cmake .
cmake --build . --config Release

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 0755 %{name} $RPM_BUILD_ROOT%{_bindir}

%files
%doc Changes.txt COPYING CREDITS README.md tools/iTunMOVI-1.1.pl
%{_bindir}/AtomicParsley


%changelog
* Fri Jul 16 2021 - harbottle@room3d3.com - 20210715.151551.e7ad03a-1
  - Fix build
  - Bump version

* Fri Jun 18 2021 - harbottle@room3d3.com - 20210617.200601.1ac7c08-1
  - Bump version

* Tue Feb 09 2021 - harbottle@room3d3.com - 20210124.204813.840499f-1
  - Bump version

* Mon Feb 08 2021 - harbottle@room3d3.com - 20210114.184825.1dbe1be-1
  - Bump version

* Sun Sep 13 2020 - harbottle@room3d3.com - 20200701.154658.b0d6223-1
  - Initial harbottle release
