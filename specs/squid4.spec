%define __perl_requires %{SOURCE98}
%global pidfile /run/%{name}.pid

Name:     squid4
Version:  4.17
Release:  1%{?dist}.harbottle
Summary:  The Squid proxy caching server
# See CREDITS for breakdown of non GPLv2+ code
License:  GPLv2+ and (LGPLv2+ and MIT and BSD and Public Domain)
Group:    Applications/System
URL:      http://www.squid-cache.org
Source0:  http://www.squid-cache.org/Versions/v4/squid-%{version}.tar.xz
Source1:  http://www.squid-cache.org/Versions/v4/squid-%{version}.tar.xz.asc
Source2:  %{name}.logrotate
Source3:  %{name}.sysconfig
Source4:  %{name}.pam
Source5:  %{name}.nm
Source6:  %{name}.service
Source7:  cache_swap.sh

Source98: perl-requires-squid.sh

Patch201: squid-4.0.11-config.patch
Patch202: squid-3.1.0.9-location.patch
Patch203: squid-3.0.STABLE1-perlpath.patch
Patch204: squid-3.5.9-include-guards.patch
Patch205: squid-4.0.21-large-acl.patch

Requires: bash >= 2.0
Requires(pre): shadow-utils
Requires(post): /sbin/chkconfig
Requires(preun): /sbin/chkconfig
Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd

# squid_ldap_auth and other LDAP helpers require OpenLDAP
BuildRequires: openldap-devel
# squid_pam_auth requires PAM development libs
BuildRequires: pam-devel
# SSL support requires OpenSSL
BuildRequires: openssl-devel
# squid_kerb_aut requires Kerberos development libs
BuildRequires: krb5-devel
# time_quota requires DB
BuildRequires: libdb-devel
# ESI support requires Expat & libxml2
BuildRequires: expat-devel libxml2-devel
# TPROXY requires libcap, and also increases security somewhat
BuildRequires: libcap-devel
# eCAP support
BuildRequires: libecap-devel
#ip_user helper requires
BuildRequires: gcc-c++
BuildRequires: libtool libtool-ltdl-devel
BuildRequires: perl-generators
# For test suite
BuildRequires: pkgconfig(cppunit)
BuildRequires: autoconf

%description
Squid is a high-performance proxy caching server for Web clients,
supporting FTP, gopher, and HTTP data objects. Unlike traditional
caching software, Squid handles all requests in a single,
non-blocking, I/O-driven process. Squid keeps meta data and especially
hot objects cached in RAM, caches DNS lookups, supports non-blocking
DNS lookups, and implements negative caching of failed requests.

Squid consists of a main server program squid, a Domain Name System
lookup program (dnsserver), a program for retrieving FTP data
(ftpget), and some management and client tools.

%prep
%setup -q -n squid-%{version}

%patch201 -p1 -b .config
%patch202 -p1 -b .location
%patch203 -p1 -b .perlpath
%patch204 -p0 -b .include-guards
%patch205 -p1 -b .large_acl

%build
# cppunit-config patch changes configure.ac
autoconf

# libtool fails somewhat on -fpie. PIC also works for -pie
CXXFLAGS="$RPM_OPT_FLAGS -fPIC"
CFLAGS="$RPM_OPT_FLAGS -fPIC"
LDFLAGS="$RPM_LD_FLAGS -pie -Wl,-z,relro -Wl,-z,now -Wl,--warn-shared-textrel"

# NIS helper has been removed because of the following bug
# https://bugzilla.redhat.com/show_bug.cgi?id=1531540
%configure \
   --exec_prefix=%{_prefix} \
   --libexecdir=%{_libdir}/%{name} \
   --localstatedir=%{_localstatedir} \
   --datadir=%{_datadir}/%{name} \
   --sysconfdir=%{_sysconfdir}/%{name} \
   --with-logdir='%{_localstatedir}/log/%{name}' \
   --with-pidfile='%{pidfile}' \
   --disable-dependency-tracking \
   --enable-eui \
   --enable-follow-x-forwarded-for \
   --enable-auth \
   --enable-auth-basic="DB,fake,getpwnam,LDAP,NCSA,PAM,POP3,RADIUS,SASL,SMB,SMB_LM" \
   --enable-auth-ntlm="SMB_LM,fake" \
   --enable-auth-digest="file,LDAP" \
   --enable-auth-negotiate="kerberos" \
   --enable-external-acl-helpers="LDAP_group,time_quota,session,unix_group,wbinfo_group,kerberos_ldap_group" \
   --enable-storeid-rewrite-helpers="file" \
   --enable-cache-digests \
   --enable-cachemgr-hostname=localhost \
   --enable-delay-pools \
   --enable-epoll \
   --enable-icap-client \
   --enable-ident-lookups \
   %ifnarch %{power64} ia64 x86_64 s390x aarch64
   --with-large-files \
   %endif
   --enable-linux-netfilter \
   --enable-removal-policies="heap,lru" \
   --enable-snmp \
   --enable-ssl \
   --enable-ssl-crtd \
   --enable-storeio="aufs,diskd,ufs,rock" \
   --enable-diskio \
   --enable-wccpv2 \
   --enable-esi \
   --enable-ecap \
   --with-aio \
   --with-default-user="%{name}" \
   --with-dl \
   --with-openssl \
   --with-pthreads \
   --disable-arch-native \
   --with-pic \
   --disable-security-cert-validators

make DEFAULT_SWAP_DIR=%{_localstatedir}/spool/%{name} %{?_smp_mflags}

%install
make DESTDIR=$RPM_BUILD_ROOT install
echo "
#
# This is /etc/httpd/conf.d/squid4.conf
#

ScriptAlias /Squid/cgi-bin/cachemgr.cgi %{_libdir}/%{name}/cachemgr.cgi

# Only allow access from localhost by default
<Location /Squid/cgi-bin/cachemgr.cgi>
 Require local
 # Add additional allowed hosts as needed
 # Require host example.com
</Location>" > $RPM_BUILD_ROOT/%{name}.httpd.tmp

mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/rc.d/init.d
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/logrotate.d
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/pam.d
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/httpd/conf.d/
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/NetworkManager/dispatcher.d
mkdir -p $RPM_BUILD_ROOT%{_unitdir}
mkdir -p $RPM_BUILD_ROOT%{_libexecdir}/%{name}
install -m 644 %{SOURCE2} $RPM_BUILD_ROOT%{_sysconfdir}/logrotate.d/%{name}
install -m 644 %{SOURCE3} $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/%{name}
install -m 644 %{SOURCE4} $RPM_BUILD_ROOT%{_sysconfdir}/pam.d/%{name}
install -m 644 %{SOURCE6} $RPM_BUILD_ROOT%{_unitdir}
install -m 755 %{SOURCE7} $RPM_BUILD_ROOT%{_libexecdir}/%{name}
install -m 644 $RPM_BUILD_ROOT/%{name}.httpd.tmp $RPM_BUILD_ROOT%{_sysconfdir}/httpd/conf.d/%{name}.conf
install -m 644 %{SOURCE5} $RPM_BUILD_ROOT%{_sysconfdir}/NetworkManager/dispatcher.d/20-%{name}
mkdir -p $RPM_BUILD_ROOT%{_localstatedir}/log/%{name}
mkdir -p $RPM_BUILD_ROOT%{_localstatedir}/spool/%{name}
mkdir -p $RPM_BUILD_ROOT%{_localstatedir}/run/%{name}
chmod 644 contrib/url-normalizer.pl contrib/user-agents.pl
iconv -f ISO88591 -t UTF8 ChangeLog -o ChangeLog.tmp
mv -f ChangeLog.tmp ChangeLog
touch $RPM_BUILD_ROOT%{pidfile}
chmod 0644 $RPM_BUILD_ROOT%{pidfile}

# install /usr/lib/tmpfiles.d/squid4.conf
mkdir -p ${RPM_BUILD_ROOT}%{_tmpfilesdir}
cat > ${RPM_BUILD_ROOT}%{_tmpfilesdir}/%{name}.conf <<EOF
# See tmpfiles.d(5) for details

d /run/%{name} 0755 %{name} %{name} - -
f %{pidfile} 0644 root %{name} - -

EOF

# Move the MIB definition to the proper place (and name)
mkdir -p $RPM_BUILD_ROOT/usr/share/snmp/mibs
mv $RPM_BUILD_ROOT/usr/share/%{name}/mib.txt $RPM_BUILD_ROOT/usr/share/snmp/mibs/SQUID4-MIB.txt

# squid.conf.documented is documentation. We ship that in doc/
rm -f $RPM_BUILD_ROOT%{_sysconfdir}/%{name}/squid.conf.documented

# remove unpackaged files from the buildroot
rm -f $RPM_BUILD_ROOT%{_bindir}/{RunAccel,RunCache}
rm -f $RPM_BUILD_ROOT/%{name}.httpd.tmp

# Remove man files as they will(may?) clash with older package
rm -rf $RPM_BUILD_ROOT%{_mandir}

# Rename executables
mv $RPM_BUILD_ROOT%{_sbindir}/squid $RPM_BUILD_ROOT%{_sbindir}/%{name}
mv $RPM_BUILD_ROOT%{_bindir}/squidclient $RPM_BUILD_ROOT%{_bindir}/squidclient4
mv $RPM_BUILD_ROOT%{_bindir}/purge $RPM_BUILD_ROOT%{_bindir}/purge4

%files
%license COPYING
%doc CONTRIBUTORS README ChangeLog QUICKSTART src/squid.conf.documented
%doc contrib/url-normalizer.pl contrib/user-agents.pl

%{_unitdir}/%{name}.service
%attr(755,root,root) %dir %{_libexecdir}/%{name}
%attr(755,root,root) %{_libexecdir}/%{name}/cache_swap.sh
%attr(755,root,root) %dir %{_sysconfdir}/%{name}
%attr(755,root,root) %dir %{_libdir}/%{name}
%attr(770,%{name},root) %dir %{_localstatedir}/log/%{name}
%attr(750,%{name},%{name}) %dir %{_localstatedir}/spool/%{name}
%attr(755,%{name},%{name}) %dir %{_localstatedir}/run/%{name}

%config(noreplace) %attr(644,root,root) %{_sysconfdir}/httpd/conf.d/%{name}.conf
%config(noreplace) %attr(640,root,%{name}) %{_sysconfdir}/%{name}/squid.conf
%config(noreplace) %attr(644,root,%{name}) %{_sysconfdir}/%{name}/cachemgr.conf
%config(noreplace) %{_sysconfdir}/%{name}/mime.conf
%config(noreplace) %{_sysconfdir}/%{name}/errorpage.css
%config(noreplace) %{_sysconfdir}/sysconfig/%{name}
# These are not noreplace because they are just sample config files
%config %{_sysconfdir}/%{name}/squid.conf.default
%config %{_sysconfdir}/%{name}/mime.conf.default
%config %{_sysconfdir}/%{name}/errorpage.css.default
%config %{_sysconfdir}/%{name}/cachemgr.conf.default
%config(noreplace) %{_sysconfdir}/pam.d/%{name}
%config(noreplace) %{_sysconfdir}/logrotate.d/%{name}

%dir %{_datadir}/%{name}
%attr(-,root,root) %{_datadir}/%{name}/errors
%attr(755,root,root) %{_sysconfdir}/NetworkManager/dispatcher.d/20-%{name}
%{_datadir}/%{name}/icons
%{_sbindir}/%{name}
%{_bindir}/squidclient4
%{_bindir}/purge4
%{_libdir}/%{name}/*
%{_datadir}/snmp/mibs/SQUID4-MIB.txt
%{_tmpfilesdir}/%{name}.conf
%attr(0644,root,%{name}) %verify(not size mtime md5) %{pidfile}

%pre
getent group %{name} >/dev/null || groupadd -f -r %{name}
getent passwd %{name} >/dev/null || useradd -r -g %{name} -d /var/spool/%{name} -s /sbin/nologin -c "%{name} user" %{name}

for i in /var/log/%{name} /var/spool/%{name} ; do
  if [ -d $i ] ; then
    for adir in `find $i -maxdepth 0 \! -user %{name}`; do
      chown -R %{name}:%{name} $adir
    done
  fi
done

exit 0

%post
%systemd_post %{name}.service

%preun
%systemd_preun %{name}.service

%postun
%systemd_postun_with_restart %{name}.service

%triggerin -- samba-common
if ! getent group wbpriv >/dev/null 2>&1 ; then
  /usr/sbin/groupadd -g 88 wbpriv >/dev/null 2>&1 || :
fi
/usr/sbin/usermod -a -G wbpriv %{name} >/dev/null 2>&1 || \
    chgrp %{name} /var/cache/samba/winbindd_privileged >/dev/null 2>&1 || :

%changelog
* Sun Oct 03 2021 - harbottle@room3d3.com - 4.17-1
  - Bump version

* Mon Jul 05 2021 - harbottle@room3d3.com - 4.16-1
  - Bump version

* Mon May 10 2021 - harbottle@room3d3.com - 4.15-1
  - Bump version

* Mon Feb 08 2021 - harbottle@room3d3.com - 4.14-1
- Fix build
- Bump version

* Sat Aug 22 2020 - harbottle@room3d3.com - 4.13-1
  - Bump version

* Tue Jun 09 2020 - harbottle@room3d3.com - 4.12-1
  - Bump version

* Sun Apr 19 2020 - harbottle@room3d3.com - 4.11-1
  - Bump version

* Wed Jan 22 2020 - harbottle@room3d3.com - 4.10-1
  - Bump version
  - Fix build

* Wed Nov 06 2019 - harbottle@room3d3.com - 4.9-1
  - Bump version

* Wed Jul 10 2019 - harbottle@room3d3.com - 4.8-1
  - Bump version

* Mon Jun 03 2019 - harbottle@room3d3.com - 4.7-1
  - Bump version

* Tue Feb 19 2019 - harbottle@room3d3.com - 4.6-1
  - Bump version

* Tue Feb 19 2019 - harbottle@room3d3.com - 4.5-3
  - Fix changelog
  - Remove macros from comments

* Wed Feb 13 2019 - harbottle@room3d3.com - 4.5-2
  - Fix pidfile
  - Fix instance name

* Tue Feb 12 2019 - harbottle@room3d3.com - 4.5-1
  - Bump version

* Tue Feb 12 2019 - harbottle@room3d3.com - 4.2-1
  - Initial packaging (adapted from Fedora squid spec file)
