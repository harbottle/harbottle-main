%define __jar_repack 0

Name:     sonarqube-perl
Version:  0.5.6
Release:  1%{?dist}.harbottle
Summary:  SonarQube Perl plugin
Group:    Applications/System
License:  Apache-2.0
URL:      https://github.com/sonar-perl/sonar-perl
Source0:  %{url}/releases/download/%{version}/sonar-perl-plugin-%{version}-all.jar
Autoprov: no

%description
Perl plugin for SonarQube.

%install
install -d -m 755 $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins
mv %{SOURCE0} $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins/sonar-perl-plugin-%{version}.jar

%pre
rm -f %{_var}/lib/sonarqube/extensions/plugins/sonar-perl-plugin-*.jar
getent group sonarqube >/dev/null || groupadd -f -r sonarqube
getent passwd sonarqube >/dev/null || useradd -r -g sonarqube -d /usr/share/sonarqube -s /sbin/nologin -c "SonarQube user" sonarqube
exit 0

%files
%attr(0644,sonarqube,sonarqube) %{_var}/lib/sonarqube/extensions/plugins/sonar-perl-plugin-%{version}.jar

%changelog
* Fri Apr 15 2022 - harbottle@room3d3.com - 0.5.6-1
  - Bump version

* Wed Apr 13 2022 - harbottle@room3d3.com - 0.5.5-1
  - Bump version

* Mon Apr 11 2022 - harbottle@room3d3.com - 0.5.4-1
  - Bump version

* Thu May 27 2021 - harbottle@room3d3.com - 0.5.3-1
  - Bump version

* Sun Jan 17 2021 - harbottle@room3d3.com - 0.5.2-1
  - Bump version

* Tue Dec 10 2019 - harbottle@room3d3.com - 0.4.6-5
  - Spec file changes for el8

* Fri Dec 06 2019 - harbottle@room3d3.com - 0.4.6-4
  - Standardize 3rd party SonarQube plugins

* Sun Jul 21 2019 - harbottle@room3d3.com - 0.4.6-3
  - Fix plugin ownership

* Sun Jul 21 2019 - harbottle@room3d3.com - 0.4.6-2
  - Standardize SonarQube plugins

* Wed Jun 12 2019 - harbottle@room3d3.com - 0.4.6-1
  - Bump version

* Mon Mar 18 2019 - harbottle@room3d3.com - 0.4.5-1
  - Bump version

* Mon Jan 14 2019 - harbottle@room3d3.com - 0.4.4-1
  - Bump version

* Sat Mar 03 2018 grainger@gmail.com - 0.3.2-2
  - Update iteration

* Mon Nov 20 2017 grainger@gmail.com - 0.3.2-1
  - Initial packaging
