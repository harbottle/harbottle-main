%define __jar_repack 0

Name:     sonarqube-xml
Version:  2.5.0.3376
Release:  1%{?dist}.harbottle
Summary:  SonarQube XML plugin
Group:    Applications/System
License:  LGPL-3.0
Source0:  https://binaries.sonarsource.com/Distribution/sonar-xml-plugin/sonar-xml-plugin-%{version}.jar
Autoprov: no

%description
XML plugin for SonarQube.

%prep

%install
install -d -m 755 $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins
mv %{SOURCE0} $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins

%pre
rm -f %{_var}/lib/sonarqube/extensions/plugins/sonar-xml-plugin-*.jar
getent group sonarqube >/dev/null || groupadd -f -r sonarqube
getent passwd sonarqube >/dev/null || useradd -r -g sonarqube -d /usr/share/sonarqube -s /sbin/nologin -c "SonarQube user" sonarqube
exit 0

%files
%attr(0644,sonarqube,sonarqube) %{_var}/lib/sonarqube/extensions/plugins/sonar-xml-plugin-%{version}.jar

%changelog
* Sun Dec 12 2021 - harbottle@room3d3.com - 2.5.0.3376-1
  - Bump version

* Mon Oct 18 2021 - harbottle@room3d3.com - 2.4.0.3273-1
  - Bump version

* Mon Aug 09 2021 - harbottle@room3d3.com - 2.3.0.3155-1
  - Bump version

* Wed Apr 28 2021 - harbottle@room3d3.com - 2.2.0.2973-1
  - Bump version

* Tue Feb 16 2021 - harbottle@room3d3.com - 2.1.0.2861-1
  - Bump version

* Tue Dec 10 2019 - harbottle@room3d3.com - 2.0.1.2020-3
  - Spec file changes for el8

* Sun Jul 21 2019 - harbottle@room3d3.com - 2.0.1.2020-2
  - Fix plugin ownership

* Sun Jul 21 2019 - harbottle@room3d3.com - 2.0.1.2020-1
  - Initial package
