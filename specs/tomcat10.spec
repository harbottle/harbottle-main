%global homedir /usr/share/%{name}

Name:             tomcat10
Version:          10.0.22
Release:          1%{?dist}.harbottle
Summary:          Apache Servlet/JSP Engine, RI for Servlet 3.1/JSP 2.3 API
Group:            Applications/System
License:          Apache-2.0
URL:              http://tomcat.apache.org/
Source0:          https://downloads.apache.org/tomcat/tomcat-10/v%{version}/bin/apache-tomcat-%{version}.tar.gz
Source1:          tomcat10.conf
Source2:          tomcat10.service
Source3:          tomcat10.logrotate
BuildRequires:    systemd-units
Provides:         tomcat10
Requires:         java >= 1:1.8.0
Requires(pre):    shadow-utils
Requires(post):   systemd-units
Requires(preun):  systemd-units
Requires(postun): systemd-units

%description
Tomcat is the servlet container that is used in the official Reference
Implementation for the Java Servlet and JavaServer Pages technologies.
The Java Servlet and JavaServer Pages specifications are developed by
Sun under the Java Community Process.

Tomcat is developed in an open and participatory environment and
released under the Apache Software License version 2.0. Tomcat is intended
to be a collaboration of the best-of-breed developers from around the world.

%package admin-webapps
Group: Applications/System
Summary: The host-manager and manager web applications for Apache Tomcat 10
Requires: %{name} = %{version}-%{release}

%description admin-webapps
The host-manager and manager web applications for Apache Tomcat 10.

%package docs-webapp
Group: Applications/Text
Summary: The docs web application for Apache Tomcat 10
Requires: %{name} = %{version}-%{release}

%description docs-webapp
The docs web application for Apache Tomcat 10.

%package webapps
Group:    Applications/Internet
Summary:  The ROOT and examples web applications for Apache Tomcat 10
Requires: %{name} = %{version}-%{release}

%description webapps
The ROOT and examples web applications for Apache Tomcat 10.

%prep
%setup -qn apache-tomcat-%{version}

%install
rm -f bin/*.bat
sed -i -e '/^2localhost/d' -e '/\[\/localhost\]/d' \
    -e '/^3manager/d' -e '/\[\/manager\]/d' \
    -e '/^4host-manager/d' -e '/\[\/host-manager\]/d' \
    -e 's/, *4host-manager.org.apache.juli.AsyncFileHandler//' \
    -e 's/, *3manager.org.apache.juli.AsyncFileHandler//' \
    conf/logging.properties
install -d -m 755 $RPM_BUILD_ROOT%{homedir}
install -d -m 755 $RPM_BUILD_ROOT%{_sysconfdir}
install -d -m 755 $RPM_BUILD_ROOT%{_unitdir}
install -d -m 755 $RPM_BUILD_ROOT%{_sysconfdir}/logrotate.d
install -d -m 755 $RPM_BUILD_ROOT%{_var}/log
install -d -m 755 $RPM_BUILD_ROOT%{_var}/cache
install -d -m 755 $RPM_BUILD_ROOT%{_var}/cache/%{name}
install -d -m 755 $RPM_BUILD_ROOT%{_var}/lib
install -d -m 755 $RPM_BUILD_ROOT%{_var}/lib/%{name}
install -d -m 755 $RPM_BUILD_ROOT%{_libexecdir}

mv bin $RPM_BUILD_ROOT%{_libexecdir}/%{name}
mv conf $RPM_BUILD_ROOT%{_sysconfdir}/%{name}
mv lib $RPM_BUILD_ROOT%{homedir}/lib
mv logs $RPM_BUILD_ROOT%{_var}/log/%{name}
mv temp $RPM_BUILD_ROOT%{_var}/cache/%{name}/temp
mv work $RPM_BUILD_ROOT%{_var}/cache/%{name}/work
mv webapps $RPM_BUILD_ROOT%{_var}/lib/%{name}/webapps
mv * $RPM_BUILD_ROOT%{homedir}/

install -m 644 %{SOURCE1} $RPM_BUILD_ROOT%{_sysconfdir}/%{name}/%{name}.conf
install -m 644 %{SOURCE2} $RPM_BUILD_ROOT%{_unitdir}/%{name}.service
install -m 644 %{SOURCE3} $RPM_BUILD_ROOT%{_sysconfdir}/logrotate.d/%{name}

ln -s %{_libexecdir}/%{name} $RPM_BUILD_ROOT%{homedir}/bin
ln -s %{_sysconfdir}/%{name} $RPM_BUILD_ROOT%{homedir}/conf
ln -s %{_var}/log/%{name} $RPM_BUILD_ROOT%{homedir}/logs
ln -s %{_var}/cache/%{name}/temp $RPM_BUILD_ROOT%{homedir}/temp
ln -s %{_var}/lib/%{name}/webapps $RPM_BUILD_ROOT%{homedir}/webapps
ln -s %{_var}/cache/%{name}/work $RPM_BUILD_ROOT%{homedir}/work

%pre
getent group %{name} >/dev/null || groupadd -f -r %{name}
getent passwd %{name} >/dev/null || useradd -r -g %{name} -d %{homedir} -s /sbin/nologin -c "Apache Tomcat 10 user" %{name}
exit 0

%post
%systemd_post %{name}.service

%preun
%systemd_preun %{name}.service

%postun
%systemd_postun_with_restart %{name}.service

%files
%attr(-,root,tomcat10) %dir %{homedir}
%attr(-,root,tomcat10) %{_libexecdir}/%{name}
%attr(0770,root,tomcat10) %dir %{_sysconfdir}/%{name}
%config(noreplace) %attr(0740,root,tomcat10) %{_sysconfdir}/%{name}/*
%attr(-,root,tomcat10) %{homedir}/lib
%attr(-,tomcat10,tomcat10) %{_var}/log/%{name}
%attr(-,tomcat10,tomcat10) %{_var}/cache/%{name}/temp
%attr(-,tomcat10,tomcat10) %dir %{_var}/lib/%{name}/webapps
%attr(-,tomcat10,tomcat10) %{_var}/cache/%{name}/work
%{_unitdir}/%{name}.service
%{_sysconfdir}/logrotate.d/%{name}
%{homedir}/bin
%{homedir}/conf
%{homedir}/logs
%{homedir}/temp
%{homedir}/webapps
%{homedir}/work
%doc %{homedir}/LICENSE
%doc %{homedir}/NOTICE
%doc %{homedir}/RELEASE-NOTES
%doc %{homedir}/RUNNING.txt
%doc %{homedir}/BUILDING.txt
%doc %{homedir}/CONTRIBUTING.md
%doc %{homedir}/README.md

%files admin-webapps
%defattr(0664,root,tomcat10,0755)
%{_var}/lib/%{name}/webapps/host-manager
%{_var}/lib/%{name}/webapps/manager
%config(noreplace) %{_var}/lib/%{name}/webapps/host-manager/WEB-INF/web.xml
%config(noreplace) %{_var}/lib/%{name}/webapps/manager/WEB-INF/web.xml

%files docs-webapp
%defattr(-,root,root,-)
%{_var}/lib/%{name}/webapps/docs

%files webapps
%defattr(0644,tomcat10,tomcat10,0755)
%{_var}/lib/%{name}/webapps/ROOT
%{_var}/lib/%{name}/webapps/examples

%changelog
* Sat Jun 11 2022 - harbottle@room3d3.com - 10.0.22-1
  - Bump version

* Mon May 16 2022 - harbottle@room3d3.com - 10.0.21-1
  - Bump version

* Fri Apr 01 2022 - harbottle@room3d3.com - 10.0.20-1
  - Bump version

* Mon Mar 14 2022 - harbottle@room3d3.com - 10.0.18-1
  - Bump version

* Mon Feb 28 2022 - harbottle@room3d3.com - 10.0.17-1
  - Bump version

* Thu Jan 20 2022 - harbottle@room3d3.com - 10.0.16-1
  - Bump version

* Sun Dec 12 2021 - harbottle@room3d3.com - 10.0.14-1
  - Bump version

* Fri Oct 01 2021 - harbottle@room3d3.com - 10.0.12-1
  - Bump version

* Fri Sep 10 2021 - harbottle@room3d3.com - 10.0.11-1
  - Bump version

* Fri Aug 06 2021 - harbottle@room3d3.com - 1.0.10-1
  - Update download location
  - Bump version

* Fri Jul 02 2021 - harbottle@room3d3.com - 10.0.8-1
  - Bump version

* Wed Jun 16 2021 - harbottle@room3d3.com - 10.0.7-1
  - Bump version

* Wed May 12 2021 - harbottle@room3d3.com - 10.0.6-1
  - Bump version

* Tue Apr 06 2021 - harbottle@room3d3.com - 10.0.5-1
  - Bump version

* Thu Mar 11 2021 - harbottle@room3d3.com - 10.0.4-1
  - Bump version

* Mon Feb 08 2021 - ciro.deluca@autistici.org - 10.0.2-1
  - Update version

* Sat Jan 30 2021 - ciro.deluca@autistici.org - 10.0.0-1
  - Initial packaging
