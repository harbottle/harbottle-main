%define __jar_repack 0

Name:     sonarqube-ruby
Version:  1.10.0.3710
Release:  1%{?dist}.harbottle
Summary:  SonarQube Ruby plugin
Group:    Applications/System
License:  LGPL-3.0
Source0:  https://binaries.sonarsource.com/Distribution/sonar-ruby-plugin/sonar-ruby-plugin-%{version}.jar
Autoprov: no

%description
Ruby plugin for SonarQube.

%prep

%install
install -d -m 755 $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins
mv %{SOURCE0} $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins

%pre
rm -f %{_var}/lib/sonarqube/extensions/plugins/sonar-ruby-plugin-*.jar
getent group sonarqube >/dev/null || groupadd -f -r sonarqube
getent passwd sonarqube >/dev/null || useradd -r -g sonarqube -d /usr/share/sonarqube -s /sbin/nologin -c "SonarQube user" sonarqube
exit 0

%files
%attr(0644,sonarqube,sonarqube) %{_var}/lib/sonarqube/extensions/plugins/sonar-ruby-plugin-%{version}.jar

%changelog
* Mon Jun 20 2022 - harbottle@room3d3.com - 1.10.0.3710-1
  - Bump version

* Sat Jan 15 2022 - harbottle@room3d3.com - 1.9.0.3429-1
  - Bump version

* Thu Apr 29 2021 - harbottle@room3d3.com - 1.8.3.2219-1
  - Bump version

* Thu Apr 29 2021 - harbottle@room3d3.com - 1.8.3.2215-1
  - Bump version

* Wed Jan 13 2021 - harbottle@room3d3.com - 1.8.2.1946-1
  - Bump version

* Fri Nov 06 2020 - harbottle@room3d3.com - 1.8.1.1804-1
  - Bump version

* Tue Oct 27 2020 - harbottle@room3d3.com - 1.8.0.1775-1
  - Bump version

* Tue Dec 10 2019 - harbottle@room3d3.com - 1.7.0.883-2
  - Spec file changes for el8

* Fri Aug 16 2019 - harbottle@room3d3.com - 1.7.0.883-1
  - Bump version

* Tue Jul 30 2019 - harbottle@room3d3.com - 1.6.0.719-1
  - Bump version

* Sun Jul 21 2019 - harbottle@room3d3.com - 1.5.0.315-2
  - Fix plugin ownership

* Sun Jul 21 2019 - harbottle@room3d3.com - 1.5.0.315-1
  - Initial package
