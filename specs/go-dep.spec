%global git_owner golang
%global git_repo dep
%global git_archive_file v%{version}.tar.gz
%global git_archive_dir %{git_repo}-%{version}
%global go_namespace github.com/%{git_owner}/%{git_repo}

Name:          go-dep
Version:       0.5.4
Release:       1.el7.harbottle
Summary:       Go dependency management tool
Group:         Applications/System
License:       Custom
Url:           https://golang.github.io/dep
Source0:       https://github.com/%{git_owner}/%{git_repo}/archive/%{git_archive_file}
BuildRequires: golang make

%description
dep is a dependency management tool for Go.

%prep
%setup -q -n %{git_archive_dir}

%build
%define debug_package %{nil}
export DATE=$(date "+%Y-%m-%d")
export GOPATH=$PWD
export GOARCH=amd64
export GOOS=linux
export CGO_ENABLED=0
mkdir -p src/%{go_namespace}/
shopt -s extglob dotglob
mv !(src) src/%{go_namespace}/
shopt -u extglob dotglob
pushd src/%{go_namespace}/
go build -a -installsuffix cgo \
  -ldflags "-s -w -X main.version=%{version} -X main.buildDate=${DATE} -X main.flagImportDuringSolve=${IMPORT_DURING_SOLVE:-false}" \
  -o "./dep" ./cmd/dep/
popd

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 755 src/%{go_namespace}/%{git_repo} $RPM_BUILD_ROOT%{_bindir}

%files
%license src/%{go_namespace}/LICENSE
%doc src/%{go_namespace}/{AUTHORS,CHANGELOG.md,CONTRIBUTING.md,CONTRIBUTORS,MAINTAINERS.md,PATENTS,README.md}
%{_bindir}/%{git_repo}

%changelog
* Sun Jul 14 2019 - harbottle@room3d3.com - 0.5.4-1
  - Initial package
