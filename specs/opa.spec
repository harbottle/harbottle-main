Name:             opa
Version:          0.42.1
Release:          1%{?dist}.harbottle
Summary:          Open Policy Agent
Group:            Applications/System
License:          Apache-2.0
Url:              https://github.com/open-policy-agent/%{name}
Source0:          %{url}/archive/v%{version}.tar.gz
Source1:          %{url}/releases/download/v%{version}/%{name}_linux_amd64
Source2:          opa.sysconfig
Source3:          opa.yaml
Source4:          opa.service
BuildRequires:    make which systemd-units zip
%if 0%{?rhel} == 8
BuildRequires:    systemd-rpm-macros
%endif
Requires(pre):    shadow-utils
Requires(post):   systemd-units
Requires(preun):  systemd-units
Requires(postun): systemd-units

%description
The Open Policy Agent (OPA) is an open source, general-purpose policy engine
that enables unified, context-aware policy enforcement across the entire stack.

%prep
%setup -q

%install
install -d -m 0755 $RPM_BUILD_ROOT%{_bindir}
install -d -m 0770 $RPM_BUILD_ROOT%{_sharedstatedir}/%{name}
install -d -m 0750 $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig
install -d -m 0750 $RPM_BUILD_ROOT%{_sysconfdir}/%{name}
install -d -m 0755 $RPM_BUILD_ROOT%{_unitdir}
install -m 0755 %{SOURCE1} $RPM_BUILD_ROOT%{_bindir}/%{name}
install -m 0640 %{SOURCE2} $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/%{name}
install -m 0640 %{SOURCE3} $RPM_BUILD_ROOT%{_sysconfdir}/%{name}.yaml
install -m 0640 %{SOURCE4} $RPM_BUILD_ROOT%{_unitdir}/%{name}.service

%pre
getent group %{name} >/dev/null || groupadd -f -r %{name}
getent passwd %{name} >/dev/null || useradd -r -g %{name} -d %{_sharedstatedir}/%{name} -s /sbin/nologin -c "Open Policy Agent" %{name}
exit 0

%post
%systemd_post %{name}.service

%preun
%systemd_preun %{name}.service

%postun
%systemd_postun_with_restart %{name}.service

%files
%license LICENSE
%doc {*.md,*.pdf}
%{_bindir}/%{name}
%config(noreplace) %attr(-,root,%{name}) %{_sysconfdir}/sysconfig/%{name}
%config(noreplace) %attr(-,root,%{name}) %{_sysconfdir}/%{name}
%config(noreplace) %attr(-,root,%{name}) %{_sysconfdir}/%{name}.yaml
%attr(-,%{name},%{name}) %{_sharedstatedir}/%{name}
%attr(0644,root,root) %{_unitdir}/%{name}.service

%changelog
* Fri Jul 08 2022 - harbottle@room3d3.com - 0.42.1-1
  - Bump version

* Mon Jul 04 2022 - harbottle@room3d3.com - 0.42.0-1
  - Bump version

* Thu Jun 02 2022 - harbottle@room3d3.com - 0.41.0-1
  - Bump version

* Thu Apr 28 2022 - harbottle@room3d3.com - 0.40.0-1
  - Bump version

* Thu Mar 31 2022 - harbottle@room3d3.com - 0.39.0-1
  - Bump version

* Mon Mar 14 2022 - harbottle@room3d3.com - 0.38.1-1
  - Bump version

* Thu Mar 03 2022 - harbottle@room3d3.com - 0.38.0-1
  - Bump version

* Fri Feb 11 2022 - harbottle@room3d3.com - 0.37.2-1
  - Bump version

* Thu Feb 03 2022 - harbottle@room3d3.com - 0.37.1-1
  - Fix build
  - Bump version

* Sat Jan 15 2022 - harbottle@room3d3.com - 0.36.1-1
  - Bump version

* Sun Dec 12 2021 - harbottle@room3d3.com - 0.35.0-1
  - Bump version

* Wed Nov 03 2021 - harbottle@room3d3.com - 0.34.1-1
  - Bump version

* Thu Oct 28 2021 - harbottle@room3d3.com - 0.34.0-1
  - Bump version

* Mon Oct 04 2021 - harbottle@room3d3.com - 0.33.1-1
  - Bump version

* Thu Sep 30 2021 - harbottle@room3d3.com - 0.33.0-1
  - Bump version

* Mon Sep 20 2021 - harbottle@room3d3.com - 0.32.1-1
  - Bump version

* Tue Aug 31 2021 - harbottle@room3d3.com - 0.32.0-1
  - Bump version

* Wed Jul 28 2021 - harbottle@room3d3.com - 0.31.0-1
  - Bump version

* Tue Jul 13 2021 - harbottle@room3d3.com - 0.30.2-1
  - Bump version

* Fri Jul 02 2021 - harbottle@room3d3.com - 0.30.1-1
  - Bump version

* Thu Jul 01 2021 - harbottle@room3d3.com - 0.30.0-1
  - Bump version

* Mon May 31 2021 - harbottle@room3d3.com - 0.29.4-1
  - Bump version

* Fri May 28 2021 - harbottle@room3d3.com - 0.29.3-1
  - Bump version

* Fri May 28 2021 - harbottle@room3d3.com - 0.29.2-1
  - Bump version

* Thu May 27 2021 - harbottle@room3d3.com - 0.29.1-1
  - Bump version

* Tue Apr 27 2021 - harbottle@room3d3.com - 0.28.0-1
  - Bump version

* Fri Mar 12 2021 - harbottle@room3d3.com - 0.27.1-1
  - Bump version

* Thu Mar 11 2021 - harbottle@room3d3.com - 0.27.0-2
  - Move to binary packaging

* Tue Mar 09 2021 - harbottle@room3d3.com - 0.27.0-1
  - Bump version
  - Fix build

* Wed Jan 20 2021 - harbottle@room3d3.com - 0.26.0-1
  - Bump version

* Tue Dec 08 2020 - harbottle@room3d3.com - 0.25.2-1
  - Bump version

* Sat Dec 05 2020 - harbottle@room3d3.com - 0.25.1-1
  - Bump version

* Thu Dec 03 2020 - harbottle@room3d3.com - 0.25.0-1
  - Bump version

* Fri Oct 16 2020 - harbottle@room3d3.com - 0.24.0-1
  - Bump version

* Mon Aug 24 2020 - harbottle@room3d3.com - 0.23.2-1
  - Bump version

* Thu Aug 20 2020 - harbottle@room3d3.com - 0.23.1-1
  - Bump version

* Fri Jul 24 2020 - harbottle@room3d3.com - 0.22.0-1
  - Bump version

* Thu Jul 09 2020 - harbottle@room3d3.com - 0.21.1-1
  - Bump version

* Tue Jun 16 2020 - harbottle@room3d3.com - 0.21.0-1
  - Bump version

* Mon Jun 01 2020 - harbottle@room3d3.com - 0.20.5-1
  - Bump version

* Sat May 23 2020 - harbottle@room3d3.com - 0.20.4-2
  - Add systemd service

* Fri May 22 2020 - harbottle@room3d3.com - 0.20.4-1
  - Bump version

* Fri May 22 2020 - harbottle@room3d3.com - 0.20.3-1
  - Bump version

* Thu May 21 2020 - harbottle@room3d3.com - 0.20.2-1
  - Bump version

* Thu May 21 2020 - harbottle@room3d3.com - 0.20.1-1
  - Bump version

* Thu May 21 2020 - harbottle@room3d3.com - 0.20.0-1
  - Bump version

* Tue Apr 28 2020 - harbottle@room3d3.com - 0.19.2-1
  - Bump version

* Wed Apr 15 2020 - harbottle@room3d3.com - 0.19.1-1
  - Initial package
