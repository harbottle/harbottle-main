%global __jar_repack 0

Name:           grails
Version:        5.2.0
Release:        1%{?dist}.harbottle
Summary:        A powerful Groovy-based web application framework for the JVM

License:        Apache 2.0
Source0:        https://github.com/grails/grails-core/archive/v%{version}.tar.gz

BuildRequires:  java-devel which
Requires:       java-headless which

%description
Grails is an Open Source (Apache 2 License) web framework, for the Java platform
aimed at multiplying developers’ productivity thanks to a
Convention-over-Configuration, sensible defaults and opinionated APIs. It
integrates smoothly with the JVM, allowing you to be immediately productive
whilst providing powerful features, including integrated ORM, Domain-Specific
Languages, runtime and compile-time meta-programming and Asynchronous
programming.

%prep
%setup -qn %{name}-core-%{version}

%build
./gradlew install --no-daemon

%install
mkdir -p %{buildroot}/usr/share/%{name}
mkdir -p %{buildroot}%{_bindir}
rm -f bin/*.bat
mv bin dist lib media %{buildroot}/usr/share/%{name}
ln -s /usr/share/%{name}/bin/%{name} $RPM_BUILD_ROOT%{_bindir}/%{name}

%files
%doc LICENSE README.md
/usr/share/%{name}
%{_bindir}/%{name}

%changelog
* Wed Jun 22 2022 - harbottle@room3d3.com - 5.2.0-1
  - Bump version

* Tue May 31 2022 - harbottle@room3d3.com - 5.1.8-1
  - Bump version

* Thu Apr 14 2022 - harbottle@room3d3.com - 5.1.7-1
  - Bump version

* Thu Mar 31 2022 - harbottle@room3d3.com - 5.1.6-1
  - Bump version

* Tue Mar 29 2022 - harbottle@room3d3.com - 5.1.5-1
  - Bump version

* Tue Mar 22 2022 - harbottle@room3d3.com - 5.1.4-1
  - Bump version

* Thu Mar 10 2022 - harbottle@room3d3.com - 5.1.3-1
  - Bump version

* Tue Jan 18 2022 - harbottle@room3d3.com - 5.1.2-1
  - Bump version

* Sat Jan 15 2022 - harbottle@room3d3.com - 5.1.1-1
  - Bump version

* Sun Dec 12 2021 - harbottle@room3d3.com - 5.0.2-1
  - Bump version

* Wed Nov 03 2021 - harbottle@room3d3.com - 5.0.1-1
  - Bump version

* Tue Oct 12 2021 - harbottle@room3d3.com - 5.0.0-1
  - Bump version

* Mon Aug 30 2021 - harbottle@room3d3.com - 4.0.12-1
  - Bump version

* Thu Jun 24 2021 - harbottle@room3d3.com - 4.0.11-1
  - Bump version

* Thu Apr 08 2021 - harbottle@room3d3.com - 4.0.10-1
  - Bump version

* Thu Mar 11 2021 - harbottle@room3d3.com - 4.0.9-1
  - Bump version

* Fri Feb 19 2021 - harbottle@room3d3.com - 4.0.8-1
  - Bump version

* Mon Feb 01 2021 - harbottle@room3d3.com - 4.0.7-1
  - Bump version

* Sat Dec 19 2020 - harbottle@room3d3.com - 4.0.6-1
  - Bump version

* Sun Oct 25 2020 - harbottle@room3d3.com - 4.0.5-1
  - Bump version

* Fri Jul 24 2020 - harbottle@room3d3.com - 4.0.4-1
  - Bump version

* Fri Apr 03 2020 - harbottle@room3d3.com - 4.0.3-1
  - Bump version

* Sat Feb 29 2020 - harbottle@room3d3.com - 4.0.2-1
  - Bump version

* Mon Oct 14 2019 - harbottle@room3d3.com - 4.0.1-1
  - Bump version

* Thu Jul 11 2019 - harbottle@room3d3.com - 4.0.0-1
  - Bump version

* Mon Jun 03 2019 - harbottle@room3d3.com - 3.3.10-1
  - Bump version

* Fri Dec 21 2018 - harbottle@room3d3.com - 3.3.9-1
  - Bump version
* Mon Oct 15 2018 - grainger@gmail.com -  3.3.8-1.el7.harbottle
  - Bump version
* Mon Jul 02 2018 - grainger@gmail.com -  3.3.6-1.el7.harbottle
  - Bump version
* Mon Apr 30 2018 - grainger@gmail.com -  3.3.5-1.el7.harbottle
  - Bump version
* Fri Apr 06 2018 - grainger@gmail.com -  3.3.4-1.el7.harbottle
  - Bump version
* Thu Mar 15 2018 - grainger@gmail.com -  3.3.3-1.el7.harbottle
  - Bump version
* Mon Dec 04 2017 - grainger@gmail.com -  3.3.2-1.el7
  - Bump version
* Fri Sep 29 2017 <grainger@gmail.com> -  3.3.1-1.el7
  - Bump version
* Thu Aug 31 2017 <grainger@gmail.com>
  - Update to 3.3.0
* Mon Jun 12 2017 grainger@gmail.com
- Initial packaging
