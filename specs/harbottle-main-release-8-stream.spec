Name:           harbottle-main-release
Version:        8
Release:        1%{?dist}
Summary:        Harbottle Main repository configuration
Group:          System Environment/Base
License:        GPLv2
URL:            https://gitlab.com/harbottle/harbottle-main-release
Source0:        https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
Source1:        harbottle-main-8-stream.repo
BuildArch:      noarch
Requires:       redhat-release >=  8

%description
This package contains the Harbottle Main repository
GPG key as well as configuration for yum.

%prep
%setup -q  -c -T
install -pm 644 %{SOURCE0} ./GPL

%build

%install
rm -rf $RPM_BUILD_ROOT
install -dm 755 $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d
install -pm 644 %{SOURCE1} $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d/harbottle-main.repo

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc GPL
%config(noreplace) /etc/yum.repos.d/*

%changelog
* Sat Jul 24 2021 - harbottle@room3d3.com - 8-1
  - Initial Package
