%define __jar_repack 0
%global homedir /usr/share/gradle

Name:     gradle
Version:  7.4.2
Release:  1%{?dist}.harbottle
Summary:  Build automation tool
Group:    Applications/System
License:  Apache
URL:      https://%{name}.org
Source0:  https://services.%{name}.org/distributions/%{name}-%{version}-bin.zip
Autoprov: no
Requires: java >= 1:1.7.0 which

%description
Gradle is build automation evolved. Gradle can automate the building,
testing, publishing, deployment and more of software packages or other
types of projects such as generated static websites, generated
documentation or indeed anything else.
Gradle combines the power and flexibility of Ant with the dependency
management and conventions of Maven into a more effective way to
build. Powered by a Groovy DSL and packed with innovation, Gradle
provides a declarative way to describe all kinds of builds through
sensible defaults. Gradle is quickly becoming the build system of
choice for many open source projects, leading edge enterprises and
legacy automation challenges.

%prep
%setup -qn %{name}-%{version}

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -d -m 755 $RPM_BUILD_ROOT%{homedir}
rm -f bin/%{name}.bat
mv * $RPM_BUILD_ROOT%{homedir}
mv $RPM_BUILD_ROOT%{homedir}/{LICENSE,NOTICE,README} .
ln -s %{homedir}/bin/%{name} $RPM_BUILD_ROOT%{_bindir}/%{name}

%files
%license LICENSE NOTICE
%doc README
%{_bindir}/%{name}
%{homedir}

%changelog
* Thu Mar 31 2022 - harbottle@room3d3.com - 7.4.2-1
  - Bump version

* Wed Mar 09 2022 - harbottle@room3d3.com - 7.4.1-1
  - Bump version

* Tue Feb 08 2022 - harbottle@room3d3.com - 7.4-1
  - Bump version

* Sat Jan 15 2022 - harbottle@room3d3.com - 7.3.3-1
  - Bump version

* Sun Dec 12 2021 - harbottle@room3d3.com - 7.3.1-1
  - Bump version

* Tue Aug 17 2021 - harbottle@room3d3.com - 7.2-1
  - Bump version

* Fri Jul 02 2021 - harbottle@room3d3.com - 7.1.1-1
  - Bump version

* Tue Jun 15 2021 - harbottle@room3d3.com - 7.1-1
  - Bump version

* Fri May 14 2021 - harbottle@room3d3.com - 7.0.2-1
  - Bump version

* Mon May 10 2021 - harbottle@room3d3.com - 7.0.1-1
  - Bump version

* Sat Apr 10 2021 - harbottle@room3d3.com - 7.0-1
  - Bump version

* Mon Feb 22 2021 - harbottle@room3d3.com - 6.8.3-1
  - Bump version

* Fri Feb 05 2021 - harbottle@room3d3.com - 6.8.2-1
  - Bump version

* Fri Jan 22 2021 - harbottle@room3d3.com - 6.8.1-1
  - Bump version

* Fri Jan 08 2021 - harbottle@room3d3.com - 6.8-1
  - Bump version

* Fri Nov 20 2020 - harbottle@room3d3.com - 6.7.1-1
  - Bump version

* Fri Oct 16 2020 - harbottle@room3d3.com - 6.7-1
  - Bump version

* Tue Aug 25 2020 - harbottle@room3d3.com - 6.6.1-1
  - Bump version

* Mon Aug 10 2020 - harbottle@room3d3.com - 6.6-1
  - Bump version

* Tue Jun 30 2020 - harbottle@room3d3.com - 6.5.1-1
  - Bump version

* Tue Jun 02 2020 - harbottle@room3d3.com - 6.5-1
  - Bump version

* Fri May 15 2020 - harbottle@room3d3.com - 6.4.1-1
  - Bump version

* Tue May 05 2020 - harbottle@room3d3.com - 6.4-1
  - Bump version

* Tue Mar 24 2020 - harbottle@room3d3.com - 6.3-1
  - Bump version

* Wed Mar 04 2020 - harbottle@room3d3.com - 6.2.2-1
  - Bump version

* Sat Feb 29 2020 - harbottle@room3d3.com - 6.2.1-1
  - Bump version

* Fri Jan 24 2020 - harbottle@room3d3.com - 6.1.1-1
  - Bump version

* Sat Jan 18 2020 - harbottle@room3d3.com - 6.1-1
  - Bump version
  - Fix build
  - Build for el8

* Mon Nov 18 2019 - harbottle@room3d3.com - 6.0.1-1
  - Bump version

* Fri Nov 08 2019 - harbottle@room3d3.com - 6.0-1
  - Bump version

* Fri Nov 01 2019 - harbottle@room3d3.com - 5.6.4-1
  - Bump version

* Fri Oct 18 2019 - harbottle@room3d3.com - 5.6.3-1
  - Bump version

* Thu Sep 05 2019 - harbottle@room3d3.com - 5.6.2-1
  - Bump version

* Wed Aug 28 2019 - harbottle@room3d3.com - 5.6.1-1
  - Bump version

* Wed Aug 14 2019 - harbottle@room3d3.com - 5.6-1
  - Bump version

* Wed Jul 10 2019 - harbottle@room3d3.com - 5.5.1-1
  - Bump version

* Fri Jun 28 2019 - harbottle@room3d3.com - 5.5-1
  - Bump version

* Fri Apr 26 2019 - harbottle@room3d3.com - 5.4.1-1
  - Bump version

* Tue Apr 16 2019 - harbottle@room3d3.com - 5.4-1
  - Bump version

* Thu Mar 28 2019 - harbottle@room3d3.com - 5.3.1-1
  - Bump version

* Wed Mar 20 2019 - harbottle@room3d3.com - 5.3-1
  - Bump version

* Fri Mar 01 2019 - harbottle@room3d3.com - 5.2.1-1
  - Initial package
