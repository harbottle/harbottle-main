%define __jar_repack 0

Name:     sonarqube-css
Version:  1.4.2.2002
Release:  1%{?dist}.harbottle
Summary:  SonarQube CSS plugin
Group:    Applications/System
License:  LGPL-3.0
Source0:  https://binaries.sonarsource.com/Distribution/sonar-css-plugin/sonar-css-plugin-%{version}.jar
Autoprov: no

%description
CSS plugin for SonarQube.

%prep

%install
install -d -m 755 $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins
mv %{SOURCE0} $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins

%pre
rm -f %{_var}/lib/sonarqube/extensions/plugins/sonar-css-plugin-*.jar
getent group sonarqube >/dev/null || groupadd -f -r sonarqube
getent passwd sonarqube >/dev/null || useradd -r -g sonarqube -d /usr/share/sonarqube -s /sbin/nologin -c "SonarQube user" sonarqube
exit 0

%files
%attr(0644,sonarqube,sonarqube) %{_var}/lib/sonarqube/extensions/plugins/sonar-css-plugin-%{version}.jar

%changelog
* Fri Apr 30 2021 - harbottle@room3d3.com - 1.4.2.2002-1
  - Bump version

* Fri Apr 30 2021 - harbottle@room3d3.com - 1.4.1.1981-1
  - Bump version

* Tue Mar 02 2021 - harbottle@room3d3.com - 1.4.0.1899-1
  - Bump version

* Tue Feb 09 2021 - harbottle@room3d3.com - 1.3.2.1782-1
  - Bump version

* Wed Oct 21 2020 - harbottle@room3d3.com - 1.3.1.1642-1
  - Bump version

* Thu Oct 01 2020 - harbottle@room3d3.com - 1.3.0.1580-1
  - Bump version

* Mon Jan 13 2020 - harbottle@room3d3.com - 1.2.0.1325-1
  - Bump version

* Tue Dec 10 2019 - harbottle@room3d3.com - 1.1.1.1010-3
  - Spec file changes for el8

* Sun Jul 21 2019 - harbottle@room3d3.com - 1.1.1.1010-2
  - Fix plugin ownership

* Sun Jul 21 2019 - harbottle@room3d3.com - 1.1.1.1010-1
  - Initial package
