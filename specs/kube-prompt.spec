Name:    kube-prompt
Version: 1.0.11
Release: 1%{?dist}.harbottle
Summary: An interactive kubernetes client featuring auto-complete
Group:   Applications/System
License: MIT
URL:     https://github.com/c-bata/%{name}
Source0: %{url}/releases/download/v%{version}/%{name}_v%{version}_linux_amd64.zip
Source1: https://raw.githubusercontent.com/c-bata/%{name}/v%{version}/LICENSE
Source2: https://raw.githubusercontent.com/c-bata/%{name}/v%{version}/README.md

%description
kube-prompt accepts the same commands as the kubectl, except you don't need to
provide the kubectl prefix. So it doesn't require the additional cost to use
this cli.

%prep
%setup -q -c

%install
mv %{SOURCE1} .
mv %{SOURCE2} .
install -d -m 0755 $RPM_BUILD_ROOT%{_bindir}
install -m 0755 %{name} $RPM_BUILD_ROOT%{_bindir}

%files
%license LICENSE
%doc README.md
%{_bindir}/%{name}

%changelog
* Wed Feb 10 2021 - harbottle@room3d3.com - 1.0.11-1
  - Bump version

* Tue Dec 03 2019 - harbottle@room3d3.com - 1.0.10-1
  - Bump version

* Sat Nov 02 2019 - harbottle@room3d3.com - 1.0.9-1
  - Bump version

* Mon Sep 09 2019 - harbottle@room3d3.com - 1.0.8-1
  - Bump version

* Mon Aug 05 2019 - harbottle@room3d3.com - 1.0.7-1
  - Bump version

* Thu Jul 04 2019 - harbottle@room3d3.com - 1.0.6-1
  - Initial package
