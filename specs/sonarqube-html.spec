%define __jar_repack 0

Name:     sonarqube-html
Version:  3.6.0.3106
Release:  1%{?dist}.harbottle
Summary:  SonarQube HTML plugin
Group:    Applications/System
License:  LGPL-3.0
Source0:  https://binaries.sonarsource.com/Distribution/sonar-html-plugin/sonar-html-plugin-%{version}.jar
Autoprov: no

%description
HTML plugin for SonarQube.

%prep

%install
install -d -m 755 $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins
mv %{SOURCE0} $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins

%pre
rm -f %{_var}/lib/sonarqube/extensions/plugins/sonar-html-plugin-*.jar
getent group sonarqube >/dev/null || groupadd -f -r sonarqube
getent passwd sonarqube >/dev/null || useradd -r -g sonarqube -d /usr/share/sonarqube -s /sbin/nologin -c "SonarQube user" sonarqube
exit 0

%files
%attr(0644,sonarqube,sonarqube) %{_var}/lib/sonarqube/extensions/plugins/sonar-html-plugin-%{version}.jar

%changelog
* Mon Jan 24 2022 - harbottle@room3d3.com - 3.6.0.3106-1
  - Bump version

* Sun Dec 12 2021 - harbottle@room3d3.com - 3.5.0.3009-1
  - Bump version

* Thu Apr 29 2021 - harbottle@room3d3.com - 3.4.0.2754-1
  - Bump version

* Thu Nov 12 2020 - harbottle@room3d3.com - 3.3.0.2534-1
  - Bump version

* Tue Dec 10 2019 - harbottle@room3d3.com - 3.2.0.2082-2
  - Spec file changes for el8

* Mon Aug 19 2019 - harbottle@room3d3.com - 3.2.0.2082-1
  - Bump version

* Sun Jul 21 2019 - harbottle@room3d3.com - 3.1.0.1615-2
  - Fix plugin ownership

* Sun Jul 21 2019 - harbottle@room3d3.com - 3.1.0.1615-1
  - Initial package
