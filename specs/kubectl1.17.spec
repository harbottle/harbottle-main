%global owner kubernetes
%global repo kubernetes
%global host github.com
%global archive v%{version}.tar.gz
%global dir %{repo}-%{version}
%global namespace k8s.io/%{repo}
%global cmd kubectl
%global tree 1.17
%global go_version 1.13.6

Name:          %{cmd}%{tree}
Version:       1.17.17
Release:       1%{?dist}.harbottle
Summary:       %{cmd} controls the Kubernetes cluster manager.
Group:         Applications/System
License:       Apache-2.0
Url:           https://%{host}/%{owner}/%{repo}
Source0:       %{url}/archive/%{archive}
Source1:       https://dl.google.com/go/go%{go_version}.linux-amd64.tar.gz
BuildRequires: which make rsync

%description
%{cmd} controls the Kubernetes cluster manager. This package provides an
up-to-date version of the command. It can be run using command: %{name}

%prep
%setup -q -n %{dir}
%setup -q -T -D -a 1 -n %{dir}

%build
%define debug_package %{nil}
export GOPATH=$PWD
export GOROOT=$PWD/go
export PATH=$GOPATH/bin:$GOROOT/bin:$PATH
mkdir -p src/%{namespace}/
shopt -s extglob dotglob
mv !(src|go) src/%{namespace}/
shopt -u extglob dotglob
pushd src/%{namespace}/
make %{cmd}
popd

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 755 src/%{namespace}/_output/bin/%{cmd} $RPM_BUILD_ROOT%{_bindir}/%{name}

%files
%license src/%{namespace}/LICENSE
%doc src/%{namespace}/{*.md,OWNERS,SECURITY_CONTACTS}
%{_bindir}/%{name}

%changelog
* Wed Jan 13 2021 - harbottle@room3d3.com - 1.17.17-1
  - Bump version

* Fri Dec 18 2020 - harbottle@room3d3.com - 1.17.16-1
  - Bump version

* Wed Dec 09 2020 - harbottle@room3d3.com - 1.17.15-1
  - Bump version

* Wed Nov 11 2020 - harbottle@room3d3.com - 1.17.14-1
  - Bump version

* Fri Oct 16 2020 - harbottle@room3d3.com - 1.17.13-1
  - Bump version

* Wed Sep 16 2020 - harbottle@room3d3.com - 1.17.12-1
  - Bump version

* Thu Aug 13 2020 - harbottle@room3d3.com - 1.17.11-1
  - Bump version

* Thu Aug 13 2020 - harbottle@room3d3.com - 1.17.10-1
  - Bump version

* Fri Jul 24 2020 - harbottle@room3d3.com - 1.17.9-1
  - Bump version

* Fri Jun 26 2020 - harbottle@room3d3.com - 1.17.8-1
  - Bump version

* Wed Jun 17 2020 - harbottle@room3d3.com - 1.17.7-1
  - Bump version

* Wed May 20 2020 - harbottle@room3d3.com - 1.17.6-1
  - Bump version

* Thu Apr 16 2020 - harbottle@room3d3.com - 1.17.5-1
  - Bump version

* Fri Mar 13 2020 - harbottle@room3d3.com - 1.17.4-1
  - Bump version

* Tue Feb 11 2020 - harbottle@room3d3.com - 1.17.3-1
  - Bump version

* Tue Jan 21 2020 - harbottle@room3d3.com - 1.17.2-1
  - Bump version

* Sat Jan 18 2020 - harbottle@room3d3.com - 1.17.1-1
  - Initial package
