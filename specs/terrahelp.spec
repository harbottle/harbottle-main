%global namespace github.com/opencredo/%{name}

Name:          terrahelp
Version:       0.7.5
Release:       1%{?dist}.harbottle
Summary:       Terraform helper
Group:         Applications/System
License:       Apache-2.0
URL:           https://%{namespace}
Source0:       %{url}/archive/v%{version}.tar.gz
BuildRequires: golang >= 1.11
BuildRequires: make which zip

%description
Terrahelp is as a command line utility written in Go and is aimed at providing
supplementary functionality which can sometimes prove useful when working with
Terraform.

%prep
%setup -q -n %{name}-%{version}

%build
%define debug_package %{nil}
export GOPROXY=https://proxy.golang.org
export GOPATH=$PWD
export PATH=${PATH}:${GOPATH}/bin
mkdir -p src/%{namespace}
shopt -s extglob dotglob
mv !(src) src/%{namespace}
shopt -u extglob dotglob
pushd src/%{namespace}
go build -o=%{name}-linux-amd64
popd

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 0755 src/%{namespace}/%{name}-linux-amd64 $RPM_BUILD_ROOT%{_bindir}/%{name}

%clean
rm -rf %{buildroot}

%files
%license src/%{namespace}/LICENSE
%doc src/%{namespace}/*.md
%{_bindir}/%{name}

%changelog
* Mon Oct 04 2021 - harbottle@room3d3.com - 0.7.5-1
  - Bump version

* Mon Dec 09 2019 - harbottle@room3d3.com - 0.7.4-2
  - Tidy-up spec file

* Mon Dec 09 2019 - harbottle@room3d3.com - 0.7.4-1
  - Bump version

* Fri Dec 06 2019 - harbottle@room3d3.com - 0.7.3-2
  - Build from source again

* Fri Dec 06 2019 - harbottle@room3d3.com - 0.7.3-1
  - Download binary, as build is now broken
  - Bump version

* Mon Sep 09 2019 - harbottle@room3d3.com - 0.7.1-1
  - Bump version

* Mon Sep 09 2019 - harbottle@room3d3.com - 0.7.0-1
  - Bump version

* Fri Aug 02 2019 - harbottle@room3d3.com - 0.6.3-1
  - Initial version
