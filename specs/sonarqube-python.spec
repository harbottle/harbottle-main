%define __jar_repack 0

Name:     sonarqube-python
Version:  3.15.1.9817
Release:  1%{?dist}.harbottle
Summary:  SonarQube Python plugin
Group:    Applications/System
License:  LGPL-3.0
Source0:  https://binaries.sonarsource.com/Distribution/sonar-python-plugin/sonar-python-plugin-%{version}.jar
Autoprov: no

%description
Python plugin for SonarQube.

%prep

%install
install -d -m 755 $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins
mv %{SOURCE0} $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins

%pre
rm -f %{_var}/lib/sonarqube/extensions/plugins/sonar-python-plugin-*.jar
getent group sonarqube >/dev/null || groupadd -f -r sonarqube
getent passwd sonarqube >/dev/null || useradd -r -g sonarqube -d /usr/share/sonarqube -s /sbin/nologin -c "SonarQube user" sonarqube
exit 0

%files
%attr(0644,sonarqube,sonarqube) %{_var}/lib/sonarqube/extensions/plugins/sonar-python-plugin-%{version}.jar

%changelog
* Wed Jun 22 2022 - harbottle@room3d3.com - 3.15.1.9817-1
  - Bump version

* Fri Jun 10 2022 - harbottle@room3d3.com - 3.15.0.9787-1
  - Bump version

* Tue May 10 2022 - harbottle@room3d3.com - 3.14.0.9677-1
  - Bump version

* Thu Apr 21 2022 - harbottle@room3d3.com - 3.13.0.9611-1
  - Bump version

* Tue Mar 29 2022 - harbottle@room3d3.com - 3.12.0.9583-1
  - Bump version

* Thu Mar 24 2022 - harbottle@room3d3.com - 3.11.0.9522-1
  - Bump version

* Tue Feb 08 2022 - harbottle@room3d3.com - 3.10.0.9380-1
  - Bump version

* Sat Jan 15 2022 - harbottle@room3d3.com - 3.9.0.9230-1
  - Bump version

* Sun Dec 12 2021 - harbottle@room3d3.com - 3.8.0.8883-1
  - Bump version

* Fri Jul 23 2021 - harbottle@room3d3.com - 3.6.0.8488-1
  - Bump version

* Wed May 26 2021 - harbottle@room3d3.com - 3.5.0.8244-1
  - Bump version

* Thu Apr 29 2021 - harbottle@room3d3.com - 3.4.1.8066-1
  - Bump version

* Wed Mar 24 2021 - harbottle@room3d3.com - 3.4.0.7980-1
  - Bump version

* Mon Mar 01 2021 - harbottle@room3d3.com - 3.3.0.7945-1
  - Bump version

* Mon Feb 01 2021 - harbottle@room3d3.com - 3.2.0.7856-1
  - Bump version

* Fri Aug 28 2020 - harbottle@room3d3.com - 3.1.0.7619-1
  - Bump version

* Fri Jul 24 2020 - harbottle@room3d3.com - 3.0.0.7330-1
  - Bump version

* Thu Jun 25 2020 - harbottle@room3d3.com - 2.13.0.7236-1
  - Bump version

* Thu Jun 11 2020 - harbottle@room3d3.com - 2.12.0.7065-1
  - Bump version

* Tue May 26 2020 - harbottle@room3d3.com - 2.11.0.6843-1
  - Bump version

* Wed May 06 2020 - harbottle@room3d3.com - 2.10.0.6571-1
  - Bump version

* Fri Apr 17 2020 - harbottle@room3d3.com - 2.9.0.6410-1
  - Bump version

* Tue Mar 24 2020 - harbottle@room3d3.com - 2.8.0.6204-1
  - Bump version

* Mon Mar 09 2020 - harbottle@room3d3.com - 2.7.0.5975-1
  - Bump version

* Sat Feb 29 2020 - harbottle@room3d3.com - 2.6.0.5863-1
  - Bump version

* Fri Feb 14 2020 - harbottle@room3d3.com - 2.5.0.5733-1
  - Bump version

* Mon Jan 27 2020 - harbottle@room3d3.com - 2.4.0.5513-1
  - Bump version

* Thu Dec 19 2019 - harbottle@room3d3.com - 2.3.0.5351-1
  - Bump version

* Tue Dec 10 2019 - harbottle@room3d3.com - 2.2.0.5331-2
  - Spec file changes for el8

* Fri Nov 29 2019 - harbottle@room3d3.com - 2.2.0.5331-1
  - Bump version

* Tue Nov 12 2019 - harbottle@room3d3.com - 2.1.0.5269-1
  - Bump version

* Mon Oct 28 2019 - harbottle@room3d3.com - 2.0.0.5043-1
  - Bump version

* Wed Oct 16 2019 - harbottle@room3d3.com - 1.17.0.4900-1
  - Bump version

* Fri Sep 27 2019 - harbottle@room3d3.com - 1.16.0.4432-1
  - Bump version

* Mon Sep 16 2019 - harbottle@room3d3.com - 1.15.1.4116-1
  - Bump version

* Fri Sep 13 2019 - harbottle@room3d3.com - 1.15.0.4069-1
  - Bump version

* Sun Jul 21 2019 - harbottle@room3d3.com - 1.14.1.3143-2
  - Fix plugin ownership

* Sun Jul 21 2019 - harbottle@room3d3.com - 1.14.1.3143-1
  - Initial package
