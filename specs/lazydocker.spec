Name:    lazydocker
Version: 0.18.1
Release: 1%{?dist}.harbottle
Summary: A simple terminal UI for both docker and docker-compose
Group:   Applications/System
License: MIT
URL:     https://github.com/jesseduffield/%{name}
Source0: %{url}/releases/download/v%{version}/%{name}_%{version}_Linux_x86_64.tar.gz

%description
Memorising docker commands is hard. Memorising aliases is slightly less hard.
Keeping track of your containers across multiple terminal windows is near
impossible. What if you had all the information you needed in one terminal
window with every common command living one keypress away (and the ability to
add custom commands as well). Lazydocker's goal is to make that dream a reality.

%prep
%setup -q -c

%install
install -d -m 0755 $RPM_BUILD_ROOT%{_bindir}
install -m 0755 %{name} $RPM_BUILD_ROOT%{_bindir}

%files
%license LICENSE
%doc README.md
%{_bindir}/%{name}

%changelog
* Wed May 11 2022 - harbottle@room3d3.com - 0.18.1-1
  - Bump version

* Wed May 11 2022 - harbottle@room3d3.com - 0.18-1
  - Bump version

* Mon May 09 2022 - harbottle@room3d3.com - 0.17.1-1
  - Bump version

* Mon May 09 2022 - harbottle@room3d3.com - 0.16-1
  - Bump version

* Wed Mar 24 2021 - harbottle@room3d3.com - 0.12-1
  - Bump version

* Fri Nov 20 2020 - harbottle@room3d3.com - 0.10-1
  - Bump version

* Thu May 28 2020 - harbottle@room3d3.com - 0.9-1
  - Bump version

* Sun Feb 02 2020 - harbottle@room3d3.com - 0.8-1
  - Bump version

* Sat Nov 16 2019 - harbottle@room3d3.com - 0.7.6-1
  - Bump version

* Mon Nov 11 2019 - harbottle@room3d3.com - 0.7.5-1
  - Bump version

* Sat Sep 07 2019 - harbottle@room3d3.com - 0.7.4-1
  - Bump version

* Sat Sep 07 2019 - harbottle@room3d3.com - 0.7.2-1
  - Bump version

* Sun Aug 25 2019 - harbottle@room3d3.com - 0.7.1-1
  - Bump version

* Wed Aug 14 2019 - harbottle@room3d3.com - 0.7-1
  - Bump version

* Fri Aug 02 2019 - harbottle@room3d3.com - 0.6.4-1
  - Bump version

* Mon Jul 29 2019 - harbottle@room3d3.com - 0.6.3-1
  - Bump version

* Mon Jul 29 2019 - harbottle@room3d3.com - 0.6-1
  - Bump version

* Sun Jul 07 2019 - harbottle@room3d3.com - 0.5.5-1
  - Bump version

* Fri Jul 05 2019 - harbottle@room3d3.com - 0.5.4-1
  - Bump version

* Thu Jul 04 2019 - harbottle@room3d3.com - 0.4-1
  - Initial package
