%define __jar_repack 0

Name:     sonarqube-pmd
Version:  3.4.0
Release:  1%{?dist}.harbottle
Summary:  SonarQube PMD plugin
Group:    Applications/System
License:  LGPL-3.0
URL:      https://github.com/jensgerdes/sonar-pmd
Source0:  %{url}/releases/download/%{version}/sonar-pmd-plugin-%{version}.jar
Autoprov: no

%description
PMD plugin for SonarQube.

%prep

%install
install -d -m 755 $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins
mv %{SOURCE0} $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins

%pre
rm -f %{_var}/lib/sonarqube/extensions/plugins/sonar-pmd-plugin-*.jar
getent group sonarqube >/dev/null || groupadd -f -r sonarqube
getent passwd sonarqube >/dev/null || useradd -r -g sonarqube -d /usr/share/sonarqube -s /sbin/nologin -c "SonarQube user" sonarqube
exit 0

%files
%attr(0644,sonarqube,sonarqube) %{_var}/lib/sonarqube/extensions/plugins/sonar-pmd-plugin-%{version}.jar

%changelog
* Wed May 11 2022 - harbottle@room3d3.com - 3.4.0-1
  - Bump version

* Fri Jan 29 2021 - harbottle@room3d3.com - 3.3.1-1
  - Bump version

* Tue Jan 12 2021 - harbottle@room3d3.com - 3.3.0-1
  - Bump version

* Fri Jan 03 2020 - harbottle@room3d3.com - 3.2.1-1
  - Initial package
