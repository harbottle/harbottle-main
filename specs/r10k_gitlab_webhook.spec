Name:             r10k_gitlab_webhook
Version:          0.1.3
Release:          5%{?dist}.harbottle
Summary:          Web hook to trigger r10k deploy
Group:            Applications/System
License:          MIT
URL:              https://github.com/spuder/r10k_gitlab_webhook
Source0:          r10k_gitlab_webhook.conf
Source1:          r10k_gitlab_webhook.service
BuildRequires:    systemd-units
Requires:         rubygem-r10k_gitlab_webhook = %{version}
Requires(pre):    shadow-utils
Requires(post):   systemd-units
Requires(preun):  systemd-units
Requires(postun): systemd-units

%description
Systemd wrapper for the ruby gem that starts a webrick webservice and listens
for requests. When a POST is recieved, it executes the following command:

  sudo r10k deploy environment -pv debug

%install
install -d -m 755 $RPM_BUILD_ROOT%{_localstatedir}/log
install -d -m 755 $RPM_BUILD_ROOT%{_sysconfdir}
install -d -m 755 $RPM_BUILD_ROOT%{_unitdir}
install -m 644 %{SOURCE0} $RPM_BUILD_ROOT%{_sysconfdir}/%{name}.conf
install -m 644 %{SOURCE1} $RPM_BUILD_ROOT%{_unitdir}/%{name}.service

%post
%systemd_post %{name}.service

%preun
%systemd_preun %{name}.service

%postun
%systemd_postun_with_restart %{name}.service

%files
%config(noreplace) %{_sysconfdir}/%{name}.conf
%{_unitdir}/%{name}.service

%changelog
* Sat Jan 04 2020 - harbottle@room3d3.com - 0.1.3-5
  - Improve dependencies

* Mon Dec 30 2019 - harbottle@room3d3.com - 0.1.3-4
  - Build for el8

* Sat Jan 26 2019 - harbottle@room3d3.com - 0.1.3-3
  - Add harbottle to release

* Fri Oct 20 2017 <grainger@gmail.com> - 0.1.3-2
  - Update default port
* Wed Oct 18 2017 <grainger@gmail.com> - 0.1.3-1
  - Initial packaging
