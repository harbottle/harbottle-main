#require "bundler/setup"
#require "rpm"
require "fpm"
require "yaml"
require "colorize"
require "popen4"
require "fileutils"
require "pathname"
require "filesize"
require "net/http"
#require 'rubygems/package'
require 'zlib'
require 'ruby_expect'

TAR_LONGLINK = '././@LongLink'

repo_name = ENV['CI_PROJECT_NAME']
repo_url_base = ENV['REPO_URL_BASE']
copr_url_base = ENV['COPR_URL_BASE']
homepage = ENV['CI_PROJECT_URL']
gpg_name = ENV['GPG_NAME']
gpg_file_name = ENV['GPG_FILE_NAME']
gpg_private_key = ENV['GPG_PRIVATE_KEY']
gpg_public_key = ENV['GPG_PUBLIC_KEY']
gpg_pass_phrase = ENV['GPG_PASS_PHRASE']
gpg_key_id = ENV['GPG_KEY_ID']
primary_repo_url = ENV['PRIMARY_REPO_URL']

default_dependencies = ['ruby']
pad_length = 38
$stdout.sync = true

# Functions
# Function to convert (yaml) object to has with symbol keys
def symbolize(obj)
    return obj.inject({}){|memo,(k,v)| memo[k.to_sym] =  symbolize(v); memo} if obj.is_a? Hash
    return obj.inject([]){|memo,v    | memo           << symbolize(v); memo} if obj.is_a? Array
    return obj
end

# Function to check if a URL is valid
def url_exist?(url_string)
  url = URI.parse(url_string)
  req = Net::HTTP.new(url.host, url.port)
  req.use_ssl = (url.scheme == 'https')
  path = url.path
  res = req.request_head(path || '/')
  res.code != "404" # false if returns 404 - not found
  rescue Errno::ENOENT
  false # false if can't find the server
end

# Function to check if an RPM exists via web access (cycling through iterations)
# Returns filename and iteration number
def rpm_exist?(repo_url_base, rpm)
  components = rpm.split('-')
  name = components[0..-3].join('-')
  ver = components[-2]
  it_arch_ext = components[-1]
  ext = it_arch_ext.split('.')[-1]
  arch = it_arch_ext.split('.')[-2]
  iteration_text = it_arch_ext.split('.')[-3]

  for i in 1..20
    filename = "#{name}-#{ver}-#{i}.#{iteration_text}.#{arch}.#{ext}"
    if url_exist?("#{repo_url_base}#{filename}")
      return {:filename => filename, :iteration_number => i}
    end
  end
  return false
end

# Function to download file from web.  Returns filename.
def download_file(url_string)
  url = URI.parse(url_string)
  path = url.path
  file_name = "#{path.split('/')[-1]}.downloaded"
  File.open(file_name,'w'){ |f|
    req = Net::HTTP.new(url.host,url.port)
    req.use_ssl = (url.scheme == 'https')
    req.request_get(url.path){ |res|
      res.read_body{ |seg|
        f << seg
        sleep 0.005
      }
    }
  }
  return file_name
end

def unzip (tar_gz_archive)

  destination = '.'

  Gem::Package::TarReader.new( Zlib::GzipReader.open tar_gz_archive ) do |tar|
    dest = nil
    tar.each do |entry|
      if entry.full_name == TAR_LONGLINK
        dest = File.join destination, entry.read.strip
        next
      end
      dest ||= File.join destination, entry.full_name
      if entry.directory?
        FileUtils.rm_rf dest unless File.directory? dest
        FileUtils.mkdir_p dest, :mode => entry.header.mode, :verbose => false
      elsif entry.file?
        FileUtils.rm_rf dest unless File.file? dest
        File.open dest, "wb" do |f|
          f.print entry.read
        end
        FileUtils.chmod entry.header.mode, dest, :verbose => false
      elsif entry.header.typeflag == '2' #Symlink!
        File.symlink entry.header.linkname, dest
      end
      dest = nil
    end
  end
end

# Function to check if package is currently available using yum
def pkg_in_yum?(package)
  outputs = ''
  errors = ''
  command_status = POpen4::popen4("repoquery --whatprovides '#{package}' --qf '%{REPO}'")  do |stdout, stderr, stdin|
    stdout.each do |line|
      outputs << "#{line.strip}\n"
    end
    stderr.each do |line|
      errors << "#{line.strip}\n"
    end
  end
  if command_status.exitstatus == 0 && outputs.length > 0
    return outputs.strip
  else
    return false
  end
end

# Function to get font-awesome icon for file
def get_icon(file)
  if File.ftype(file) == 'directory'
    icon = "<i class='fas fa-folder'></i>"
  else
    if File.extname(file) == '.rpm'
      icon = "<i class='fas fa-archive'></i>"
    else
      icon = "<i class='fas fa-file'></i>"
    end
  end
  return icon
end

# Function to get system ready to sign RPM files
def setup_gpg(gpg_name, private_key, public_key)
  file = File.open("#{ENV['HOME']}/.rpmmacros", "w")
  file.write(<<-RPMMACROS.gsub(/^ {6}/, ''))
    %_signature gpg
    %_gpg_path /root/.gnupg
    %_gpg_name #{gpg_name}
    %_gpgbin /usr/bin/gpg
    %_gpg_digest_algo sha256
  RPMMACROS
  file.close
  file = File.open("/tmp/private", "w")
  file.write(private_key)
  file.close
  file = File.open("/tmp/public", "w")
  file.write(public_key)
  file.close
  commands = {
  'Importing GPG private key.......' => 'gpg --allow-secret-key-import --import /tmp/private',
  'Importing GPG public key........' => 'rpm --import /tmp/public' }
  commands.each do |key, value|
    command_desc = key
    command = value
    print "#{command_desc}"
    outputs = ''
    errors =  ''
    command_status = POpen4::popen4(command) do |stdout, stderr, stdin|
      stdout.each do |line|
        outputs << "#{line.strip}\n"
      end
      stderr.each do |line|
        errors << "#{line.strip}\n"
      end
    end
    if command_status.exitstatus == 0
      print "Success: Action completed.\n".green
    else
      print "Failure:\n".red
      print "#{outputs.strip}\n".red
      print "#{errors.strip}\n".red
      global_status = command_status.exitstatus
    end
  end
end

# Function to sign RPM file
def sign_rpm(rpm_file, pass_phrase, key_id)
  until rpm_is_signed?(rpm_file, key_id)
    command = "rpm --resign #{rpm_file}"
    exp = RubyExpect::Expect.spawn(command)
    exp.procedure do
      each do
        expect 'Enter pass phrase:' do
          send pass_phrase
        end
      end
    end
  end
end

# Function to check if package is signed correctly
def rpm_is_signed?(rpm_file, key_id)
  outputs = ''
  errors = ''
  found_key = ''
  command_status = POpen4::popen4("rpm -K #{rpm_file} -v")  do |stdout, stderr, stdin|
    stdout.each do |line|
      outputs << "#{line.strip}\n"
    end
    stderr.each do |line|
      errors << "#{line.strip}\n"
    end
  end
  if outputs =~ /^.*key ID .*\:.*$/i
    found_key = /^.*key ID (.*)\:.*$/.match(outputs).captures[-1]
  end
  if found_key == key_id
    return true
  else
    return false
  end
end

# Function to render HTML directory listings
def render_html(path, level = 0, repo_url_base, primary_repo_url, primary_repo_distro)
  if File.directory?(path)
    pn = Pathname.new(path)
    dir_name = "/#{Pathname(pn).each_filename.to_a[2..-1].join('/')}"
    leaf_name = "#{Pathname(pn).each_filename.to_a[-1]}"
    file = File.open("#{pn}/index.html", "a")
    file.write(<<-HTML.gsub(/^ {6}/, ''))
      <!DOCTYPE html>
      <html>
        <head>
          <title>harbottle-main</title>
          <script type='text/javascript'>
            if(window.location.protocol != 'https:') {
              location.href = location.href.replace("http://", "https://");
            }
          </script>
          <link href='#{"../"*level}bower_components/bootstrap/dist/css/bootstrap.min.css'
                rel='stylesheet'>
          <link href='#{"../"*level}bower_components/bootstrap/dist/css/bootstrap-theme.min.css'
                rel='stylesheet'>
          <link href='#{"../"*level}bower_components/font-awesome-5/css/all.min.css'
                rel='stylesheet'>
          <link href='#{"../"*level}theme.css' rel='stylesheet'>
          <link rel='icon' href='#{"../"*level}favicon.png'>
        </head>
        <body>
          <nav class='navbar navbar-inverse navbar-fixed-top'>
            <div class='container'>
              <div class='navbar-header'>
                <button type='button' class='navbar-toggle collapsed'
                        data-toggle='collapse' data-target='#navbar'
                        aria-expanded='false' aria-controls='navbar'>
                  <span class='sr-only'>Toggle navigation</span>
                  <span class='icon-bar'></span>
                  <span class='icon-bar'></span>
                  <span class='icon-bar'></span>
                </button>
              </div>
              <div id='navbar' class='navbar-collapse collapse'>
                <ul class='nav navbar-nav navbar-left'>
                  <li class='active'>
                    <a href='#{"../"*level}#'>
                      harbottle-main <span class='sr-only'>(current)</span>
                    </a>
                  </li>
                  <li>
                    <a href='#{"../"*level}../wine32#'>
                      wine32 <span class='sr-only'>(current)</span>
                    </a>
                  </li>
                  <li>
                    <a href='#{"../"*level}../epmel#'>
                      epmel <span class='sr-only'>(current)</span>
                    </a>
                  </li>
                  <li>
                  <li>
                    <a href='#{"../"*level}../ergel#'>
                      ergel <span class='sr-only'>(current)</span>
                    </a>
                  </li>
                  <li>
                    <a href='#{"../"*level}../epypel#'>
                      epypel <span class='sr-only'>(current)</span>
                    </a>
                  </li>
                </ul>
                <ul class='nav navbar-nav navbar-right'>
    HTML
    if File.exist?("#{pn}/harbottle-main-release.rpm")
      file.write(<<-HTML.gsub(/^ {6}/, ''))
                  <li>
                    <a href='harbottle-main-release.rpm'>
                      Release RPM Permalink
                    </a>
                  </li>
      HTML
    end
    if File.exist?("#{pn}/../harbottle-main-release.rpm")
      file.write(<<-HTML.gsub(/^ {6}/, ''))
                  <li>
                    <a href='../harbottle-main-release.rpm'>
                      Release RPM Permalink
                    </a>
                  </li>
      HTML
    end
    file.write(<<-HTML.gsub(/^ {6}/, ''))
                  <li>
                    <a href='#{"../"*level}RPM-GPG-KEY-harbottle'>
                      GPG key
                    </a>
                  </li>
                  <li>
                    <a href='https://gitlab.com/harbottle/harbottle-main/issues/new'>
                      Request a package
                    </a>
                  </li>
                  <li>
                    <a href='https://gitlab.com/harbottle/harbottle-main'>About</a>
                  </li>
                  <li>
                    <a href='https://twitter.com/harbottle_liger'>
                       Contact
                       <i class='fab fa-twitter' aria-hidden='true'></i>
                    </a>
                  </li>
                  <li>
                    <a href='https://www.patreon.com/harbottle'>
                      Donate
                      <i class='fab fa-patreon' aria-hidden='true'></i>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </nav>
          <div class='container theme-showcase' role='main'>
            <div class="alert alert-success">
              <strong>Find this repo useful?</strong>
              Please consider
              <a href="https://www.patreon.com/harbottle">
                supporting my work by donating on Patreon!
                <i class='fab fa-patreon' aria-hidden='true'></i>
              </a> 
            </div>
    HTML
    if level == 0
      file.write(<<-HTML.gsub(/^ {6}/, ''))
            <div class='jumbotron'>
              <h1>harbottle-main</h1>
              <p>Harbottle's main repo for Enterprise Linux</p>
            </div>
      HTML
    end
    file.write(<<-HTML.gsub(/^ {6}/, ''))
            <div class='page-header'>
              <div class='panel panel-primary'>
                <div class='panel-heading'>#{dir_name}</div>
                <table class='table'>
                  <thead>
                    <tr>
                      <th>&nbsp;</th>
                      <th>&nbsp;</th>
                      <th>File&nbsp;name</th>
                      <th>File&nbsp;size</th>
                    </tr>
                  </thead>
                  <tbody>
    HTML
    if level > 0
      file.write(<<-HTML.gsub(/^ {6}/, ''))
                    <tr>
                      <td>
                      </td>
                      <td>
                        <a href='..'>
                          <i class="fas fa-level-up-alt"></i>
                        </a>
                      </td>
                      <td><a href='..'>..</a></td>
                      <td>&nbsp;</td>
                    </tr>
      HTML
    end
    files = Dir.glob("#{pn}/*").select { |x| File.ftype(x) == 'file' }.sort
    dirs = Dir.glob("#{pn}/*").select { |x| File.ftype(x) == 'directory' }.sort do |x,y|
      pathname_x = Pathname.new(x)
      pathname_y = Pathname.new(y)
      "#{pathname_x.basename}".sub(/\d{8}-/,'') <=> "#{pathname_y.basename}".sub(/\d{8}-/,'')
    end
    (dirs + files).each do | subpath |
      spn = Pathname.new(subpath)
      if File.ftype(subpath) == 'directory'
        friendly_name = "#{spn.basename}".sub(/\d{8}-/,'')
      else
        friendly_name = "#{spn.basename}"
      end
      if File.extname(subpath) == '.rpm' && friendly_name != 'harbottle-main-release.rpm'
        link_target = "#{primary_repo_url}/#{primary_repo_distro}/#{leaf_name}/#{spn.basename}"
        url_check = "#{repo_url_base}#{dir_name}"
      else
        link_target = "#{spn.basename}"
        url_check = "#{repo_url_base}#{dir_name}/#{spn.basename}"
      end
      filesize = (Filesize.from("#{File.size?(subpath)} B").pretty).gsub(' ','&nbsp;')
      if "#{spn.basename}" != 'index.html'
        file.write(<<-HTML.gsub(/^ {6}/, ''))
                    <tr>
                      <td>
        HTML
        if !url_exist?(url_check)
          file.write(<<-HTML.gsub(/^ {6}/, ''))
                        <a href="#{link_target}">
                          <span class="label label-success label-as-badge">
                            New!
                          </span>
                        </a>
            HTML
        end
        file.write(<<-HTML.gsub(/^ {6}/, ''))
                      </td>
                      <td>
                        <a href='#{link_target}'>
                          #{get_icon(subpath)}
                        </a>
                      </td>
                      <td>
                        <div class='truncate-ellipsis'>
                          <span>
                            <a href='#{link_target}'>#{friendly_name}</a>
                          </span>
                        </div>
                      </td>
                      <td>
                        <a href='#{link_target}'>#{filesize}</a>
                      </td>
                    </tr>
        HTML
      end
      if /^\d+$/.match?("#{friendly_name}")
        primary_repo_distro = "epel-#{friendly_name}-x86_64"
      end
      render_html(subpath, level + 1, repo_url_base, primary_repo_url, primary_repo_distro)
    end
    #if level == 1
    #  file.write(<<-HTML.gsub(/^ {6}/, ''))
    #                <tr>
    #                  <td>
    #                  </td>
    #                  <td>
    #                    <a href='https://harbottle.gitlab.io/harbottle-main-src/7/SRPMS'>
    #                      <span class='glyphicon glyphicon-folder-open'></span>
    #                    </a>
    #                  </td>
    #                  <td><a href='https://harbottle.gitlab.io/harbottle-main-src/7/SRPMS'>SRPMS</a></td>
    #                  <td>&nbsp;</td>
    #                </tr>
    #  HTML
    #end
    file.write(<<-HTML.gsub(/^ {6}/, ''))
                  </tbody>
                </table>
              </div><!-- class='panel panel-primary' -->
            </div><!-- class='page-header' -->
          </div><!-- class='container theme-showcase' -->
          <div class='footer'>
            <div class='container text-center'>
              <p>
                <a href='#{"../"*level}#'>
                  harbottle-main
                </a> last updated #{Time.utc(*Time.new.to_a)}</p>
              </p>
            </div><!-- class='container text-center' -->
          </div><!-- class='footer' -->
        </body>
        <script src='#{"../"*level}bower_components/jquery/dist/jquery.min.js'></script>
        <script src='#{"../"*level}bower_components/bootstrap/dist/js/bootstrap.min.js'></script>
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
          ga('create', 'UA-88201102-1', 'auto');
          ga('send', 'pageview');
        </script>
      </html>
    HTML
    file.close
  end
end

def create_release_rpm( name, yum_description, rpm_description, version,
                        iteration, vendor, maintainer, yum_url, rpm_url,
                        public_key, public_key_name )
  FileUtils::mkdir_p './release/etc/yum.repos.d'
  FileUtils::mkdir_p './release/etc/pki/rpm-gpg'
  file = File.open("./release/etc/yum.repos.d/#{name}.repo", "w")
  file.write(<<-REPO.gsub(/^ {4}/, ''))
    [#{name}]
    name=#{yum_description}
    baseurl=#{yum_url}
    gpgcheck=1
    gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-#{public_key_name}
    enabled=1
    REPO
  file.close
  file = File.open("./release/etc/pki/rpm-gpg/RPM-GPG-KEY-#{public_key_name}", "w")
  file.write(public_key)
  file.close
  dir = FPM::Package::Dir.new
  dir.name = "#{name}-release"
  dir.description = rpm_description
  dir.version = version
  dir.iteration = iteration
  dir.vendor = vendor
  dir.maintainer = maintainer
  dir.url = rpm_url
  dir.attributes[:chdir] = "#{FileUtils::pwd}/release"
  dir.input('etc')
  dir.config_files << "/etc/yum.repos.d/#{name}.repo"
  rpm = dir.convert(FPM::Package::RPM)
  output = "NAME-VERSION-ITERATION.ARCH.rpm"
  rpm.output(rpm.to_s(output))
  rpm.cleanup
  dir.cleanup
end

def repo_sync(source_url, target_dir, repo_name)
  # Create repo file
  file = File.open("/tmp/#{repo_name}.repo", "w")
  file.write(<<-REPO.gsub(/^ {4}/, ''))
    [#{repo_name}]
    name=#{repo_name}
    baseurl=#{source_url}
    gpgcheck=0
    enabled=1
    REPO
  file.close

  # Create target directories
  FileUtils.mkdir_p target_dir

  # Sync repo
  print "Sync repo from #{source_url}..."
  outputs = ''
  errors = ''
  command_status = POpen4::popen4("cd .; reposync --repoid=#{repo_name} -c /tmp/#{repo_name}.repo -n -d --download_path=#{target_dir} --norepopath --source")  do |stdout, stderr, stdin|
    stdout.each do |line|
      outputs << "#{line.strip}\n"
    end
    stderr.each do |line|
      errors << "#{line.strip}\n"
    end
  end
  if command_status.exitstatus == 0
    print "Success.\n".green
  else
    print "Failure:\n#{errors}\n".red
    exit 1
  end
end

# Main code

# Sync repo
print "\n"
repo_sync("#{primary_repo_url}/epel-7-x86_64/", './public/7/x86_64', 'harbottle-main-7')
repo_sync("#{primary_repo_url}/epel-8-x86_64/", './public/8/x86_64', 'harbottle-main-8')
repo_sync("#{primary_repo_url}/centos-stream-8-x86_64/", './public/8-stream/x86_64', 'harbottle-main-8-stream')

# Create yum repo
print "\n"
print "Build public yum repo...".ljust(pad_length-14, padstr='.')
outputs = ''
errors = ''
command_status = POpen4::popen4("cd .; createrepo -d public/7/x86_64 -u #{primary_repo_url}/epel-7-x86_64/")  do |stdout, stderr, stdin|
  stdout.each do |line|
    outputs << "#{line.strip}\n"
  end
  stderr.each do |line|
    errors << "#{line.strip}\n"
  end
end
if command_status.exitstatus == 0
  print "Success.\n".green
else
  print "Failure:\n#{errors}\n".red
  exit 1
end
print "\n"
print "Build public yum repo...".ljust(pad_length-14, padstr='.')
outputs = ''
errors = ''
command_status = POpen4::popen4("cd .; createrepo -d public/8/x86_64 -u #{primary_repo_url}/epel-8-x86_64/")  do |stdout, stderr, stdin|
  stdout.each do |line|
    outputs << "#{line.strip}\n"
  end
  stderr.each do |line|
    errors << "#{line.strip}\n"
  end
end
if command_status.exitstatus == 0
  print "Success.\n".green
else
  print "Failure:\n#{errors}\n".red
  exit 1
end
print "\n"
print "Build public yum repo...".ljust(pad_length-14, padstr='.')
command_status = POpen4::popen4("cd .; createrepo -d public/8-stream/x86_64 -u #{primary_repo_url}/centos-stream-8-x86_64/")  do |stdout, stderr, stdin|
  stdout.each do |line|
    outputs << "#{line.strip}\n"
  end
  stderr.each do |line|
    errors << "#{line.strip}\n"
  end
end
if command_status.exitstatus == 0
  print "Success.\n".green
else
  print "Failure:\n#{errors}\n".red
  exit 1
end

# Copy release RPM
Dir.chdir("./public/7/x86_64") do
  source_rpm = Dir.glob("./**/harbottle-main-release*.noarch.rpm")[0]
  FileUtils.cp(source_rpm, "harbottle-main-release.rpm")
end
Dir.chdir("./public/8/x86_64") do
  source_rpm = Dir.glob("./**/harbottle-main-release*.noarch.rpm")[0]
  FileUtils.cp(source_rpm, "harbottle-main-release.rpm")
end
Dir.chdir("./public/8-stream/x86_64") do
  source_rpm = Dir.glob("./**/harbottle-main-release*.noarch.rpm")[0]
  FileUtils.cp(source_rpm, "harbottle-main-release.rpm")
end

# Create directory listings
print "Build public dir listings...".ljust(pad_length-14, padstr='.')
render_html('./public', 0, repo_url_base, primary_repo_url, '')
print "Success.\n".green

# Delete RPMs
Dir.chdir("./public/7/x86_64") do
  FileUtils.mv("harbottle-main-release.rpm","../harbottle-main-release.rpm")
  FileUtils.rm_f(Dir.glob("./**/*.rpm"))
  FileUtils.mv("../harbottle-main-release.rpm","harbottle-main-release.rpm")
end
Dir.chdir("./public/8/x86_64") do
  FileUtils.mv("harbottle-main-release.rpm","../harbottle-main-release.rpm")
  FileUtils.rm_f(Dir.glob("./**/*.rpm"))
  FileUtils.mv("../harbottle-main-release.rpm","harbottle-main-release.rpm")
end
Dir.chdir("./public/8-stream/x86_64") do
  FileUtils.mv("harbottle-main-release.rpm","../harbottle-main-release.rpm")
  FileUtils.rm_f(Dir.glob("./**/*.rpm"))
  FileUtils.mv("../harbottle-main-release.rpm","harbottle-main-release.rpm")
end

# Copy other web assets
print "Build public assets...".ljust(pad_length-14, padstr='.')
FileUtils.cp 'theme.css', 'public/theme.css'
FileUtils.cp 'favicon.png', 'public/favicon.png'
file = File.open("public/RPM-GPG-KEY-harbottle", "w")
file.write(ENV['GPG_PUBLIC_KEY'])
file.close
print "Success.\n".green

# Install bootstrap etc.
print "Build public vendor components...".ljust(pad_length-14, padstr='.')
FileUtils.cp 'bower.json', 'public/bower.json'
command_status = POpen4::popen4("cd public; bower --allow-root install")  do |stdout, stderr, stdin|
  stdout.each do |line|
    outputs << "#{line.strip}\n"
  end
  stderr.each do |line|
    errors << "#{line.strip}\n"
  end
end
if command_status.exitstatus == 0
  print "Success.\n".green
else
  print "Failure:\n#{errors}\n".red
  exit 1
end
