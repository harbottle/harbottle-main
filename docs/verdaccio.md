# verdaccio

[Verdaccio](https://verdaccio.org) package in the
[harbottle-main](https://harbottle.gitlab.io/harbottle-main) CentOS repo.

## Suggested Basic Installation Procedure for CentOS 8

The following script installs the latest version of Verdaccio. It enables and
starts the service.

After running this script, Verdaccio is available at http://localhost:4873 and
serves content from `/var/lib/verdaccio`.

The config file is at `/etc/verdaccio/config.yaml`.

```bash
#!/bin/bash

# Install Verdaccio using harbottle-main repo
dnf -y install https://harbottle.gitlab.io/harbottle-main/8/x86_64/harbottle-main-release.rpm
dnf -y install verdaccio

# Enable and start Verdaccio
systemctl enable --now verdaccio
```

## Suggested Basic Installation Procedure for CentOS 7

The following script installs the latest version of Verdaccio. It enables and
starts the service.

After running this script, Verdaccio is available at http://localhost:4873 and
serves content from `/var/lib/verdaccio`.

The config file is at `/etc/verdaccio/config.yaml`.

```bash
#!/bin/bash

# Install Verdaccio using harbottle-main repo
yum -y install https://harbottle.gitlab.io/harbottle-main/7/x86_64/harbottle-main-release.rpm
yum -y install verdaccio

# Enable and start Verdaccio
systemctl enable --now verdaccio
```
