# Install Mattermost using harbottle-main

[[_TOC_]]

## Intro

This guide provides installation steps for the [Mattermost Server - Team Edition](https://mattermost.com/) RPM package in the
[harbottle-main](https://harbottle.gitlab.io/harbottle-main) CentOS YUM repo.

## Suggested Basic Installation Procedure for CentOS Stream 8

### Option 1: with PostgreSQL

The following script installs PostgreSQL and the latest version of Mattermost
Server - Team Edition on CentOS Stream 8. It initializes PostgreSQL, creates a new
database and configures Mattermost to use it.

After running this script as root, Mattermost is available at
http://localhost:8065

```bash
#!/bin/bash

# Install PostgreSQL
dnf -y install postgresql-server

# Initialize PostgreSQL
postgresql-setup --initdb

# Enable and start PostgreSQL
systemctl enable --now postgresql

# Install mattermost using harbottle-main repo
dnf -y install https://harbottle.gitlab.io/harbottle-main/8-stream/x86_64/harbottle-main-release.rpm
dnf -y install mattermost

# Configure PostgreSQL for Mattermost
sudo su - postgres <<SU
createuser mattermost
createdb mattermost
SU

# Configure Mattermost database settings using jq
# (just edit the config file if you prefer)
dnf -y install jq
MMCONFIG=/etc/mattermost/config.json
sudo su - mattermost -s /bin/bash <<SU
jq '.SqlSettings.DriverName = "postgres"' ${MMCONFIG} > mm.tmp && mv mm.tmp ${MMCONFIG}
jq '.SqlSettings.DataSource = "postgres:///mattermost?host=/run/postgresql"' ${MMCONFIG} > mm.tmp && mv mm.tmp ${MMCONFIG}
SU

# Enable and start Mattermost
systemctl enable --now mattermost
```

### Option 2: With MariaDB

The following script installs MariaDB and the latest version of Mattermost
Server - Team Edition on CentOS Stream 8. It secures and configures MariaDB. It creates
a new database and configures Mattermost to use it.

After running this script, Mattermost is available at http://localhost:8065

Change the passwords in the script to something secure.

```bash
#!/bin/bash

# Change these values
MARIA_ROOT_PW=change_me
MM_DB_PW=change_me

# Install MariaDB
dnf -y install mariadb-server

# Enable and start MariaDB
systemctl enable --now mariadb

# Secure MariaDB
mysql -sfu root <<MARIADB
UPDATE mysql.user SET Password=PASSWORD('${MARIA_ROOT_PW}') WHERE User='root';
DELETE FROM mysql.user WHERE User='';
DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');
DROP DATABASE IF EXISTS test;
DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';
FLUSH PRIVILEGES;
MARIADB

# Configure MariaDB for Mattermost
mysql -sfu root -p${MARIA_ROOT_PW} <<MARIADB
create user 'mmuser'@'%' identified by '${MM_DB_PW}';
create database mattermost;
grant all privileges on mattermost.* to 'mmuser'@'%';
MARIADB

# Install mattermost using harbottle-main repo
dnf -y install https://harbottle.gitlab.io/harbottle-main/8-stream/x86_64/harbottle-main-release.rpm
dnf -y install mattermost

# Configure Mattermost database settings using jq
# (just edit the config file if you prefer)
dnf -y install jq
MMCONFIG=/etc/mattermost/config.json
sudo su - mattermost -s /bin/bash <<SU
jq '.SqlSettings.DriverName = "mysql"' ${MMCONFIG} > mm.tmp && mv mm.tmp ${MMCONFIG}
jq ".SqlSettings.DataSource = \"mmuser:${MM_DB_PW}@unix(/var/lib/mysql/mysql.sock)/mattermost?charset=utf8mb4,utf8\u0026readTimeout=30s\u0026writeTimeout=30s\"" ${MMCONFIG} > mm.tmp && mv mm.tmp ${MMCONFIG}
SU

# Enable and start Mattermost
systemctl enable --now mattermost
```

## Suggested Basic Installation Procedure for CentOS 8

### Option 1: with PostgreSQL

The following script installs PostgreSQL and the latest version of Mattermost
Server - Team Edition on CentOS 8. It initializes PostgreSQL, creates a new
database and configures Mattermost to use it.

After running this script as root, Mattermost is available at
http://localhost:8065

```bash
#!/bin/bash

# Install PostgreSQL
dnf -y install postgresql-server

# Initialize PostgreSQL
postgresql-setup --initdb

# Enable and start PostgreSQL
systemctl enable --now postgresql

# Install mattermost using harbottle-main repo
dnf -y install https://harbottle.gitlab.io/harbottle-main/8/x86_64/harbottle-main-release.rpm
dnf -y install mattermost

# Configure PostgreSQL for Mattermost
sudo su - postgres <<SU
createuser mattermost
createdb mattermost
SU

# Configure Mattermost database settings using jq
# (just edit the config file if you prefer)
dnf -y install jq
MMCONFIG=/etc/mattermost/config.json
sudo su - mattermost -s /bin/bash <<SU
jq '.SqlSettings.DriverName = "postgres"' ${MMCONFIG} > mm.tmp && mv mm.tmp ${MMCONFIG}
jq '.SqlSettings.DataSource = "postgres:///mattermost?host=/run/postgresql"' ${MMCONFIG} > mm.tmp && mv mm.tmp ${MMCONFIG}
SU

# Enable and start Mattermost
systemctl enable --now mattermost
```

### Option 2: With MariaDB

The following script installs MariaDB and the latest version of Mattermost
Server - Team Edition on CentOS 8. It secures and configures MariaDB. It creates
a new database and configures Mattermost to use it.

After running this script, Mattermost is available at http://localhost:8065

Change the passwords in the script to something secure.

```bash
#!/bin/bash

# Change these values
MARIA_ROOT_PW=change_me
MM_DB_PW=change_me

# Install MariaDB
dnf -y install mariadb-server

# Enable and start MariaDB
systemctl enable --now mariadb

# Secure MariaDB
mysql -sfu root <<MARIADB
UPDATE mysql.user SET Password=PASSWORD('${MARIA_ROOT_PW}') WHERE User='root';
DELETE FROM mysql.user WHERE User='';
DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');
DROP DATABASE IF EXISTS test;
DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';
FLUSH PRIVILEGES;
MARIADB

# Configure MariaDB for Mattermost
mysql -sfu root -p${MARIA_ROOT_PW} <<MARIADB
create user 'mmuser'@'%' identified by '${MM_DB_PW}';
create database mattermost;
grant all privileges on mattermost.* to 'mmuser'@'%';
MARIADB

# Install mattermost using harbottle-main repo
dnf -y install https://harbottle.gitlab.io/harbottle-main/8/x86_64/harbottle-main-release.rpm
dnf -y install mattermost

# Configure Mattermost database settings using jq
# (just edit the config file if you prefer)
dnf -y install jq
MMCONFIG=/etc/mattermost/config.json
sudo su - mattermost -s /bin/bash <<SU
jq '.SqlSettings.DriverName = "mysql"' ${MMCONFIG} > mm.tmp && mv mm.tmp ${MMCONFIG}
jq ".SqlSettings.DataSource = \"mmuser:${MM_DB_PW}@unix(/var/lib/mysql/mysql.sock)/mattermost?charset=utf8mb4,utf8\u0026readTimeout=30s\u0026writeTimeout=30s\"" ${MMCONFIG} > mm.tmp && mv mm.tmp ${MMCONFIG}
SU

# Enable and start Mattermost
systemctl enable --now mattermost
```

## Suggested Basic Installation Procedure for CentOS 7

### Option 1: with PostgreSQL

The following script installs a recent version of PostgreSQL and the latest
version of Mattermost Server - Team Edition on CentOS 7. It initializes
PostgreSQL, creates a new database and configures Mattermost to use it.

After running this scriptas root, Mattermost is available at
http://localhost:8065

```bash
#!/bin/bash

# Install PostgreSQL 10 using software collections
yum -y install centos-release-scl
yum -y install rh-postgresql10

# Initialize PostgreSQL
scl enable rh-postgresql10 - <<SCL
postgresql-setup --initdb
SCL

# Enable and start PostgreSQL
systemctl enable --now rh-postgresql10-postgresql

# Install mattermost using harbottle-main repo
yum -y install https://harbottle.gitlab.io/harbottle-main/7/x86_64/harbottle-main-release.rpm
yum -y install mattermost

# Configure PostgreSQL for Mattermost
sudo su - postgres <<SU
scl enable rh-postgresql10 - <<SCL
createuser mattermost
createdb mattermost
SCL
SU

# Configure Mattermost database settings using jq and sponge
# (just edit the config file if you prefer)
yum -y install epel-release
yum -y install moreutils jq
MMCONFIG=/etc/mattermost/config.json
sudo su - mattermost -s /bin/bash <<SU
jq '.SqlSettings.DriverName = "postgres"' ${MMCONFIG} | sponge ${MMCONFIG}
jq '.SqlSettings.DataSource = "postgres:///mattermost?host=/run/postgresql"' ${MMCONFIG} | sponge ${MMCONFIG}
SU

# Enable and start Mattermost
systemctl enable --now mattermost
```

### Option 2: With MariaDB

The following script installs a recent version of MariaDB and the latest
version of Mattermost - Team Edition on CentOS 7. It secures and
configures MariaDB. It creates a new database and configures Mattermost to use
it.

After running this script, Mattermost is available at http://localhost:8065

Change the passwords in the script to something secure.

```bash
#!/bin/bash

# Change these values
MARIA_ROOT_PW=change_me
MM_DB_PW=change_me

# Install MariaDB 10.2 using software collections
yum -y install centos-release-scl
yum -y install rh-mariadb102

# Enable and start MariaDB
systemctl enable --now rh-mariadb102-mariadb

# Secure MariaDB
scl enable rh-mariadb102 - <<SCL
mysql -sfu root <<MARIADB
UPDATE mysql.user SET Password=PASSWORD('${MARIA_ROOT_PW}') WHERE User='root';
DELETE FROM mysql.user WHERE User='';
DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');
DROP DATABASE IF EXISTS test;
DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';
FLUSH PRIVILEGES;
MARIADB
SCL

# Configure MariaDB for Mattermost
scl enable rh-mariadb102 - <<SCL
mysql -sfu root -p${MARIA_ROOT_PW} <<MARIADB
create user 'mmuser'@'%' identified by '${MM_DB_PW}';
create database mattermost;
grant all privileges on mattermost.* to 'mmuser'@'%';
MARIADB
SCL

# Install mattermost using harbottle-main repo
yum -y install https://harbottle.gitlab.io/harbottle-main/7/x86_64/harbottle-main-release.rpm
yum -y install mattermost

# Configure Mattermost database settings using jq and sponge
# (just edit the config file if you prefer)
yum -y install epel-release
yum -y install moreutils jq
MMCONFIG=/etc/mattermost/config.json
sudo su - mattermost -s /bin/bash <<SU
jq '.SqlSettings.DriverName = "mysql"' ${MMCONFIG} | sponge ${MMCONFIG}
jq ".SqlSettings.DataSource = \"mmuser:${MM_DB_PW}@unix(/var/lib/mysql/mysql.sock)/mattermost?charset=utf8mb4,utf8\u0026readTimeout=30s\u0026writeTimeout=30s\"" ${MMCONFIG} | sponge ${MMCONFIG}
SU

# Enable and start Mattermost
systemctl enable --now mattermost
```
